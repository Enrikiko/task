<div class="block">
  <? if (count($this->data["workshops"])) { ?>
    <? foreach ($this->data["workshops"] as $workshop) { ?>
      <div class="col-md-4 col-xs-6">
        <div class="block block-condensed padding-top-20">
            <? if ($this->getValue($workshop,"Image")) { ?>
              <div style="height: 130px;background-repeat: no-repeat;background-image: url(<?= $this->getValue($workshop,"Image"); ?>)">
            <? } else { ?>
              <div>
            <? } ?>
                <div class="contact-container">
                    <b><?= $workshop["name"]; ?></b>
                </div>
            </div>
            <div class="list-group">
              <? if ($workshop["address"] != "-") { ?>
                <div class="list-group-item"><span class="fa fa-map-marker"></span><?= $workshop["address"]; ?></div>
              <? } ?>
              <? if ($workshop["phone"] != "-") { ?>
                <div class="list-group-item"><span class="fa fa-phone"></span><?= $workshop["phone"]; ?></div>
              <? } ?>
            </div>
            <div class="block-content padding-top-20">
                <div class="row">
                    <div class="col-md-12">
                      <form class="" action="employee" method="post">
                        <button name="workshop" value="<?= $workshop["ID_workshop"]; ?>" class="btn btn-info btn-clean btn-block"><?= $this->language->getLabel("cmd-enter"); ?></button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <? } ?>
  <? } else { ?>
    <h2><?= $this->language->getLabel("no-workshop"); ?></h2>
    <a href="/admin/workshop/0">
      <button name="workshop" value="<?= $workshop["ID_workshop"]; ?>" class="btn btn-info btn-icon-fixed">
        <?= $this->language->getLabel("cmd-new-workshop"); ?>
        <span class="fa fa-plus"></span>
      </button>
    </a>
  <? } ?>
</div>
