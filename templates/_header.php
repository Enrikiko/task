<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Rastrillos Doblas</title>

        <!-- META SECTION -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- END META SECTION -->
        <!-- CSS INCLUDE -->
        <link rel="stylesheet" href="/assets/boooya/css/styles.css">
        <!-- EOF CSS INCLUDE -->
        <!-- IMPORTANT SCRIPTS -->
        <script type="text/javascript" src="/assets/boooya/js/vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/moment/moment.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/customscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="/assets/js/vue.js"></script>
        <!-- END IMPORTANT SCRIPTS -->
        <!-- THIS PAGE SCRIPTS -->
        <script type="text/javascript" src="/assets/boooya/js/vendor/bootstrap-select/bootstrap-select.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/select2/select2.full.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/bootstrap-daterange/daterangepicker.js"></script>
        <!-- END THIS PAGE SCRIPTS -->

        <script type="text/javascript" src="/assets/boooya/js/vendor/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/datatables/dataTables.bootstrap.min.js"></script>
        <!-- APP SCRIPTS -->
        <script type="text/javascript" src="/assets/boooya/js/app.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/app_plugins.js"></script>
        <!-- END APP SCRIPTS -->
        <!-- START OWN SCRIPTS -->
        <script type="text/javascript" src="/assets/js/language.js"></script>
        <script type="text/javascript" src="/assets/js/manageTasks.js"></script>
        <script type="text/javascript" src="/assets/js/mask.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/sweetalert/sweetalert.min.js"></script>
        <!-- END OWN SCRIPTS-->
    </head>
    <body>

        <!-- APP WRAPPER -->
        <div class="app">
  
            <!-- START APP CONTAINER -->
            <div class="app-container">
                <!-- START SIDEBAR -->
                <div class="app-sidebar app-navigation app-navigation-fixed scroll app-navigation-style-default app-navigation-open-hover dir-left" data-type="close-other" style="<?= ( (!array_key_exists("workshop",$_SESSION) || !array_key_exists("employee",$_SESSION)) ? 'display:none;' : '' ); ?>">

                    <div class="app-header" style="background: none; border-bottom: none;width: auto;height: auto;float: right;">
                        <ul class="app-header-buttons"  style="<?= ( (!array_key_exists("workshop",$_SESSION) || !array_key_exists("employee",$_SESSION)) ? 'display: none;' : '' ); ?>padding-top: 0px;padding-bottom: 0px;">
                            <li class="visible-mobile"><a href="#" class="btn btn-link btn-icon" data-sidebar-toggle=".app-sidebar.dir-left"><span class="icon-menu"></span></a></li>
                            <li class="hidden-mobile"><a href="#" class="btn btn-link btn-icon" data-sidebar-minimize=".app-sidebar.dir-left"><span class="icon-menu"></span></a></li>
                        </ul>
                    </div>
                    <? if (array_key_exists("logo",$_SESSION) && $_SESSION["logo"]) { ?>
                      <a href="/task">
                        <img src="<?= $_SESSION["logo"]; ?>" style="max-height: 45px;padding-top: 5px;padding-left: 5px;">
                      </a>
                    <? } ?>

                    <nav>
                        <ul>
                            <li><a href="/products"><span class="fa fa-cubes"></span><?= $this->language->getLabel("inventory"); ?></a></li>
                            <li><a href="/task"><span class="fa fa-tasks"></span><?= $this->language->getLabel("task"); ?></a></li>
                            <? if ($_SESSION["ID_User"] == -1) {  ?>
                              <li><a href="/receipt"><span class="fa fa-shopping-basket"></span><?= $this->language->getLabel("receipt"); ?></a></li>
                              <li><a href="/order"><span class="fa fa-eur"></span><?= $this->language->getLabel("order"); ?></a></li>
                            <? }  ?>
                            <? if (authService::isAdmin()) { ?>
                              <li class="openable <?= (authService::inSection("admin") ? "open" : "" ); ?>">
                                  <a href="#"><span class="fa fa-adn"></span><?= $this->language->getLabel("admin"); ?>
                                    <? if (count($this->alert["Stock"])) { ?>
                                      <span class="label label-danger label-bordered label-ghost">!</span>
                                    <? } ?>
                                  </a>
                                  <ul>
                                    <li>
                                      <a href="/admin/workshop"><span class="fa fa-building"></span><?= $this->language->getLabel("workshop"); ?></a>
                                    </li>
                                    <li>
                                      <a href="/admin/location"><span class="fa fa-map-marker"></span><?= $this->language->getLabel("location"); ?></a>
                                    </li>
                                    <li>
                                      <a href="/admin/employee"><span class="fa fa-group"></span><?= $this->language->getLabel("employee"); ?></a>
                                    </li>
                                    <li>
                                      <a href="/admin/family"><span class="fa fa-tags"></span><?= $this->language->getLabel("family"); ?></a>
                                    </li>
                                    <li>
                                      <a href="/admin/feature"><span class="fa fa-puzzle-piece"></span><?= $this->language->getLabel("feature"); ?></a>
                                    </li>
                                    <li>
                                      <a href="/admin/product">
                                        <span class="fa fa-cubes"></span><?= $this->language->getLabel("products"); ?>
                                        <? if (count($this->alert["Stock"])) { ?>
                                          <span class="label label-danger label-bordered label-ghost"><?= count($this->alert["Stock"]); ?></span>
                                        <? } ?>
                                      </a>
                                    </li>
                                    <li class="openable <?= (authService::inSection("admin/task") ? "open" : "" ); ?>">
                                      <a href="#"><span class="fa fa-tasks"></span><?= $this->language->getLabel("admin-task"); ?></a>
                                      <ul>
                                        <li>
                                          <a href="/admin/taskproject"><span class="fa fa-book"></span><?= $this->language->getLabel("projects"); ?></a>
                                        </li>
                                        <li>
                                          <a href="/admin/task"><span class="fa fa-list"></span><?= $this->language->getLabel("task"); ?></a>
                                        </li>
                                        <li>
                                          <a href="/admin/taskhistoric"><span class="fa fa-calendar"></span><?= $this->language->getLabel("historic"); ?></a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                              </li>
                              <li>
                                <a href="/settings"><span class="fa fa-gear"></span><?= $this->language->getLabel("settings"); ?></a>
                              </li>
                            <? } ?>
                            <? if ($this->hideElement("workshop")) { ?>
                              <li>
                                <a href="/" style="color: red;">
                                  <span class="fa fa-building"></span>
                                  <?= $this->language->getLabel("change-workshop"); ?>
                                </a>
                              </li>
                              <? if ($this->hideElement("employee")) { ?>
                                <li>
                                  <a href="/employee" style="color: red;">
                                    <span class="fa fa-group"></span>
                                    <?= $this->language->getLabel("change-employee"); ?>
                                  </a>
                                </li>
                              <? } ?>
                            <? } ?>
                          <li><a href="/logout"><span class="icon-exit"></span> <?= $this->language->getLabel("cmd-disconect"); ?></a></li>
                        </ul>
                    </nav>
                </div>
                <!-- END SIDEBAR -->

                <!-- START APP CONTENT -->
                <div class="<?= ( (!array_key_exists("workshop",$_SESSION) || !array_key_exists("employee",$_SESSION)) ? '' : 'app-content app-sidebar-left' ); ?>">
                    <!-- START APP HEADER -->
                    <!-- END APP HEADER  -->

                    <!-- START PAGE HEADING -->
                    <? include_once "./templates/_notifications.php"; ?>
                    <!-- END PAGE HEADING -->

                    <!-- START PAGE CONTAINER -->
                    <div class="container">