<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Manage Tasks</title>

        <!-- META SECTION -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- END META SECTION -->
        <!-- CSS INCLUDE -->
        <link rel="stylesheet" href="/assets/boooya/css/styles.css">
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>

        <!-- APP WRAPPER -->
        <div class="app">

            <!-- START APP CONTAINER -->
            <div class="app-container">

                <div class="app-login-box">
                    <div class="app-login-box-user"></div>
                    <div class="app-login-box-title">
                        <div class="title"><?= $this->language->getLabel("sign-member"); ?></div>
                        <div class="subtitle"><?= $this->language->getLabel("sign-in"); ?></div>
                    </div>
                    <div class="app-login-box-container">
                        <form action="" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="login" placeholder="<?= $this->language->getLabel("sign-email"); ?>">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="<?= $this->language->getLabel("sign-password"); ?>">
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                        <div class="app-checkbox">
                                            <label><input type="checkbox" name="remember" value="1"> <?= $this->language->getLabel("sign-remember"); ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <button class="btn btn-success btn-block"><?= $this->language->getLabel("cmd-login"); ?></button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="app-login-box-footer">
                        &copy; Enric Doblas 2016. All rights reserved.
                    </div>
                </div>

            </div>
            <!-- END APP CONTAINER -->

        </div>
        <!-- END APP WRAPPER -->

        <!-- IMPORTANT SCRIPTS -->
        <script type="text/javascript" src="/assets/boooya/js/vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="/mageTasks/assets/boooya/js/vendor/moment/moment.min.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/vendor/customscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <!-- END IMPORTANT SCRIPTS -->
        <!-- APP SCRIPTS -->
        <script type="text/javascript" src="/assets/boooya/js/app.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/app_plugins.js"></script>
        <script type="text/javascript" src="/assets/boooya/js/app_demo.js"></script>
        <!-- END APP SCRIPTS -->
    </body>
</html>
