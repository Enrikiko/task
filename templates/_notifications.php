<? if ($this->notification->show()) { ?>
	<div class="col-md-12" style="padding-top: 10px">
		<div class="alert alert-<?= $this->notification->type; ?> alert-icon-block alert-dismissible" role="alert">
		    <div class="alert-icon">
		        <span class="<?= $this->notification->icon; ?>"></span> 
		    </div>
		    <? if ($this->notification->title != "") { ?>
		    	<h4 style="margin: 0px;"><strong><?= $this->notification->title; ?></strong></h4>
		    <? } ?>
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span></button>
		    <?  if ($this->notification->message) { ?>
		    	<ul>
		    		<? foreach ($this->notification->message as $message) { ?>
		    			<li><?= $message; ?></li>
		    		<? } ?>
		    	</ul>
		    <? } ?>
		</div>
	</div>
<? } ?>
