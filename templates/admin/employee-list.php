<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["employees"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th colspan="2">&nbsp;</th>
                    <th><?= $this->language->getLabel("name"); ?></th>
                    <th><?= $this->language->getLabel("schedule"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["employees"] as $employee) { ?>
                <tr style="<?= (!count(json_decode($employee["Tasks"])) ? "background-color: bisque;" : ""); ?>">
                  <td>
                    <? if (!count(json_decode($employee["Tasks"]))) { ?>
                      <span class="fa fa-exclamation-circle" style="font-size: 25px;color: red;" data-toggle="tooltip" data-placement="right" data-original-title="<?= $this->language->getLabel("tooltip_assing_task"); ?>"></span>
                    <? } ?>
                  </td>
                  <td>
                    <img style="max-width: 60px;" src="<?= $this->getValue($employee,"Image"); ?>" alt="">
                  </td>
                  <td><a href="/admin/employee/<?= $employee["ID_Employee"]; ?>"><?= $employee["Name"]; ?></a></td>
                  <td><a href="/admin/employee/<?= $employee["ID_Employee"]; ?>"><?= $employee["Horario"]; ?></a></td>
                  <td style="float:right;">
                    <a href="/admin/employee/<?= $employee["ID_Employee"]; ?>">
                      <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                    </a>
                    <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $employee["Name"]; ?>','<?= $employee["ID_Employee"]; ?>')">
                        <?= $this->language->getLabel("cmd-delete"); ?>
                        <span class="fa fa-trash"></span>
                    </button>
                  </td>
                </tr>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-employees"); ?>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>