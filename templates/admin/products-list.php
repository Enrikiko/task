<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["products"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th colspan="2">&nbsp;</th>
                    <th><?= $this->language->getLabel("s-family"); ?></th>
                    <th class="col-md-3"><?= $this->language->getLabel("name"); ?></th>
                    <th><?= $this->language->getLabel("description"); ?></th>
                    <th><?= $this->language->getLabel("min_quantity"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["products"] as $product) { ?>
                <tr style="<?= (array_key_exists($product["ID_Product"], $this->alert["Stock"]) ? "background-color: bisque;" : ""); ?>">
                  <td>
                    <? if (array_key_exists($product["ID_Product"], $this->alert["Stock"])) { ?>
                      <span class="fa fa-exclamation-circle" style="font-size: 25px;color: red;" data-toggle="tooltip" data-placement="right" data-original-title="<?= $this->language->getLabel("tooltip_product"); ?>"></span>
                    <? } ?>
                  </td>
                  <td>
                    <? if ($this->getValue($product,"Image")) { ?>
                    <button class="btn btn-default preview" data-preview-image="<?= $this->getValue($product,"Image"); ?>" data-preview-size="modal-lg">
                      <img style="max-width: 60px;" src="<?= $this->getValue($product,"Image"); ?>" alt="">
                    </button>
                    <? } else { ?>
                      &nbsp;
                    <? } ?>
                  </td>
                  <td>
                    <a href="/admin/product/<?= $product["ID_Product"]; ?>">
                      <?= $this->getValue($this->data["family"],array($product["ID_Family"],"Description")); ?>
                    </a>
                  </td>
                  <td><a href="/admin/product/<?= $product["ID_Product"]; ?>"><?= $product["Name"]; ?></a></td>
                  <td><a href="/admin/product/<?= $product["ID_Product"]; ?>"><?= ($product["Description"] ? $product["Description"] : "-" );?></a></td>
                  <td><a href="/admin/product/<?= $product["ID_Product"]; ?>"><?= ($product["Min_enabled"] ? $product["Min_quantity"] : "-"); ?></a></td>
                  <td style="float:right;">
                    <a href="/admin/product/<?= $product["ID_Product"]; ?>">
                      <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                    </a>
                    <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $product["Name"]; ?>','<?= $product["ID_Product"]; ?>')">
                        <?= $this->language->getLabel("cmd-delete"); ?>
                        <span class="fa fa-trash"></span>
                    </button>
                  </td>
                </tr>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-products"); ?>
      <? } ?>
    </div>
  </div>
  <? include_once "./templates/_warning.php"; ?>
</form>

<? include_once "./templates/_showImage.php"; ?>