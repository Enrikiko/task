<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["families"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th><?= $this->language->getLabel("description"); ?></th>
                    <th><?= $this->language->getLabel("type-family"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["families"] as $family) { ?>
                <tr>
                  <td><a href="/admin/family/<?= $family["ID_Family"]; ?>"><?= $family["Description"]; ?></a></td>
                  <td><a href="/admin/family/<?= $family["ID_Family"]; ?>"><?= $this->language->getLabel($this->data["type"][$family["Type"]]); ?></a></td>
                  <td style="float:right;">
                    <a href="/admin/family/<?= $family["ID_Family"]; ?>">
                      <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                    </a>
                    <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $family["Description"]; ?>','<?= $family["ID_Family"]; ?>')">
                        <?= $this->language->getLabel("cmd-delete"); ?>
                        <span class="fa fa-trash"></span>
                    </button>
                  </td>
                </tr>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-family"); ?>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>
