<script src="/assets/grantt/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="/assets/grantt/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">
<div class="block">
	<div class="col-md-6">
		<h1>
			<b>
				<?= $this->language->getLabel("flow-task"); ?>
			</b>
		</h1>
	</div>
	<br>
	<div style="padding-top:30px" id="gantt"></div>
	<br>
	<div class="block-content btn-group">
		<form action="" method="post">
			<button class="btn btn-success btn-icon-fixed" name="cmd-save" value="" onclick="save(this)" style="float:right;">
				<span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
			</button>
		</form>
	</div>
</div>
<script type="text/javascript">
	var tasks =  <?= json_encode($this->data["flow"]); ?>;

	gantt.config.autosize = true;
	gantt.config.columns = [
	    {name:"text",       label:"Task name",  width:"*", tree:true },
	    {name:"start_date", label:"Start time", align: "center" },
	    {name:"duration",   label:"Duration",   align: "center" },
	];
	gantt.config.buttons_left = ["dhx_save_btn", "dhx_cancel_btn"];
	gantt.config.buttons_right = ["dhx_delete_btn"];


	gantt.init("gantt");

	gantt.parse(tasks);

	function save(button) {
		button.value = JSON.stringify(gantt.serialize());
	}

	$( document ).ready(function() {
		$('.gantt_add').css("display", "none");
	});
</script>
