<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["tasks"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th><?= $this->language->getLabel("name"); ?></th>
                    <th><?= $this->language->getLabel("description"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["tasks"] as $task) { ?>
                <tr>
                    <td><a href="/admin/task/<?= $task["ID_Task"]; ?>"></a></td>
                    <td><a href="/admin/task/<?= $task["ID_Task"]; ?>"><?= $task["Name"]; ?></a></td>
                    <td><a href="/admin/task/<?= $task["ID_Task"]; ?>"><?= $task["Description"]; ?></a></td>
                    <td style="float:right;">
                      <a href="/admin/task?active=<?= $task["ID_Task"]; ?>">
                        <button type="button" class="btn btn-success btn-clean"><?= $this->language->getLabel("cmd-enabled-task"); ?> <span class="fa fa-play-circle"></span></button>
                      </a>
                      <a href="/admin/task/<?= $task["ID_Task"]; ?>">
                        <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                      </a>
                      <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $task["Description"]; ?>','<?= $task["ID_Task"]; ?>')">
                          <?= $this->language->getLabel("cmd-delete"); ?>
                          <span class="fa fa-trash"></span>
                      </button>
                    </td>
                </tr>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-tasks"); ?>
      <? } ?>
    </div>
  </div>
  <? include_once "./templates/_warning.php"; ?>
</form>