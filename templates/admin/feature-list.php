<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["features"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th><?= $this->language->getLabel("description"); ?></th>
                    <th><?= $this->language->getLabel("unity"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["features"] as $feature) { ?>
                <? if ($feature["ID_User"]) { ?>
                  <tr>
                    <td><a href="/admin/feature/<?= $feature["ID_Feature"]; ?>"><?= $feature["Name"]; ?></a></td>
                    <td><a href="/admin/feature/<?= $feature["ID_Feature"]; ?>"><?= $this->language->getLabel($feature["Unity"]); ?></a></td>
                    <td style="float:right;">
                      <a href="/admin/feature/<?= $feature["ID_Feature"]; ?>">
                        <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                      </a>
                      <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $feature["Name"]; ?>','<?= $feature["ID_Feature"]; ?>')">
                          <?= $this->language->getLabel("cmd-delete"); ?>
                          <span class="fa fa-trash"></span>
                      </button>
                    </td>
                  </tr>
                <? } else { ?>
                  <tr>
                    <td><?= $feature["Name"]; ?></td>
                    <td colspan="2"><?= $this->language->getLabel($feature["Unity"]); ?></td>
                  </tr>
                <? } ?>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-feature"); ?>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>
