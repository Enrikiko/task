<form class="" action="/admin/family/<?= $this->request["id"]; ?>" method="post">
  <div class="block">
    <div class="col-md-6">
      <h1>
        <b>
          <? if ($this->request["id"]) { ?>
            <?= $this->language->getLabel("s-family"); ?>
          <? } else { ?>
            <?= $this->language->getLabel("cmd-new-family"); ?>
          <? } ?>
        </b>
      </h1>
    </div>
    <div class="block-content" id="family">
      <div class="block" style="padding: 0px;">
        <ul class="nav nav-pills nav-pills-bordered nav-justified">
            <li class="active"><a href="#general" data-toggle="tab"><?= $this->language->getLabel("detail-family"); ?></a></li>
            <li v-if="type == 'P'"><a href="#create-features" data-toggle="tab"><?= $this->language->getLabel("create-features"); ?></a></li>
            <li v-if="type == 'P'"><a href="#preview-features" data-toggle="tab"><?= $this->language->getLabel("preview-features"); ?></a></li>
        </ul>
      </div>

      <div class="tab-content">
        <div class="tab-pane active" id="general">
          <div class="col-md-6">
            <h4><b><?= $this->language->getLabel("s-general"); ?></b></h4>
            <div class="col-md-12">
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("assign-type"); ?></label>
                <select class="form-control" v-model="type" name="Type" required>
                  <option value=""><?= $this->language->getLabel("select-type"); ?></option>
                  <? foreach ($this->data["type"] as $key => $type) { ?>
                    <option value="<?= $key; ?>" >
                      <?= $this->language->getLabel("s-" . $type); ?>
                    </option>
                  <? } ?>
                </select>
              </div>
              <div class="input-group" v-if="type == 'P'">
                <label class="control-label"><?= $this->language->getLabel("select-state-matter"); ?></label>
                <select class="form-control" name="State_matter" >
                  <option value="0"><?= $this->language->getLabel("select-state-matter"); ?></option>
                  <? foreach ($this->data["state_matter"] as $key => $state) { ?>
                    <option value="<?= $key; ?>" <?= ( $key == $this->getValue($this->data["family"],"State_matter") ? "selected=\"selected\"" : "" ); ?> >
                      <?= $this->language->getLabel("state_matter_" . $key); ?>
                    </option>
                  <? } ?>
                </select>
              </div>
              <div class="input-group" v-if="type == 'P'">
                <label class="control-label"><?= $this->language->getLabel("select-family-father"); ?></label>
                <select class="form-control" id="ID_Father" name="ID_Father" v-on:change="getFamilyFeatures()">
                  <option value="0"><?= $this->language->getLabel("no-family-father"); ?></option>
                  <? foreach ($this->data["families"] as $id_family => $family) { ?>
                    <option value="<?= $id_family; ?>" <?= ( $id_family == $this->getValue($this->data["family"],"ID_Family_Father") ? "selected=\"selected\"" : "" ); ?> >
                      <?= $family["Description"]; ?>
                    </option>
                  <? } ?>
                </select>
              </div>
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("description"); ?></label>
                <input class="form-control" name="Description" type="text" value="<?= $this->getValue($this->data["family"],"Description"); ?>" required>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="create-features">
          <input type="text" hidden="hidden" name="features" v-model="getJSON(family_features)">
          <div class="block">
            <div class="col-md-12">
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("feature-name"); ?></label>
                <select class="form-control" id="ID_Feature" v-on:change="getFamilyFeatures()" required>
                  <option value="0"><?= $this->language->getLabel("select-attribute"); ?></option>
                  <? foreach ($this->data["features"] as $key => $feature) { ?>
                    <option value="<?= $key; ?>">
                      <?= $feature["Name"]; ?>
                    </option>
                  <? } ?>
                </select>
              </div>
              <button type="button" class="btn btn-warning col-md-12" v-on:click="addFeature">
                <span class="fa fa-plus"></span> <?= $this->language->getLabel("cmd-add-feature"); ?>
              </button>
            </div>
          </div>
          <div class="block">
            <div class="col-md-6">
              <h4 style="padding-top: 10px;"><b><?= $this->language->getLabel("list-feature-father"); ?></b></h4>
              <table class="table table-head-custom" v-if="features_father.length">
                <thead>
                    <tr>
                        <th><?= $this->language->getLabel("name"); ?></th>
                        <th><?= $this->language->getLabel("rest"); ?></th>
                    </tr>
                </thead>
                <tbody>
                  <tr v-for="(feature, index) in features_father">
                    <td>{{feature.Name}}</td>
                    <td>
                      <input type="checkbox" v-model="feature.Rest" disabled="true">
                    </td>
                  </tr>
                </tbody>
              </table>
              <label v-else><?= $this->language->getLabel("no-feature"); ?></label>
            </div>
            <div class="col-md-6">
              <h4 style="padding-top: 10px;"><b><?= $this->language->getLabel("list-feature"); ?></b></h4>
              <table class="table table-head-custom" v-if="family_features.length">
                <thead>
                    <tr>
                        <th><?= $this->language->getLabel("name"); ?></th>
                        <th><?= $this->language->getLabel("rest"); ?></th>
                        <th class="col-md-1">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                  <tr v-for="(feature, index) in family_features" v-if="feature.remove == 0">
                    <td>{{feature.Name}}</td>
                    <td>
                      <input type="checkbox" v-model="feature.Rest">
                    </td>
                    <td>
                      <button type="button" class="btn btn-danger col-md-12" v-on:click="delFeature(index)">
                        <span class="fa fa-trash"></span> <?= $this->language->getLabel("cmd-delete"); ?>
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <label v-else><?= $this->language->getLabel("no-feature"); ?></label>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="preview-features">
          <input type="text" hidden="hidden" name="preview" v-model="getJSON(preview_settings)">
          <div class="block">
            <div class="col-md-12">
              <h4><b><?= $this->language->getLabel("generate-product-preview"); ?></b></h4>
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="input-group">
                    <label class="control-label"><?= $this->language->getLabel("select-attribute"); ?></label>
                    <select class="form-control" v-model="select" id="select-feature">
                      <option value=""><?= $this->language->getLabel("select-attribute"); ?></option>
                      <option v-for="feature in features_father" :value="feature.ID_Feature">{{feature.Name}}</option>
                      <option v-for="feature in family_features" v-if="feature.ID_Feature" :value="feature.ID_Feature">{{feature.Name}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="control-label"><?= $this->language->getLabel("select-postion"); ?></label>
                  <div>
                    <label><input type="radio" v-model="position" value="b"><?= $this->language->getLabel("before"); ?></label>
                    <label><input type="radio" v-model="position" value="a"><?= $this->language->getLabel("after"); ?></label>
                    <label><input type="radio" v-model="position" value="ba"><?= $this->language->getLabel("before-after"); ?></label>
                    <label><input type="radio" v-model="position" value="e"><?= $this->language->getLabel("empty-feature"); ?></label>
                  </div>
                </div>
              </div>
              <button type="button" class="btn btn-warning col-md-12" v-on:click="addPreview">
                <span class="fa fa-plus"></span>
                <?= $this->language->getLabel("cmd-add"); ?>
              </button>
            </div>
          </div>
          <div class="block">
            <h4><b><?= $this->language->getLabel("preview-product"); ?></b></h4>
            <div class="col-md-12">
              <div v-for="(setting, index) in preview_settings">
                <div class="col-md-1" style="margin-top: 5px;" v-if="setting.position != 'a' && setting.position != 'e'">
                  <input type="text" class="form-control" v-model="setting.value_before">
                </div>
                <div class="col-md-2" style="padding: 5px;border: 1px solid #DBE0E4;margin: 5px;text-align: center;">
                  <label class="col-md-10">{{setting.label}}</label>
                  <button type="button" class="btn btn-danger col-md-2" style="padding: 0px 5px 0px 5px;" v-on:click="delPreview(index)">
                    <span class="fa fa-trash"></span>
                  </button>
                </div>
                <div class="col-md-1" style="margin-top: 5px;" v-if="setting.position != 'b' && setting.position != 'e'">
                  <input type="text" class="form-control" v-model="setting.value_after">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-content btn-group" id="delete">
      <? if ($this->request["id"]) { ?>
        <button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["family"]["Description"]; ?>','<?= $this->request["id"]; ?>')">
            <span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
        </button>
        <button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["family"]["ID_Family"]; ?>" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
        </button>
      <? } else { ?>
        <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
        </button>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>


<script type="text/javascript">
  var php_preview = <?= ($this->getValue($this->data["family"],"Preview_Product") ? $this->getValue($this->data["family"],"Preview_Product") : '[]'); ?>;
  var php_features = <?= json_encode($this->data["features"]); ?>;
  var php_family_features = <?= json_encode($this->data["family_features"]); ?>;
  var php_features_family = <?= json_encode($this->data["features_family"]); ?>;
  var php_type = '<?= ( $this->getValue($this->data["family"],"Type") ? $this->getValue($this->data["family"],"Type") : ""); ?>';

  initFamily();
</script>