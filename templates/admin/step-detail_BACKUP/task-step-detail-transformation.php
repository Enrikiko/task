              <div v-else>
                <h4><b><?= $this->language->getLabel("steps"); ?></b></h4>
                <div class="block" v-for="(step, index) in steps">
                  <div class="col-md-6">
                    <div class="input-group">
                      <label><?= $this->language->getLabel("action"); ?></label>
                      <select class="form-control" v-model="step.actions[0].id_action" v-on:change="addValues(index, actions[step.actions[0].id_action].Features)">
                          <option value="0"><?= $this->language->getLabel("select-action"); ?></option>
                          <? foreach ($this->data["actions_steps"] as $id_action => $action) { ?>
                            <? if ($action["Type"] == "T") {  ?>
                              <option value="<?= $id_action; ?>"><?= $this->language->getLabel("action-" . $action["Name"]); ?></option>
                            <? } ?>
                          <? } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-1">
                    <button type="button" class="btn btn-danger col-md-12" v-on:click="delStep(index)">
                      <span class="fa fa-trash">Delete step</span>
                    </button>
                  </div>
                  <div v-if="step.actions[0].id_action != '0'">
                    <h5><b><?= $this->language->getLabel("product"); ?></b></h5>
                    <div class="col-md-12" v-for="(action_obj, action_id ) in step.actions">
                      <div v-if="action_obj.del != '1'">
                        <div class="col-md-8">
                          <div class="input-group">
                            <label class="control-label"><?= $this->language->getLabel("s-product"); ?></label>
                            <select class="form-control" v-model="action_obj.id_product">
                                <option value="0"><?= $this->language->getLabel("select-product"); ?></option>
                                <option v-for="(product, id_product) in products" :value="id_product">{{product.Name}}</option>
                                <option v-for="product in products_created" :value="product.ID_Product">{{product.Name}}</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="input-group">
                            <label class="control-label"><?= $this->language->getLabel("qty"); ?></label>
                            <input class="form-control" type="text" v-model="action_obj.qty">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <button type="button" class="btn btn-danger col-md-12" v-on:click="delAction(index, action_id)">
                            <span class="fa fa-trash"></span>
                          </button>
                        </div>
                      </div>
                    </div>
                    <button type="button" class="btn btn-warning col-md-12" v-on:click="addActionTransformation(index, actions[step.actions[0].id_action].Features )">
                      <span class="fa fa-plus"><?= $this->language->getLabel("cmd-add-product"); ?></span>
                    </button>
                  </div>
                </div>
              </div>