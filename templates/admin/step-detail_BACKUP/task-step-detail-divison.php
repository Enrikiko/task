                    <div v-if="action == 'D'">
                      <div class="col-md-2">
                        <div class="input-group">
                          <label><?= $this->language->getLabel("action"); ?></label>
                          <select class="form-control" v-model="action_obj.id_action">
                              <option value="0"><?= $this->language->getLabel("select-action"); ?></option>
                              <? foreach ($this->data["actions_steps"] as $id_action => $action) { ?>
                                <? if ($action["Type"] == "D") {  ?>
                                  <option value="<?= $id_action; ?>"><?= $this->language->getLabel("action-" . $action["Name"]); ?></option>
                                <? } ?>
                              <? } ?>
                          </select>
                        </div>
                      </div>
                      <div v-if="action_obj.id_action > 0">
                        <div v-for="feature in actions[action_obj.id_action].Obj_Features" class="col-md-2">
                          <div class="input-group">
                            <label class="control-label">{{feature.Name}}</label>
                            <input class="form-control" type="text" v-model="action_obj.values[feature.ID_Feature]">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-1" v-if="action_obj.id_action > 0">
                        <div class="input-group">
                          <label class="control-label"><?= $this->language->getLabel("qty"); ?></label>
                          <input class="form-control" type="text" v-model="action_obj.qty">
                        </div>
                      </div>
                      <div class="col-md-2" v-if="action_obj.id_product == 0 && action_obj.id_action != 0 && action_obj.id_action <= 2">
                        <button type="button" class="btn btn-info col-md-12" v-on:click="linkProduct(index, action_id)">
                          <span class="fa fa-chain">Link Product</span>
                        </button>
                      </div>
                      <div class="col-md-1">
                        <button type="button" class="btn btn-danger col-md-12" v-on:click="delAction(index, action_id)">
                          <span class="fa fa-trash"></span>
                        </button>
                      </div>
                    </div>