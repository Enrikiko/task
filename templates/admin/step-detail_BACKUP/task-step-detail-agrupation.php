                    <div v-if="action == 'A'">
                      <div class="col-md-8">
                        <div class="input-group">
                          <label class="control-label"><?= $this->language->getLabel("product-need"); ?></label>
                          <select class="form-control" v-model="action_obj.id_product">
                            <option value=""><?= $this->language->getLabel("select-product"); ?></option>
                            <option v-for="(product, id_product) in products" :value="id_product">{{product.Name}}</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="input-group">
                          <label class="control-label"><?= $this->language->getLabel("qty"); ?></label>
                          <input class="form-control" type="text" v-model="action_obj.qty">
                        </div>
                      </div>
                      <div class="col-md-1">
                        <button type="button" class="btn btn-danger col-md-12" v-on:click="delAction(index, action_id)">
                          <span class="fa fa-trash"></span>
                        </button>
                      </div>
                    </div>