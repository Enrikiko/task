<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["n_locations"]) { ?>         
            <table class="table table-striped" id="delete">
                <thead>
                    <tr>
                        <th><?= $this->language->getLabel("s-workshop"); ?></th>
                        <th><?= $this->language->getLabel("name"); ?></th>
                        <th><?= $this->language->getLabel("description"); ?></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                  <? foreach ($this->data["workshop"] as $workshop) { ?>
                    <? if (count($workshop["locations"]) > 0 ) { ?>
                      <tr><td rowspan="<?= count($workshop["locations"]); ?>"><b><?= $workshop["name"]; ?></b></td>
                      <? foreach ($workshop["locations"] as $location) { ?>
                          <td><a href="/admin/location/<?= $location["ID_Location"]; ?>"><?= $location["Name_location"]; ?></a></td>
                          <td><a href="/admin/location/<?= $location["ID_Location"]; ?>"><?= $location["Description_location"]; ?></a></td>
                          <td style="float:right;">
                            <a href="/admin/location/<?= $location["ID_Location"]; ?>">
                              <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                            </a>
                            <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $location["Name_location"]; ?>','<?= $location["ID_Location"]; ?>')">
                                <?= $this->language->getLabel("cmd-delete"); ?>
                                <span class="fa fa-trash"></span>
                            </button>
                          </td>
                        </tr><tr>
                      <? } ?>
                    <? } ?>
                  <? } ?>
                </tbody>
            </table>
            <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-location"); ?>
      <? } ?>
    </div>
  </div>
  <? include_once "./templates/_warning.php"; ?>
</form>