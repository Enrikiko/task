<form class="" action="/admin/<?= $this->data["page"]; ?>/<?= $this->request["id"]; ?>" method="post">
 	<div class="block">
 		<div class="col-md-6">
			<h1>
				<b>
					<? if ($this->request["id"]) { ?>
						<?= $this->language->getLabel("s-projects"); ?>
					<? } else { ?>
						<?= $this->language->getLabel("cmd-new-projects"); ?>
					<? } ?>
				</b>
			</h1>
	    </div>
    	<div class="block-content">
    		<div class="col-md-12">
          <div class="input-group">
            <label class="control-label"><?= $this->language->getLabel("name"); ?></label>
            <input class="form-control" name="Name" type="text" value="<?= $this->getValue($this->data["project"],"Name"); ?>" required>
          </div>
          <div class="input-group">
            <label class="control-label"><?= $this->language->getLabel("task"); ?></label>
            <table class="table table-striped">
            	<thead>
            		<tr>
            			<th><?= $this->language->getLabel("Name"); ?></th>
            			<th><?= $this->language->getLabel("Description"); ?></th>
            			<th><?= $this->language->getLabel("in-project"); ?></th>
            		</tr>
            	</thead>
            	<tbody>
            		<? foreach ($this->data["tasks"] as $key => $task) { ?>
	            		<tr>
	            			<td><?= $task["Name"]; ?></td>
	            			<td><?= $task["Description"]; ?></td>
	            			<td>
		                  <div class="input-group">	
		                    <div class="app-radio inline">
		                        <label><input name="in_project[<?= $task["ID_Task"]; ?>]" value="1" type="radio" <?= ($task["in_project"] != 0 && $task["in_project"] == $this->request["id"] ? "checked" : "" ); ?>><?= $this->language->getLabel("Yes"); ?><span></span></label>
		                    </div>
		                    <div class="app-radio inline">
		                        <label><input name="in_project[<?= $task["ID_Task"]; ?>]" value="0" type="radio" <?= ($task["in_project"] == 0 || $task["in_project"] != $this->request["id"]  ? "checked" : "" ); ?>><?= $this->language->getLabel("No"); ?><span></span></label>
		                    </div>
		                  </div>
		                </td>
	            		</tr>
            		<? } ?>
            	</tbody>
            </table>
          </div>
      	</div>
    	</div>
    	<div class="block-content btn-group" id="delete">
		<? if ($this->request["id"]) { ?>
			<button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["project"]["Name"]; ?>','<?= $this->request["id"]; ?>')">
				<span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
			</button>
			<button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["project"]["ID_Project"]; ?>" style="float:right;">
				<span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
			</button>
		<? } else { ?>
			<button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
				<span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
			</button>
			<? } ?>
		</div>
	</div>
</form>