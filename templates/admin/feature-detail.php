<form class="" action="/admin/<?= $this->data["page"]; ?>/<?= $this->request["id"]; ?>" method="post">
  <div class="block">
    <div class="col-md-6">
      <h1>
        <b>
          <? if ($this->request["id"]) { ?>
            <?= $this->language->getLabel("s-feature"); ?>
          <? } else { ?>
            <?= $this->language->getLabel("cmd-new-feature"); ?>
          <? } ?>
        </b>
      </h1>
    </div>
    <div class="block-content">
      <div class="input-group">
        <label class="control-label"><?= $this->language->getLabel("description"); ?></label>
        <input class="form-control" name="Name" type="text" value="<?= $this->getValue($this->data["feature"],"Name"); ?>" required>
      </div>
      <div class="input-group">
        <label class="control-label"><?= $this->language->getLabel("assign-type"); ?></label>
        <select class="form-control" name="Unity" required>
          <option value=""><?= $this->language->getLabel("select-type"); ?></option>
          <? foreach ($this->data["unity"] as $key => $unity) { ?>
            <option value="<?= $unity; ?>" <?= ( $unity == $this->getValue($this->data["feature"],"Unity") ? "selected=\"selected\"" : "" ); ?> >
              <?= $this->language->getLabel($unity); ?>
            </option>
          <? } ?>
        </select>
      </div>
    </div>
    <div class="block-content btn-group" id="delete">
      <? if ($this->request["id"]) { ?>
        <button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["feature"]["Name"]; ?>','<?= $this->request["id"]; ?>')">
            <span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
        </button>
        <button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["feature"]["ID_Feature"]; ?>" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
        </button>
      <? } else { ?>
        <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
        </button>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>