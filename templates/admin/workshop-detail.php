<form class="" action="/admin/workshop/<?= $this->request["id"]; ?>" method="post">
  <div class="block" id="employee">
    <div class="col-md-6">
      <h1>
        <b>
          <? if ($this->request["id"]) { ?>
            <?= $this->language->getLabel("s-workshop"); ?>
          <? } else { ?>
            <?= $this->language->getLabel("cmd-new-workshop"); ?>
          <? } ?>
        </b>
      </h1>
    </div>
    <div class="block-content">
      <div class="input-group">
        <label class="control-label"><?= $this->language->getLabel("name"); ?></label>
        <input class="form-control" name="name" type="text" value="<?= $this->getValue($this->data["workshop"],"name"); ?>" required>
      </div>
      <div class="input-group">
        <label class="control-label"><?= $this->language->getLabel("address"); ?></label>
        <input class="form-control" name="address" type="text" value="<?= $this->getValue($this->data["workshop"],"address"); ?>" required>
      </div>
      <div class="input-group">
        <label class="control-label"><?= $this->language->getLabel("phone"); ?></label>
        <input class="form-control" name="phone" type="text" value="<?= $this->getValue($this->data["workshop"],"phone"); ?>" required>
      </div>
      <div class="block">
        <div class="col-md-12">
          <? if ($this->getValue($this->data["workshop"],"Image")) { ?>
            <div class="col-md-6">                              
              <div class="title">
                <h4><?= $this->language->getLabel("image"); ?>:</h4>
              </div>   
              <img src="<?= $this->getValue($this->data["workshop"],"Image"); ?>">
            </div>
          <? } ?>
          <div class="col-md-6">                               
            <div class="title">
              <h4><?= $this->language->getLabel("upload-file"); ?>:</h4>
            </div> 
            <div action="/ajax/dropzone?q=<?= $this->request["id"]; ?>&f=workshop" enctype="multipart/form-data" class="dropzone dropzone-tiny"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-content btn-group" id="delete">
      <? if ($this->request["id"]) { ?>
        <button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["workshop"]["name"]; ?>','<?= $this->request["id"]; ?>')">
            <span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
        </button>
        <button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["workshop"]["ID_workshop"]; ?>" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
        </button>
      <? } else { ?>
        <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
        </button>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>

<script type="text/javascript" src="/assets/boooya/js/vendor/dropzone/dropzone.js"></script>