<form action="/admin/<?= $this->data["page"]; ?>" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["projects"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th class="col-md-1">&nbsp;</th>
                    <th><?= $this->language->getLabel("description"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["projects"] as $project) { ?>
                <tr>
                  <td class="col-md-1">
                    <button class="btn btn-success btn-clean" name="startProject" value="<?= $project["ID_Project"]; ?>"><?= $this->language->getLabel("enabled-project"); ?> <span class="fa fa-play-circle-o"></span></button>
                  </td>
                  <td><a href="/admin/<?= $this->data["page"]; ?>/<?= $project["ID_Project"]; ?>"><?= $project["Name"]; ?></a></td>
                  <td style="float:right;">
                    <a href="/admin/taskflow/<?= $project["ID_Project"]; ?>">
                      <button type="button" class="btn btn-info btn-clean"><?= $this->language->getLabel("flow"); ?> <span class="fa fa-code-fork"></span></button>
                    </a>
                    <a href="/admin/<?= $this->data["page"]; ?>/<?= $project["ID_Project"]; ?>">
                      <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                    </a>
                    <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $project["Name"]; ?>','<?= $project["ID_Project"]; ?>')">
                        <?= $this->language->getLabel("cmd-delete"); ?>
                        <span class="fa fa-trash"></span>
                    </button>
                  </td>
                </tr>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-projects"); ?>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>
