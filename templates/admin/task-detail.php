<form class="" action="/admin/task/<?= $this->request["id"]; ?>" method="post">
  <div class="block">
    <div class="col-md-6">
      <h1>
        <b>
          <? if ($this->request["id"]) { ?>
            <?= $this->language->getLabel("s-task"); ?>
          <? } else { ?>
            <?= $this->language->getLabel("cmd-new-task"); ?>
          <? } ?>
        </b>
      </h1>
    </div>
    <div class="block-content" id="Task">
      <div class="block" style="padding: 0px;">
        <ul class="nav nav-pills nav-pills-bordered nav-justified">
            <li class="active"><a href="#general" data-toggle="tab"><?= $this->language->getLabel("detail-task"); ?></a></li>
            <li><a href="#detail-steps" data-toggle="tab"><?= $this->language->getLabel("manage-steps"); ?></a></li>
        </ul>
      </div>
      <div class="tab-content">
          <div class="tab-pane active" id="general">
            <div class="col-md-12">
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("s-family"); ?></label>
                <select class="form-control" name="ID_Family">
                  <option value=""><?= $this->language->getLabel("select-family"); ?></option>
                  <? foreach ($this->data["family"] as $id_family => $family) { ?>
                    <option value="<?= $id_family; ?>" <?=  ($id_family == $this->getValue($this->data["task"],"ID_Family" ) ? "selected=\"selected\"" : "" ); ?>>
                      <?= $family["Description"]; ?>
                    </option>
                  <? } ?>
                </select>
              </div>
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("name"); ?></label>
                <input class="form-control" name="Name" type="text" value="<?= $this->getValue($this->data["task"],"Name"); ?>">
              </div>
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("description"); ?></label>
                <input class="form-control" name="Description" type="text" value="<?= $this->getValue($this->data["task"],"Description"); ?>">
              </div>
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("observation"); ?></label>
                <input class="form-control" name="Observations" type="text" value="<?= $this->getValue($this->data["task"],"Observations"); ?>">
              </div>
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel("task-hour"); ?></label>
                <input class="form-control" name="Hours" type="text" value="<?= $this->getValue($this->data["task"],"Hours"); ?>">
              </div>
              <div class="col-md-6">
                <div class="input-group">
                  <br>
                  <label><?= $this->language->getLabel("task-rest-need"); ?></label>
                  <br>
                  <div class="app-radio inline">
                      <label><input name="Rest_need" value="1" type="radio" <?= ($this->getValue($this->data["task"],"Rest_need") == 1 ? "checked" : "" ); ?>><?= $this->language->getLabel("Yes"); ?><span></span></label>
                  </div>
                  <div class="app-radio inline">
                      <label><input name="Rest_need" value="0" type="radio" <?= ($this->getValue($this->data["task"],"Rest_need") == 0 ? "checked" : "" ); ?>><?= $this->language->getLabel("No"); ?><span></span></label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <br>
                <div class="input-group">
                  <label><?= $this->language->getLabel("task-rest-result"); ?></label>
                  <br>
                  <div class="app-radio inline">
                      <label><input name="Rest_result" value="1" type="radio" <?= ($this->getValue($this->data["task"],"Rest_result") == 1 ? "checked" : "" ); ?>><?= $this->language->getLabel("Yes"); ?><span></span></label>
                  </div>
                  <div class="app-radio inline">
                      <label><input name="Rest_result" value="0" type="radio" <?= ($this->getValue($this->data["task"],"Rest_result") == 0 ? "checked" : "" ); ?>><?= $this->language->getLabel("No"); ?><span></span></label>
                  </div>
                </div>
              </div>
              <div class="input-group">
                <label><?= $this->language->getLabel("task-partial"); ?></label>
                <br>
                <div class="app-radio inline">
                    <label><input name="Partial" value="1" type="radio" <?= ($this->getValue($this->data["task"],"Partial") == 1 ? "checked" : "" ); ?>><?= $this->language->getLabel("Yes"); ?><span></span></label>
                </div>
                <div class="app-radio inline">
                    <label><input name="Partial" value="0" type="radio" <?= ($this->getValue($this->data["task"],"Partial") == 0 ? "checked" : "" ); ?>><?= $this->language->getLabel("No"); ?><span></span></label>
                </div>
              </div>
            </div>
          </div>

          <div class="tab-pane" id="detail-steps">

                  {{steps}}
            <input type="text" hidden="hidden" name="steps" v-model="getJSON()">
            <div class="col-md-12">
              <h4><b><?= $this->language->getLabel("steps"); ?></b></h4>
              <div class="block" v-for="(step, index) in steps" v-if="step.del != '1'">
                <div class="col-md-12">
                  <div class="col-md-2">
                    <div class="input-group">
                      <label><?= $this->language->getLabel("s-type"); ?></label>
                      <select class="form-control" v-model="step.type" v-on:change="isOptimize(index)">
                          <option value="0"><?= $this->language->getLabel("default"); ?></option>
                          <option value="optimizeCut"><?= $this->language->getLabel("optimize-cut"); ?></option>
                          <option value="agrupation"><?= $this->language->getLabel("agrupation"); ?></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-9" v-if="step.type == '0' && (step.actions[0].id_action== '0' || step.actions[0].type == 'T')">
                    <div class="input-group">
                      <label><?= $this->language->getLabel("action"); ?></label>
                      <select class="form-control" v-model="step.actions[0].id_action" v-on:change="addActionValues(index, actions[step.actions[0].id_action].Type, actions[step.actions[0].id_action].Features)">
                          <option value="0"><?= $this->language->getLabel("select-action"); ?></option>
                          <? foreach ($this->data["actions_steps"] as $id_action => $action) { ?>
                              <option value="<?= $id_action; ?>"><?= $this->language->getLabel("action-" . $action["Name"]); ?></option>
                          <? } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-9" v-else>
                    <div class="col-md-9">
                      <div class="input-group">
                        <label><?= $this->language->getLabel("s-product"); ?></label>
                        <select class="form-control" v-model="step.product">
                            <option value="0"><?= $this->language->getLabel("select-product"); ?></option>
                            <option v-for="(product, id_product) in products" :value="id_product">{{product.Name}}</option>
                            <option v-for="product in products_created" :value="product.ID_Product">{{product.Name}}</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <label class="control-label"><?= $this->language->getLabel("qty"); ?></label>
                        <input class="form-control" type="text" v-model="step.qty">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-1" style="float: right;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" v-on:click="delStep(index)">
                      <span class="fa fa-times"></span>
                    </button>
                  </div>
                </div>

                <div v-if="step.type == 'agrupation' || step.actions[0].id_action != '0'">
                  <h5><b><?= $this->language->getLabel("Actions"); ?></b></h5>
                  <div class="col-md-12" v-for="(action_obj, action_id ) in step.actions" v-if="action_obj.del != '1'">
                    <div class="col-md-3" v-if="step.type != 'optimizeCut'">
                      <div class="input-group" v-if="step.actions[0].type == 'D'">
                        <label><?= $this->language->getLabel("action"); ?></label>
                        <select class="form-control" v-model="action_obj.id_action">
                            <option value="0" v-if="step.type != 'optimizeCut'"><?= $this->language->getLabel("select-action"); ?></option>
                            <? foreach ($this->data["actions_steps"] as $id_action => $action) { ?>
                              <? if ($action["Type"] == "D") {  ?>
                                <option value="<?= $id_action; ?>"><?= $this->language->getLabel("action-" . $action["Name"]); ?></option>
                              <? } ?>
                            <? } ?>
                        </select>
                      </div>
                      <div class="input-group" v-if="step.type == 'agrupation' || step.actions[0].type == 'T'">
                        <label class="control-label"><?= $this->language->getLabel("description"); ?></label>
                        <input class="form-control" type="text" v-model="action_obj.description">
                      </div>
                    </div>

                    <div class="col-md-auto">
                      <div v-if="step.type == 'optimizeCut' || action_obj.type == 'D'">
                        <div v-for="feature in actions[action_obj.id_action].Obj_Features" class="col-md-2">
                          <div class="input-group">
                            <label class="control-label">{{feature.Name}}</label>
                            <input class="form-control" type="text" v-model="action_obj.values[feature.ID_Feature]">
                          </div>
                        </div>
                      </div>
                      <div v-else class="col-md-6" >
                        <div class="input-group">
                          <label><?= $this->language->getLabel("s-product"); ?></label>
                          <select class="form-control" v-model="action_obj.id_product">
                              <option value="0"><?= $this->language->getLabel("select-product"); ?></option>
                              <option v-for="(product, id_product) in products" :value="id_product">{{product.Name}}</option>
                              <option v-for="product in products_created" :value="product.ID_Product">{{product.Name}}</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="input-group">
                        <label class="control-label"><?= $this->language->getLabel("qty"); ?></label>
                        <input class="form-control" type="text" v-model="action_obj.qty">
                      </div>
                    </div>
                    <div class="col-md-1">
                      <button type="button" class="btn btn-danger col-md-12" v-on:click="delAction(index, action_id)">
                        <span class="fa fa-trash"></span>
                      </button>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <button type="button" class="btn btn-warning col-md-12" v-on:click="addAction(index)">
                      <span class="fa fa-plus"><?= $this->language->getLabel("cmd-add-action"); ?></span>
                    </button>
                  </div>
                </div>
              </div>
              <button  type="button" class="btn btn-warning col-md-12" v-on:click="addEmptyStep">
                <span class="fa fa-plus"></span> <?= $this->language->getLabel("cmd-add-step"); ?>
              </button>
            </div>
          </div>
      </div>
    </div>
    <div class="block-content btn-group" id="delete">
      <? if ($this->request["id"]) { ?>
        <button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["task"]["Name"]; ?>','<?= $this->request["id"]; ?>')">
            <span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
        </button>
        <button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["task"]["ID_Task"]; ?>" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
        </button>
      <? } else { ?>
        <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
        </button>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>

<script type="text/javascript">
  var php_steps = <?= json_encode($this->data["steps"]); ?>;
  var php_products = <?= json_encode($this->getValue($this->data,"products")); ?>;
  var php_product_id = <?= json_encode($this->getValue($this->data["task"],"ID_Product")); ?>;
  var php_actions = <?= json_encode($this->data["actions_steps"]); ?>;

  initTask();
</script>