<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["historic"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th><?= $this->language->getLabel("description"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["historic"] as $historic) { ?>
                <tr>
                  <td><a href="/admin/historic/<?= $historic["ID_Historic"]; ?>"><?= $historic["Description"]; ?></a></td>
                  <td style="float:right;">
                    <a href="/admin/historic/<?= $historic["ID_Historic"]; ?>">
                      <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                    </a>
                    <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $historic["Description"]; ?>','<?= $historic["ID_Historic"]; ?>')">
                        <?= $this->language->getLabel("cmd-delete"); ?>
                        <span class="fa fa-trash"></span>
                    </button>
                  </td>
                </tr>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-historic"); ?>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>
