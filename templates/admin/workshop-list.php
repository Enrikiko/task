<form action="/admin/<?= $this->data["page"]; ?>/" method="post">
  <? include_once "./templates/_search.php"; ?>

  <div class="block">
    <? include_once "./templates/admin/_header-list.php"; ?>
    <div class="block-content">
      <? if ($this->data["workshop"]) { ?>
        <table class="table table-striped" id="delete">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th><?= $this->language->getLabel("name"); ?></th>
                    <th><?= $this->language->getLabel("address"); ?></th>
                    <th><?= $this->language->getLabel("phone"); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
              <? foreach ($this->data["workshop"] as $workshop) { ?>
                <tr>
                  <td>
                    <img style="max-width: 60px;" src="<?= $this->getValue($workshop,"Image"); ?>" alt="">
                  </td>
                  <td><a href="/admin/workshop/<?= $workshop["ID_workshop"]; ?>"><?= $workshop["name"]; ?></a></td>
                  <td><a href="/admin/workshop/<?= $workshop["ID_workshop"]; ?>"><?= $workshop["address"]; ?></a></td>
                  <td><a href="/admin/workshop/<?= $workshop["ID_workshop"]; ?>"><?= $workshop["phone"]; ?></a></td>
                  <td style="float:right;">
                    <a href="/admin/workshop/<?= $workshop["ID_workshop"]; ?>">
                      <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-edit"); ?> <span class="fa fa-edit"></span></button>
                    </a>
                    <button type="button" class="btn btn-danger btn-clean" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $workshop["name"]; ?>','<?= $workshop["ID_workshop"]; ?>')">
                        <?= $this->language->getLabel("cmd-delete"); ?>
                        <span class="fa fa-trash"></span>
                    </button>
                  </td>
                </tr>
              <? } ?>
            </tbody>
        </table>
        <? include_once "./templates/_pagination.php"; ?>
      <? } else { ?>
        <?= $this->language->getLabel("no-workshop"); ?>
      <? } ?>
    </div>
  </div>
  <? include_once "./templates/_warning.php"; ?>
</form>