<form class="" action="/admin/<?= $this->data["page"]; ?>/<?= $this->request["id"]; ?>" method="post">
  <div class="block">
    <div class="col-md-6">
      <h1>
        <b>
          <? if ($this->request["id"]) { ?>
            <?= $this->language->getLabel("s-product"); ?>
          <? } else { ?>
            <?= $this->language->getLabel("cmd-new-product"); ?>
          <? } ?>
        </b>
      </h1>
    </div>
    <div class="block-content" id="editProduct">
      <div class="col-md-12">
        <div class=" col-md-6">
          <h4><b><?= $this->language->getLabel("s-general"); ?></b></h4>
          <div class="input-group">
            <label class="control-label"><?= $this->language->getLabel("name"); ?></label>
            <input class="form-control" name="Name" type="text" value="<?= $this->getValue($this->data["product"],"Name"); ?>">
          </div>
          <div class="input-group">
            <label class="control-label"><?= $this->language->getLabel("description"); ?></label>
            <input class="form-control" name="Description" type="text" value="<?= $this->getValue($this->data["product"],"Description"); ?>">
          </div>
          <div class="input-group">
            <label class="control-label"><?= $this->language->getLabel("select-family"); ?></label>
            <select class="form-control" name="ID_Family" id="ID_Family" v-on:change="getFamilyFeatures()" required>
              <option value="0"><?= $this->language->getLabel("select-family"); ?></option>
              <? foreach ($this->data["family"] as $id_family => $family) { ?>
                <option value="<?= $id_family; ?>" <?= ( $id_family == $this->getValue($this->data["product"],"ID_Family") ? "selected=\"selected\"" : "" ); ?> >
                  <?= $family["Description"]; ?>
                </option>
              <? } ?>
            </select>
          </div>
          <div class="input-group">
            <label class="control-label">
              <input type="checkbox" name="Min_enabled" v-model="min_qty">
              <?= $this->language->getLabel("enable_quantity"); ?>
            </label>
            <div class="col-md-12" v-if="min_qty != 0">
            <label class="control-label"><?= $this->language->getLabel("min_quantity"); ?></label>
              <input class="form-control" name="Min_quantity" type="text" value="<?= $this->getValue($this->data["product"],"Min_quantity"); ?>" required>
            </div>
          </div>
          <div class="block">
            <div class="col-md-12">
              <? if ($this->getValue($this->data["product"],"Image")) { ?>
                <div class="col-md-6">                              
                  <div class="title">
                    <h4><?= $this->language->getLabel("image"); ?>:</h4>
                  </div>

                  <? if ($this->getValue($this->data["product"],"Image")) { ?>
                    <button class="btn btn-default preview" data-preview-image="<?= $this->getValue($this->data["product"],"Image"); ?>" data-preview-size="modal-lg">
                      <img style="max-width: 170px;" src="<?= $this->getValue($this->data["product"],"Image"); ?>">
                    </button>
                  <? } ?>
                </div>
              <? } ?>
              <div class="col-md-6">                               
                <div class="title">
                  <h4><?= $this->language->getLabel("upload-file"); ?>:</h4>
                </div> 
                <div action="/ajax/dropzone?q=<?= $this->request["id"]; ?>&f=product" enctype="multipart/form-data" class="dropzone dropzone-tiny"></div>
              </div>
            </div>
          </div>
        </div>
        <div class=" col-md-6" v-if="features.length">
          <h4><b><?= $this->language->getLabel("attribut"); ?></b></h4>
          <div class="input-group" v-for="feature in features">
            <label class="control-label">{{feature.Name}}</label>
            <input class="form-control" :name="'features[' + feature.ID_Feature + ']'" type="text" :value="features_product[feature.ID_Feature]">
          </div>
        </div>
      </div>
    </div>
    <div class="block-content btn-group" id="delete">
      <? if ($this->request["id"]) { ?>
        <button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["product"]["Name"]; ?>','<?= $this->request["id"]; ?>')">
            <span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
        </button>
        <button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["product"]["ID_Product"]; ?>" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
        </button>
      <? } else { ?>
        <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
        </button>
      <? } ?>
    </div>
  </div>
  
  <? include_once "./templates/_warning.php"; ?>
</form>

<? include_once "./templates/_showImage.php"; ?>

<script type="text/javascript" src="/assets/boooya/js/vendor/dropzone/dropzone.js"></script>
<script type="text/javascript">
  var php_min_qty = <?= ($this->getValue($this->data["product"],"Min_enabled") ? $this->getValue($this->data["product"],"Min_enabled") : 0 ); ?>;
  var php_features_product = <?= json_encode($this->getValue($this->data["product"],"Attributs")); ?>;
  if (php_features_product.length) {
    php_features_product = JSON.parse(php_features_product);
  }
  
  initProduct();
</script>