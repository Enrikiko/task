<div class="block">
	<form method="post" action="/settings">
		<div class="block-content">
			<div class="input-group">
		      <label class="control-label"><?= $this->language->getLabel("register-email"); ?></label>
		      <input class="form-control" name="email" type="text" value="<?= $this->getValue($this->data["user"],"email"); ?>" required>
		    </div>
		    <div class="input-group">
	          <label class="control-label"><?= $this->language->getLabel("register-company"); ?></label>
	          <input class="form-control" name="company_name" type="text" value="<?= $this->getValue($this->data["user"],"company_name"); ?>" required>
	        </div>
	        <label class="control-label"><?= $this->language->getLabel("register-image"); ?></label>
	        <div class="block">
	          <div class="col-md-12">
	            <? if ($this->getValue($this->data["user"],"Image")) { ?>
	              <div class="col-md-6">                              
	                <div class="title">
	                  <h4><?= $this->language->getLabel("logo"); ?>:</h4>
	                </div>   
	                <img src="<?= $this->getValue($this->data["user"],"Image"); ?>">
	              </div>
	            <? } ?>
	            <div class="col-md-6">                               
	              <div class="title">
	                <h4><?= $this->language->getLabel("upload-file"); ?>:</h4>
	              </div> 
	              <div action="/ajax/dropzone?q=<?= $_SESSION["ID_User"]; ?>&f=user" enctype="multipart/form-data" class="dropzone dropzone-tiny"></div>
	            </div>
	          </div>
	        </div>
		</div>
		<div class="block-content btn-group" style="float: right;">
		    <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
		      <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
		    </button>
	    </div>
    </form>
</div>

<script type="text/javascript" src="/assets/boooya/js/vendor/dropzone/dropzone.js"></script>