<div id="button-search">
  <div class="col-md-12" style="padding-top: 10px;">
    <button type="button" class="btn btn-success btn-icon-fixed" style="margin-right: 30px;" v-on:click="enableSearch">
      <span class="fa fa-search"></span> <?= $this->language->getLabel("cmd-search"); ?>
    </button>

    <? if (!array_key_exists("no_create", $this->data)) { ?>
      <a href="/admin/<?= $this->data["page"]; ?>/0">
        <button type="button" class="btn btn-info btn-icon-fixed">
          <span class="fa fa-plus"></span> <?= $this->language->getLabel("cmd-new-" . $this->data["page"]); ?>
        </button>
      </a>
    <? } ?>
  </div>
  <div class="col-md-12">
    <h1 style="margin-bottom: 0px; margin-top: 20px;"><b><?= $this->language->getLabel($this->data["page"]); ?></b></h1>
  </div>
</div>

<?
  $count_Post = 0;
  if (array_key_exists("limit", $_POST)) {
    $count_Post++;
  }

  if (array_key_exists("del", $_POST)) {
    $count_Post++; 
  }
?>

<script type="text/javascript">
  var php_show = <?= (count($_POST) > $count_Post ? 'true' : 'false'); ?>;
  initSearch();
</script>