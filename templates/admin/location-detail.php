<form class="" action="/admin/location/<?= $this->request["id"]; ?>" method="post">
  <div class="block">
    <div class="col-md-6">
      <h1>
        <b>
          <? if ($this->request["id"]) { ?>
            <?= $this->language->getLabel("s-location"); ?>
          <? } else { ?>
            <?= $this->language->getLabel("cmd-new-location"); ?>
          <? } ?>
        </b>
      </h1>
    </div>
    <div class="block-content">
      <div class="input-group">
        <label class="control-label"><?= $this->language->getLabel("assign-workshop"); ?></label>
        <select class="form-control" name="ID_workshop" required>
          <option value=""><?= $this->language->getLabel("select-workshop"); ?></option>
          <? foreach ($this->data["workshop"] as $key => $workshop) { ?>
            <option value="<?= $workshop["ID_workshop"]; ?>" <?= ( $workshop["ID_workshop"] == $this->getValue($this->data["location"],"ID_workshop") ? "selected=\"selected\"" : "" ); ?> >
              <?= $workshop["name"]; ?>
            </option>
          <? } ?>
        </select>
        <div class="input-group">
          <label class="control-label"><?= $this->language->getLabel("name"); ?></label>
          <input class="form-control" name="Name_location" type="text" value="<?= $this->getValue($this->data["location"],"Name_location"); ?>" required>
        </div>
          <div class="input-group">
            <label class="control-label"><?= $this->language->getLabel("description"); ?></label>
            <input class="form-control" name="Description_location" type="text" value="<?= $this->getValue($this->data["location"],"Description_location"); ?>" required>
          </div>
      </div>
    </div>
    <div class="block-content btn-group" id="delete">
      <? if ($this->request["id"]) { ?>
        <button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["location"]["Description_location"]; ?>','<?= $this->request["id"]; ?>')">
            <span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
        </button>
        <button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["location"]["ID_Location"]; ?>" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
        </button>
      <? } else { ?>
        <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
        </button>
      <? } ?>
    </div>
  </div>

  <? include_once "./templates/_warning.php"; ?>
</form>