<form class="" action="/admin/employee/<?= $this->request["id"]; ?>" method="post">
  <div class="block">
    <div class="col-md-6">
      <h1>
        <b>
          <? if ($this->request["id"]) { ?>
            <?= $this->language->getLabel("s-employee"); ?>
          <? } else { ?>
            <?= $this->language->getLabel("cmd-new-employee"); ?>
          <? } ?>
        </b>
      </h1>
    </div>
    <div class="block-content" id="employee">
      <div class="block" style="padding: 0px;">
        <ul class="nav nav-pills nav-pills-bordered nav-justified">
            <li class="active"><a href="#general" data-toggle="tab"><?= $this->language->getLabel("detail-employee"); ?></a></li>
            <li><a href="#asign-task2" data-toggle="tab"><?= $this->language->getLabel("assign-employee"); ?></a></li>
        </ul>
      </div>

      <div class="tab-content">
          <div class="tab-pane active" id="general">
            <div class="col-md-12">
                <div class="input-group">
                  <label class="control-label"><?= $this->language->getLabel("name"); ?></label>
                  <input class="form-control" name="Name" type="text" value="<?= $this->getValue($this->data["employee"],"Name"); ?>" required>
                </div>
                <div class="form-group">
                  <label class="control-label"><?= $this->language->getLabel("schedule"); ?></label><br>
                  <div class="col-md-12">
                    <label>
                      <input type="checkbox" id="checkbox" v-model="checked">
                      <?= $this->language->getLabel("intensive-workday"); ?>
                    </label>
                  </div>
                  <div class="form-group">
                    <div class="col-md-1">
                      <label v-if="!checked"><?= $this->language->getLabel("morning"); ?>:</label>
                    </div>
                    <div class="col-md-3">
                      <input class="form-control" name="since[m]" placeholder="HH:mm" value="<?= $this->getValue($this->data["employee"],array("Horario","since","m")); ?>" required>
                    </div>
                    <div class="col-md-3">
                      <input class="form-control" name="until[m]" placeholder="HH:mm" value="<?= $this->getValue($this->data["employee"],array("Horario","until","m")); ?>" required>
                    </div>
                  </div>
                  <div class="form-group" v-if="!checked">
                    <div class="col-md-1">
                      <label><?= $this->language->getLabel("afternoon"); ?>:</label>
                    </div>
                    <div class="col-md-3">
                      <input class="form-control" name="since[a]" placeholder="HH:mm" value="<?= $this->getValue($this->data["employee"],array("Horario","since","a")); ?>" required>
                    </div>
                    <div class="col-md-3">
                      <input class="form-control" name="until[a]" placeholder="HH:mm" value="<?= $this->getValue($this->data["employee"],array("Horario","until","a")); ?>" required>
                    </div>
                  </div>
                </div>
                <div class="input-group">
                  <label class="control-label"><?= $this->language->getLabel("select-rol"); ?></label>
                  <select class="form-control" name="ID_Role" required>
                    <? if (array_key_exists("employee", $_SESSION)) { ?>
                      <option value="0"><?= $this->language->getLabel("select-rol"); ?></option>
                      <? foreach ($this->data["role"] as $key => $role) { ?>
                        <option value="<?= $role["ID_Role"]; ?>" <?= ( $role["ID_Role"] == $this->getValue($this->data["employee"],"ID_Role") ? "selected=\"selected\"" : "" ); ?> >
                          <?= $role["Description"]; ?>
                        </option>
                      <? } ?>
                    <? } else { ?>
                      <? foreach ($this->data["role"] as $key => $role) { ?>
                        <? if ($role["Role"] == "Admin") { ?>
                          <option value="<?= $role["ID_Role"]; ?>" <?= ( $role["ID_Role"] == $this->getValue($this->data["employee"],"ID_Role") ? "selected=\"selected\"" : "" ); ?> >
                            <?= $role["Description"]; ?>
                          </option>
                        <? } ?>
                      <? } ?>
                    <? } ?>
                  </select>
                </div>
                <div class="block">
                  <div class="col-md-12">
                    <? if ($this->getValue($this->data["employee"],"Image")) { ?>
                      <div class="col-md-6">                              
                        <div class="title">
                          <h4><?= $this->language->getLabel("image"); ?>:</h4>
                        </div>   
                        <img src="<?= $this->getValue($this->data["employee"],"Image"); ?>">
                      </div>
                    <? } ?>
                    <div class="col-md-6">                               
                      <div class="title">
                        <h4><?= $this->language->getLabel("upload-file"); ?>:</h4>
                      </div> 
                      <div action="/ajax/dropzone?q=<?= $this->request["id"]; ?>&f=employee" enctype="multipart/form-data" class="dropzone dropzone-tiny"></div>
                    </div>
                  </div>
                </div>
            </div>
          </div>

          <div class="tab-pane" id="asign-task2">
            <div class="block">
              <h4 style="padding-top: 10px;"><b><?= $this->language->getLabel("list-group-task"); ?></b></h4>
                <div class="app-accordion" data-open="close-other">
                  <div class="item" v-for="(group, id_group) in tasks">
                      <div class="heading">
                          <div class="title">{{group.Description}}</div>
                      </div>
                      <div class="content" style="height: auto;">
                        <div class="block">
                          <div class="col-md-9"><h5 style="padding-top: 10px;"><b><?= $this->language->getLabel("list-task"); ?></b></h5></div>
                          <div class="col-md-3" style="margin-top: 5px;"><button type="button" style="float: right;" class="btn btn-warning btn-icon-fixed" v-on:click="setAllCheck(id_group)"><span class="fa fa-check-square-o"></span> Select_all_group</button></div>
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Description</th>
                                <th>&nbsp;</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr v-for="task in group.tasks">
                                <td>{{task.Name}} ({{task.Description}})</td>
                                <td><input type="checkbox" :id="'check_' + task.ID_Task" :name="'group_' + id_group" :checked="in_assing_task(task.ID_Task)" v-on:change="setAssign(task.ID_Task)" :value="task.ID_Task"></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                </div>
              <input type="text" hidden="hidden" name="tasks" v-model="getJSON()">
            </div>
          </div>
      </div>
    </div>
    <div class="block-content btn-group" id="delete">
      <? if ($this->request["id"]) { ?>
        <button type="button" class="btn btn-danger btn-icon-fixed" data-toggle="modal" data-target="#modal-warning" v-on:click="setName('<?= $this->data["employee"]["Name"]; ?>','<?= $this->request["id"]; ?>')">
            <span class="fa fa-trash"></span><?= $this->language->getLabel("cmd-delete"); ?>
        </button>
        <button class="btn btn-success btn-icon-fixed" name="cmd-overwrite" value="<?= $this->data["employee"]["ID_Employee"]; ?>" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-overwrite"); ?>
        </button>
      <? } else { ?>
        <button class="btn btn-success btn-icon-fixed" name="cmd-save" style="float:right;">
          <span class="fa fa-floppy-o"></span> <?= $this->language->getLabel("cmd-save"); ?>
        </button>
      <? } ?>
    </div>
  </div>
  <? include_once "./templates/_warning.php"; ?>
</form>

<script type="text/javascript" src="/assets/boooya/js/vendor/dropzone/dropzone.js"></script>
<script type="text/javascript">
  var intensive_workday =  <?= json_encode($this->request["id"] && !$this->getValue($this->data["employee"],array("Horario","since","a"))); ?>;
  var php_tasks = <?= json_encode($this->data["tasks"]); ?>;
  var php_assign = <?= ($this->getValue($this->data["employee"],"Tasks")? $this->getValue($this->data["employee"],"Tasks") : json_encode(array())); ?>;
  initAdminEmployee();
  
  $('input[name="since[m]"]').mask('00:00');
  $('input[name="since[a]"]').mask('00:00');
  $('input[name="until[m]"]').mask('00:00');
  $('input[name="until[a]"]').mask('00:00');
</script>