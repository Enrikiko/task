<div class="block" id="list_steps">
	<form action="" method="post">
		<div class="col-md-6">
			<h1>
				<b>
					<?= $this->language->getLabel("s-task"); ?>: <?= $this->data["task"]["Name"]; ?>
				</b>
			</h1>
		</div>
		<div class="col-md-6">
			<? if ($this->data["historic"]["Status"] == "C") { ?>
				<button style="float: right;" class="btn btn-success" name="action" value="S">
					<span class="fa fa-play-circle"></span>
					<?= $this->language->getLabel("cmd-start"); ?>
				</button>
			<? } else { ?>
				<button style="float: right;" class="btn btn-success" name="action" value="F">
					<span class="fa fa-save"></span>
					<?= $this->language->getLabel("end-task"); ?>
				</button>
			<? } ?>
		</div>
		<div class="col-md-12">
			<input type="text" name="historic" hidden="" v-model="getJSON(historic)">
			<input type="text" name="steps" hidden="" v-model="getJSON(historic)">
			<div v-for="(step, key_step) in steps">
				<? include_once "./templates/task/division.php"; ?>
				<? include_once "./templates/task/agrupation.php"; ?>
				<? include_once "./templates/task/transformation.php"; ?>
			</div>
		</div>
	</form>
</div>

<? include_once "./templates/_showImage.php"; ?>

<script type="text/javascript">
	var php_steps = <?= json_encode($this->data["steps"]); ?>;
	var php_stock = <?= json_encode($this->data["stock"]); ?>;
	var php_historic = <?= json_encode($this->data["historic"]["Steps"]); ?>;
	var php_status = <?= json_encode($this->data["historic"]["Status"]); ?>;
	var php_id_historic = <?= json_encode($this->data["historic"]["ID_Historic"]); ?>;

	vue_steps = initSteps();

	vue_steps.addLabel('action_1', '<?= $this->language->getLabel("action_1"); ?>');
	vue_steps.addLabel('action_2', '<?= $this->language->getLabel("action_2"); ?>');
	vue_steps.addLabel('action_3', '<?= $this->language->getLabel("action_3"); ?>');
</script>