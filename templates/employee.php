<div id="employee">
  <div class="block">
    <? if (count($this->data["employees_disc"]) || count($this->data["employees_connec"])) { ?>
      <div class="form-group">
        <div class="col-md-6">
            <div class="col-md-10">
              <label>
                <?= $this->language->getLabel("employee"); ?>:
              </label>
                <select class="form-control" id="select_employees">
                  <option value="-1"><?= $this->language->getLabel("select-employee"); ?></option>
                  <option v-for="(employee, index) in employees_disc" :id="index" :value="index">{{employee.Name}}</option>
              </select>
            </div>
            <div class="col-md-2">
              <button class="btn btn-success btn-icon-fixed" v-on:click="connect"><?= $this->language->getLabel("cmd-login"); ?>
                <span class="fa fa-user-plus"></span>
              </button>
            </div>
        </div>
      </div>
      <div class="col-md-4 col-xs-6" v-for="(employee, index) in employees_connec">
        <div class="block block-condensed padding-top-20">
          <div class="contact contact-single">
            <img :src="employee.Image" alt="" style="width:100px">
            <div class="contact-container">
              <b>{{employee.Name}}</b>
            </div>
          </div>
          <div class="block-content padding-top-20">
            <div class="row">
              <div class="col-md-6">
                <form class="" action="task" method="post">
                  <button class="btn btn-success btn-clean btn-block" name="employee" :value="employee.ID_Employee">
                    <?= $this->language->getLabel("cmd-enter"); ?>
                    <span class="fa fa-sign-in"></span>
                  </button>
                </form>
              </div>
              <div class="col-md-6">
                <button class="btn btn-danger btn-clean btn-block" v-on:click="disconnect(index)">
                  <?= $this->language->getLabel("cmd-disconect"); ?>
                  <span class="fa fa-power-off"></span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    <? } else { ?>
      <h2><?= $this->language->getLabel("no-employees"); ?></h2>
      <a href="/admin/employee/0">
        <button name="workshop" value="<?= $workshop["ID_workshop"]; ?>" class="btn btn-info btn-icon-fixed">
          <?= $this->language->getLabel("cmd-new-employee"); ?>
          <span class="fa fa-plus"></span>
        </button>
      </a>
    <? } ?>
  </div>
</div>
<script type="text/javascript">
  var disc_employees = <?= json_encode($this->data["employees_disc"]); ?>;
  var connec_employees = <?= json_encode($this->data["employees_connec"]); ?>;
  initEmployee();
</script>
