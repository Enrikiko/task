
			<div class="app-accordion" data-open="close-other" v-if="step.Type == 'optimizeCut'">
				<div class="item"  v-if="!finishStep(step.ID_Step)">
					<div class="heading" style="background: white;">
						<div class="title col-md-12">
							<div class="col-md-6">
								{{step.Product.Name}}
							</div>
							<div class="col-md-6" style="float: left;text-align: end;">
								Stock necesario: {{step.Qty}}
							</div>
							<!--<div class="col-md-2" style="float: right;">
                                <div class="progress" style="margin: 15px;">
	                                <div class="progress-bar progress-bar-success progress-bar-striped progress-lg" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;color: black;">80%</div>
	                            </div>
                            </div>-->
						</div>
					</div>
					<div class="content" style="height: auto;">
						<div style="padding: 5px;">
	                        <ul class="nav nav-tabs nav-justified">
	                            <li><a :href="'#locations-' + step.ID_Step" data-toggle="tab"><?= $this->language->getLabel("location"); ?></a></li>
	                            <li class="active" v-if="status == 'S'"><a :href="'#actions-' + step.ID_Step" data-toggle="tab">Acciones</a></li>
	                        </ul>
	                        <div class="tab-content tab-content-bordered">
	                            <div class="tab-pane" :id="'locations-' + step.ID_Step">
									<div class="col-md-12" v-for="(location, index) in stock[step.Product.ID_Product]">
										<h5><b><?= $this->language->getLabel("s-location"); ?> - {{location.Name_location}}</b></h5>
										<table class="table">
											<thead>
												<th><?= $this->language->getLabel("feature"); ?></th>
												<th><?= $this->language->getLabel("stock-aviable"); ?></th>
												<th><?= $this->language->getLabel("stock-take"); ?></th>
												<th>&nbsp;</th>
											</thead>
											<tbody>
												<tr v-for="(qty, id_qty) in location.full">
													<td>{{qty.features}}</td>
													<td>{{qty.qty}}</td>
													<td>
														<input class="form-control" type="text" v-model="historic.need[step.Product.ID_Product][index].full[id_qty]" v-on:change="save">
													</td>
													<td>
		                            					<button type="button" class="btn btn-primary" v-on:click="getAllStock(step.Product.ID_Product, index, id_qty, step.Qty, qty.qty)">
															<span class="fa fa-play-circle"></span>
															<?= $this->language->getLabel("get-all"); ?>
														</button>
													</td>
												</tr>
												<tr v-for="(qty, id_qty) in location.rest">
													<td>{{qty.features}}</td>
													<td>{{qty.qty}}</td>
													<td>
														<input class="form-control" type="text" v-model="historic.need[step.Product.ID_Product][index].rest[id_qty]" v-on:change="save">
													</td>
													<td>
		                            					<button type="button" class="btn btn-primary" v-on:click="getAllStock()">
															<span class="fa fa-play-circle"></span>
															<?= $this->language->getLabel("get-all"); ?>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
	                            </div>
	                            <div class="tab-pane active" :id="'actions-' + step.ID_Step" v-if="step.Type == '0'">
	                            	<table class="table">
	                            		<thead>
	                            			<tr>
	                            				<th>&nbsp;</th>
	                            				<th>action</th>
	                            				<th>Qty</th>
	                            				<th>Description</th>
	                            				<th>&nbsp;</th>
	                            			</tr>
	                            		</thead>
	                            		<tbody>
	                            			<tr v-for="(action, key_action) in step.actions">
	                            				<td v-if="action.image">
                                        			<button class="btn btn-default preview" :data-preview-image="action.image" data-preview-size="modal-lg">
                                        				<img style="max-width: 40px;" :src="action.image">
                                        			</button>
                  								</td>
	                            				<td>{{getLabel('action_'+ action.ID_Action, action.Features_Values)}}</td>
	                            				<td>{{action.Qty}}</td>
	                            				<td>{{action.Description}}</td>
	                            				<td v-if="historic.actions[step.ID_Step][action.ID_ActionStep] == '0'">
	                            					<button type="button" class="btn btn-primary" v-on:click="completed(key_step, key_action)">
														<span class="fa fa-play-circle"></span>
														<?= $this->language->getLabel("cmd-realise"); ?>
													</button>
												</td>
	                            			</tr>
	                            		</tbody>
	                            	</table>
	                            </div>
	                            <div class="tab-pane active" :id="'actions-' + step.ID_Step" v-else-if="step.Type == 'optimizeCut'">
	                            	<div class="col-md-12" v-for="(lines, key_line) in step.Extra">
										<h4 class="col-md-6"><b><?= $this->language->getLabel("step"); ?> {{key_line + 1}} {{ ((lines.repeat - historic.extra[key_step][key_line]) > 0 ? '' :  ' - REALIZED') }}</b></h4>
										<div v-if="(lines.repeat - historic.extra[key_step][key_line]) > 0">
											<div>
												<?= $this->language->getLabel("repeat-action"); ?>: {{lines.repeat - historic.extra[key_step][key_line]}}
												<button  style="float: right;" type="button" class="btn btn-primary" v-on:click="completedOptimizeCut(key_step, key_line)">
													<span class="fa fa-play-circle"></span>
													<?= $this->language->getLabel("cmd-realise"); ?>
												</button>
											</div>
			                            	<table class="table" >
			                            		<thead>
			                            			<tr>
			                            				<th>action</th>
			                            				<th>Qty</th>
			                            				<th>Description</th>
			                            			</tr>
			                            		</thead>
			                            		<tbody>
			                            			<tr v-for="(action, key_action) in lines.lines">
			                            				<td>{{getLabel('action_1', action.features)}}</td>
			                            				<td>{{action.qty}}</td>
			                            				<td>{{action.description}}</td>
			                            			</tr>
			                            		</tbody>
			                            	</table>
		                            	</div>
	                            	</div>
	                            </div>
	                            <div v-else>Not found template for actions</div>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>