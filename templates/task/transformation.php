			<div v-if="step.Type == '0'">
				<h4><b>Products need:</b></h4>
				<ul class="list-group" >
					<li type="button" class="list-group-item" v-for="(action, key_action) in step.actions" >
						<button v-if="action.image" class="btn btn-default preview" :data-preview-image="action.image" data-preview-size="modal-lg">
							<img style="max-width: 40px;" :src="action.image">
						</button>
						<div class="col-md-5">Product: {{action.Name}} - Qty: {{action.Qty}}</div>
						<? if ($this->data["historic"]["Status"] != "C") { ?>
							<div v-if="historic.actions[step.ID_Step][action.ID_ActionStep] == '0'">
								<button type="button" class="btn btn-primary" v-on:click="completed(key_step, key_action)">
									<span class="fa fa-play-circle"></span>
									<?= $this->language->getLabel("cmd-realise"); ?>
								</button>
							</div>
						<? } ?>
					</li>
				</ul>
			</div>