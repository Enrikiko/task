			<div v-if="step.Type == 'agrupation'">
				<h4><b>Products need:</b></h4>
				<ul class="list-group">
					<li type="button" class="list-group-item" v-for="(action, index) in step.actions" >
						<div class="col-md-5">Product: {{action.Name}} - Qty: {{action.Qty}}</div>
						<button v-if="action.image" class="btn btn-default preview" :data-preview-image="action.image" data-preview-size="modal-lg">
							<img style="max-width: 40px;" :src="action.image">
						</button>
					</li>
				</ul>
			</div>