<div class="modal fade" id="modal-warning" tabindex="-1" role="dialog" aria-labelledby="modal-warning-header">
    <div class="modal-dialog modal-danger" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-cross"></span></button>

        <div class="modal-content">
            <div class="modal-header" style="border-radius:0px">
                <h4 class="modal-title" id="modal-success-header"><?= $this->language->getLabel("title-modal-".$this->data["page"]); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= $this->language->getLabel("modal-".$this->data["page"]); ?></p>
                <b><p>{{name}}</p></b>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?= $this->language->getLabel("cmd-cancel"); ?></button>
                <button class="btn btn-danger" name="del" :value="id"><?= $this->language->getLabel("cmd-delete"); ?></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  initDelete();
</script>
