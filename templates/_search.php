<? if (array_key_exists("items-search", $this->data)) { ?>
  <div class="block" id="search" v-if="show">
    <div class="block-content">
      <div class="col-md-11">
        <h1><b><?= $this->language->getLabel("search"); ?></b></h1>
      </div>
      <div class="col-md-1">
        <button type="button" class="close" style="float: right;" v-on:click="disableSearch"><span class="fa fa-times"></span></button>
      </div>
      <div class="col-md-12">
        <? foreach ($this->data["items-search"] as $key => $searchObject) { ?>
          <? if ($searchObject->type == "text") { ?>
            <div class="col-md-<?= $searchObject->width; ?>">
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel($searchObject->label); ?></label>
                <input class="form-control" name="<?= $searchObject->name; ?>" type="text" value="<?= $this->getValue($this->data["request"],$searchObject->name); ?>">
              </div>
            </div>
          <? } else if ($searchObject->type == "select") { ?>
            <div class="col-md-<?= $searchObject->width; ?>">
              <div class="input-group">
                <label class="control-label"><?= $this->language->getLabel($searchObject->label); ?></label>
                <select class="form-control" name="<?= $searchObject->name; ?>">
                  <option value=""><?= $this->language->getLabel("select-".$searchObject->label); ?></option>
                  <? foreach ($this->data[$searchObject->object["name"]] as $key => $object) { ?>
                    <? if (array_key_exists($searchObject->name, $object)) { ?>
                      <option value="<?= $object[$searchObject->name]; ?>" <?= ($object[$searchObject->name] == $this->getValue($this->data["request"],$searchObject->name) ? "selected" : ""); ?>>
                        <?= $object[$searchObject->object["option"]]; ?>
                      </option>
                    <? } else { ?>
                      <option value="<?= $key; ?>" <?= ($key == $this->getValue($this->data["request"],$searchObject->name) ? "selected" : ""); ?>>
                        <?= $object[$searchObject->object["option"]]; ?>
                      </option>
                    <? } ?>
                  <? } ?>
                </select>
              </div>
            </div>
          <? } ?>
        <? } ?>
      </div>
      <div class="col-md-12">
        <button style="float: right;" class="btn btn-success btn-icon-fixed">
          <span class="fa fa-search"></span>
          <?= $this->language->getLabel("cmd-search"); ?> 
        </button>
      </div>
    </div>
  </div>
<? } ?>