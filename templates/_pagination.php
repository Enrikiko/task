<? if ($this->data["pagination"] > 1) { ?>
  <nav style="float: right;">
    <ul class="pagination">
      
      <? if ($this->getValue($_POST,array("limit")) && $this->getValue($_POST,array("limit")) != 1) { ?>
        <li>
          <button class="btn btn-default" name="limit" value="<?= $this->getValue($_POST,array("limit")) - 1; ?>" style="padding: 6px 12px 6px 12px;">&laquo;</button>
        </li>
      <? } ?>

      <? for ($i = 1; $i <= ($this->data["pagination"] ); $i++) { ?>
        <li>
          <button class="btn btn-<?= ( (!$this->getValue($_POST,array("limit")) && $i == 1) || $this->getValue($_POST,array("limit")) == $i ? 'primary' : 'default'); ?>" name="limit" value="<?= $i; ?>" style="padding: 6px 12px 6px 12px;"><?= $i; ?></button>
        </li>
      <? } ?>
      
      <? if ($this->getValue($_POST,array("limit")) != ($this->data["pagination"])) { ?>
        <li>
          <button class="btn btn-default" name="limit" value="<?= $this->getValue($_POST,array("limit")) + 1; ?>" style="padding: 6px 12px 6px 12px;">&raquo;</button>
        </li>
      <? } ?>
    </ul>
  </nav>
<? } ?>