<div class="block">
  <div class="col-md-6">
    <h1><b><?= $this->language->getLabel("task"); ?></b></h1>
  </div>
  <div class="block-content">
    <? if ($this->data["tasks"]) { ?>
      <table class="table table-striped">
          <thead>
              <tr>
                  <th>&nbsp;</th>
                  <th><?= $this->language->getLabel("s-task"); ?></th>
                  <th><?= $this->language->getLabel("status"); ?></th>
                  <th>&nbsp;</th>
              </tr>
          </thead>
          <tbody>
            <? foreach ($this->data["tasks"] as $task) { ?>
              <tr style="<?= ($task["Priority"] ? "background-color: bisque;" : ""); ?>">
                <td>
                  <? if ($task["Priority"]) { ?>
                    <span class="fa fa-exclamation-circle" style="font-size: 25px;color: red;" data-toggle="tooltip" data-placement="right" data-original-title="<?= $this->language->getLabel("tooltip_task"); ?>"></span>
                  <? } ?>
                </td>
                <td><a href="/task/<?= $task["ID_Historic"]; ?>"><?= $task["Name"]; ?></a></td>
                <td><a href="/task/<?= $task["ID_Historic"]; ?>"><?= $this->language->getLabel("status_". $task["Status"]); ?></a></td>
                <td style="">
                  <a href="/task/<?= $task["ID_Historic"]; ?>">
                    <button type="button" class="btn btn-primary btn-clean"><?= $this->language->getLabel("cmd-show"); ?> <span class="fa fa-reorder"></span></button>
                  </a>
                </td>
              </tr>
            <? } ?>
          </tbody>
      </table>
    <? } else { ?>
      <?= $this->language->getLabel("no-tasks"); ?>
    <? } ?>
  </div>
</div>
