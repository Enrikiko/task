<div class="block" id="inventory">
  <div class="col-md-6">
    <h1>
      <b><?= $this->language->getLabel("manage-inventory"); ?></b>
    </h1>
  </div>
  <div class="col-md-6 pull-right">
    <button class="btn btn-info btn-icon-fixed"  data-toggle="modal" data-target="#modal-add-stock" type="button" style="float: right;">
      <span class="fa fa-plus"></span>
      <?= $this->language->getLabel("add-stock"); ?>                                   
    </button>
    <button class="btn btn-success btn-icon-fixed" type="button" v-if="product_id" style="float: right;margin-right: 5px;" v-on:click="reset()">
      <span class="fa fa-search"></span>
      <?= $this->language->getLabel("cmd-select-product"); ?>                                   
    </button>
  </div>
  <div class="block-content">
    <div class="col-md-12">
      <div class="input-group" v-if="!this.product_id">
        <label><?= $this->language->getLabel("select-product"); ?></label>
        <select class="form-control" id="product" v-model="product_id" v-on:change="getLocationByProduct">
          <option value=""><?= $this->language->getLabel("select-product"); ?></option>
          <? foreach ($this->data["products"] as $key => $product) { ?>
            <option value="<?= $key; ?>"><?= $product["Name"]; ?></option>
          <? } ?>
        </select>
      </div>
      <div v-else>
        <h4><b><?= $this->language->getLabel("s-product"); ?>: {{this.products[this.product_id].Name}}</b></h4>
      </div>
    </div>
    <div class="col-md-12">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" v-if="locations.length != 0">
        <div class="panel panel-default" v-for="location in locations" >
            <div class="panel-heading" role="tab" id="location" role="button" data-toggle="collapse" data-parent="#accordion" :href="'#location_'+location.id_Location" aria-expanded="true" :aria-controls="'location_'+location.id_Location" style="cursor: pointer;">
                <h4 class="panel-title"><?= $this->language->getLabel("s-location"); ?>: {{location.name}}</h4>
            </div>
            <div :id="'location_'+location.id_Location" class="panel-collapse collapse" role="tabpanel" >
                <div class="panel-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th><?= $this->language->getLabel("qty"); ?></th>
                        <th v-for="feature in features">{{feature.Name}}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="qty in location.qty">
                        <td>{{qty.qty}}</td>
                        <td v-for="feature in features">{{ ( qty.features[feature.ID_Feature] ? qty.features[feature.ID_Feature] : (products[product_id].Attributs[feature.ID_Feature] ? products[product_id].Attributs[feature.ID_Feature] : '-' )) }}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
      </div>
      <div v-if="locations.length == 0 && this.product_id">
        <p><?= $this->language->getLabel("no-stock"); ?></p>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-add-stock" tabindex="-1" role="dialog">
  <form method="post" action="">
    <div class="modal-dialog" role="document">                    
      <div class="modal-content">
        <div class="modal-body">

          <div class="app-heading app-heading-small app-heading-condensed">                                
            <div class="title">
              <h5><?= $this->language->getLabel("add-stock"); ?></h5>
            </div>
          </div>

          <div class="input-group">
            <label><?= $this->language->getLabel("select-product"); ?></label>
            <select class="form-control" name="product" required>
              <option value=""><?= $this->language->getLabel("select-product"); ?></option>
              <? foreach ($this->data["products"] as $key => $product) { ?>
                <option value="<?= $product["ID_Product"]; ?>"><?= $product["Name"]; ?></option>
              <? } ?>
            </select>
          </div>
          <div class="input-group">
            <label><?= $this->language->getLabel("select-location"); ?></label>
            <select class="form-control" name="location" required>
              <option value="0"><?= $this->language->getLabel("select-location"); ?></option>
              <? foreach ($this->data["workshop"] as $key => $workshop) { ?>
                <optgroup label="<?= $this->language->getLabel("s-workshop") . " - " . $workshop["name"]; ?>">
                <? foreach ($workshop["locations"] as $key => $location) { ?>
                  <option value="<?= $location["ID_Location"]; ?>"><?= $location["Name_location"]; ?></option>
                <? } ?>
                </optgroup>
              <? } ?>
            </select>
          </div>
          <div class="input-group">
            <label><?= $this->language->getLabel("qty"); ?></label>
            <input class="form-control" type="text" name="qty" required>
          </div>
          <button class="btn btn-default" data-dismiss="modal"><?= $this->language->getLabel("cmd-cancel"); ?></button>
          <button class="btn btn-success pull-right" name="cmd-add-stock" ><?= $this->language->getLabel("cmd-create"); ?></button>
        </div>                    
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
  var php_products = <?= json_encode($this->data["products"]); ?>;

  initInventory();
</script>