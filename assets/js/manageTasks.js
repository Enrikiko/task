function initEmployee() {
  var employee = new Vue({
    el: '#employee',
    data: {
      employees_disc: disc_employees,
      employees_connec: connec_employees,
    },
    methods: {
      connect: function() {
        var id = $('#select_employees').val();
        if (id != -1) {
          this.ajax('c',this.employees_disc[id].ID_Employee);
          this.employees_connec.push(this.employees_disc[id]);
          this.employees_disc.splice(id,1);
        }
      },
      disconnect: function(id) {
        this.ajax('d',this.employees_connec[id].ID_Employee);
        this.employees_disc.push(this.employees_connec[id]);
        this.employees_connec.splice(id,1);
      },
      ajax: function(action, id) {
        $.getJSON('/ajax/employee', {q: id, action: action});
      }
    }
  });
}

/*function initSteps() {
  var steps = new Vue({
    el: '#steps',
    data: {
      stocks: php_stock,
      action: php_action,
      location_select: [],
      qty_select: [],
      feature_value: [],
      location_select_rest: '',
      reserved: true,
    },
    methods: {
      refreshStock: function(id_product) {
        var qty = 0;
        var keys = Object.keys(this.stocks.product_need[id_product].locations);

        for(var i = 0; i < keys.length; i++){
          qty += parseInt(this.stocks.product_need[id_product].locations[keys[i]].lack);
        }
        
        if (!isNaN(qty)) {
          this.stocks.product_need[id_product].result = qty ;
        }
        this.enableStart();
      },
      refreshStockByRest: function(id_product) {
        var qty = 0;
        var keys = Object.keys(this.stocks.product_need[id_product].rest_selected);

        for(var i = 0; i < keys.length; i++){
          qty += parseInt(this.stocks.product_need[id_product].rest_selected[keys[i]]);
        }
        
        if (!isNaN(qty)) {
          this.stocks.product_need[id_product].result_rest = qty;
        }
        this.enableStart();
      },
      enableStart: function() {
        for (var i = 0 ; i < this.stocks.product_need.length; i++) {
          this.reserved = ((this.stocks.product_need[i].result + this.stocks.product_need[i].result_rest) >= this.stocks.product_need[i].qty ? false : true);
          if (this.reserved) {
            break;
          }
        }
      },
      addLocation: function(id) {
        if (this.location_select[id] && this.qty_select[id]) {
          var location = this.location_select[id].split('##');
          var canPut = true;

          for (var i = 0; i < this.stocks.product_result[id].result_locations.length; i++) {
            if (this.stocks.product_result[id].result_locations[i].ID_Location == location[0]) {
              canPut = false;
              break;
            }
          }

          if (canPut) {
            this.stocks.product_result[id].result_locations.push({'ID_Location': location[0], 'Name': location[1], 'qty': this.qty_select[id]});
            this.stocks.product_result[id].result = parseInt(this.stocks.product_result[id].result) + parseInt(this.qty_select[id]);
            this.location_select[id] = '';
            this.qty_select[id] = '';
          }
        }
      },
      removeLocation: function(id, id_location) {
        this.stocks.product_result[id].result -= parseInt(this.stocks.product_result[id].result_locations[id_location].qty);
        this.stocks.product_result[id].result_locations.splice(id_location,1);
      },
      addRest: function(index){
        if (this.feature_value.length) {
          var location = this.location_select_rest.split('##');
          
          this.stocks.product_need[index].rest.push({'ID_Location': location[0],'Name_Location': location[1], 'features' : this.feature_value});

          this.location_select_rest = '';
          this.feature_value = [];
        }
      },
      removeRest: function(index, id){
        this.stocks.product_need[index].rest.splice(id,1);
      },
      getJSON: function(obj) {
        return JSON.stringify(obj);
      },
    }
  });
}*/

function initAdminEmployee() {
  var emplyeeAdmin = new Vue({
    el: '#employee',
    data: {
      checked: intensive_workday,
      tasks: php_tasks,
      task_assign: php_assign,
      task: '',
      family: '',
    },
    methods: {
      addTask: function() {
        if (this.task) {
          for (var i = 0; i < this.task_assign.length; i++) {
            if (this.task_assign[i].ID_Task == this.tasks[this.task-1].ID_Task) {
              return false;
            }
          }

          this.task_assign.push({'ID_Task': this.tasks[this.task-1].ID_Task});
          this.task = '';
        }
      },
      addGroup: function() {
        if (this.family) {
          for (var i = 0; i < this.tasks.length; i++) {
            if (this.tasks[i].ID_Family == this.family) {
              this.task = i + 1;
              this.addTask();
            }
          }

          this.family = '';
        }
      },
      del: function(id) {
        this.task_assign.splice(id,1);
      },
      getJSON: function() {
        return JSON.stringify(this.task_assign);
      },
      in_assing_task: function(id_task){
        return this.task_assign.includes(id_task);
      },
      setAssign: function(id_task){
        if ($('#check_' + id_task)[0].checked) {
          this.task_assign.push(id_task);
        } else {
          id = 0;

          for (var i = 0; i < this.task_assign.length; i++) {
            if (this.task_assign[i] == id_task){
              this.task_assign.splice(i, 1);
              break;
            }
          }
        }
      },
      setAllCheck: function(id_group) {
        for( i = 0; i < $('[name="group_' + id_group + '"]').length; i++){
          if (!$('[name="group_' + id_group + '"]')[i].checked){
            this.task_assign.push($('[name="group_' + id_group + '"]')[i].value);
            $('[name="group_' + id_group + '"]')[i].checked = true;
          }
        }
      },
    },
  });
}

function initDelete() {
  var warning = new Vue({
    el: '#modal-warning',
    data: {
      name: '',
      id: '',
    },
  });

  var button = new Vue({
    el: '#delete',
    methods: {
      setName: function(str, id) {
        warning.name = str;
        warning.id = id;
      }
    }
  });
}

function initSettingsProducts() {
  var settingsProducts = new Vue({
    el: '#products',
    data: {
      settings: settings_load,
      name: '',
    },
    methods: {
      del: function(id) {
        this.settings.splice(id,1);
      },
      newAttr: function() {
        if (this.name) {
          this.settings.push(this.name);
          this.name = '';
        }
      },
    },
  });


  if (settingsProducts.settings.length == 0 ) {
    this.settings = [];
  }
}

function initProduct() {
  var editProduct = new Vue({
    el: '#editProduct',
    data: {
      features: [],
      features_product: php_features_product,
      min_qty: php_min_qty,
      qty: 0,
      location: 0,
    },
    methods: {
      getFamilyFeatures: function() {
        var id = parseInt($('#ID_Family').val());
        
        if (id) {
          $.getJSON('/ajax/features', {q: id}).done(function(data) {
            editProduct.features = data;
          });
        }
      },
    },
  });

  editProduct.getFamilyFeatures();
}

function initTask() { 

  json_action = {'id_action' : '0', 'values': {}, 'qty': '0', 'id_product': '0', 'description' : ''};
  json_step = {'product':'', 'qty':'0', 'actions':[], "type": '0'};
  json_step.actions.push(json_action);

  if (!php_steps.length) {
    php_steps.push(json_step);
  }

  if (!php_steps[0].actions.length) {
    php_steps[0].actions.push(json_action);
  }

  var task = new Vue({
    el: '#Task',
    data: {
      task_steps: php_steps,
      products: php_products,
      product_need: '',
      product_result: '',
      qty_need: '',
      qty_result: '',
      description: '',
      product_id: php_product_id,

      actions: php_actions,
      steps: php_steps,
      products_created: [],
    },
    methods: {
      addEmptyStep: function() {
        id_step = this.steps.push(json_step);
      },
      delStep: function(id_step) {
        if (this.steps[id_step].ID_Step) {
          this.steps[id_step].del = '1';
        } else {
          this.steps.splice(id_step, 1);
        }
      },
      addAction: function(id_step, id_action = '0', values = {}) {
        if(this.steps[id_step].type == "optimizeCut") {
          id_action = '1';
        }
        this.steps[id_step].actions.push({'id_action' : id_action, 'values': values, 'qty': '0', 'id_product': '0', 'description' : ''});
      },
      addActionValues: function(id_step, type, features){
        this.steps[id_step].actions[0].type = type;
        if (type == "T") {
          features = JSON.parse(features);
          this.steps[id_step].actions[0].values = features.ID_Feature;
        }
      },
      delAction: function(id_step, id_action) {
        if (this.steps[id_step].actions[id_action].ID_ActionStep) {
          this.steps[id_step].actions[id_action].del = '1';
        } else {
          this.steps[id_step].actions.splice(id_action, 1);
        }
      },
      isOptimize: function(id_step){
        if(this.steps[id_step].type == "optimizeCut") {
          this.steps[id_step].actions[0].id_action = '1';
        }
      },
      getJSON: function() {
        return JSON.stringify(this.steps);
      },
      /*addActionTransformation: function(id_step, features){
        features = JSON.parse(features);
        this.addAction(id_step, this.steps[0].actions[0].id_action, features.ID_Feature);
      },
      linkProduct: function(id_step, id_action){
        if (this.steps[id_step].actions[id_action].id_action <= 2){
          $.getJSON('/ajax/product', {product: this.steps[id_step].product, values: this.steps[id_step].actions[id_action].values}).done(function(data) {
            if (!task.products[data.ID_Product]) {
              task.products_created.push(data);
            }
            
            task.steps[id_step].actions[id_action].id_product = data.ID_Product;
          });
        }
      },*/
    },
  });
}

function initSearch() {
  var search = new Vue({
    el: '#search',
    data: {
      show: php_show,
    },
    methods: {
      disableSearch: function() {
        this.show = false;
      }
    },
  });

  var button = new Vue({
    el: '#button-search',
    methods: {
      enableSearch: function() {
        search.show = true;
      }
    },
  });
}

function initFamily() {
  var family = new Vue({
    el: '#family',
    data: {
      type: php_type,

      preview_settings: php_preview,
      select: '',
      position: '',

      features: php_features,
      family_features: php_family_features,
      features_father: php_features_family,
    },
    methods: {
      addFeature: function() {
        var id = $('#ID_Feature').val();
        this.family_features.push({'ID_Feature': this.features[id].ID_Feature, 'Name': this.features[id].Name, 'Rest': false, 'new': 1,'remove': 0});
      },
      delFeature: function(id) {
        for (var i = 0; i < this.preview_settings.length; i++) {
          if (this.preview_settings[i].ID_Feature == this.features[id].ID_Feature) {
            this.delPreview(i);
          }
        }

        if (this.family_features[id].new) {
          this.family_features.splice(id,1);
        } else {
          this.family_features[id].remove = 1;
        }
      },

      getFamilyFeatures: function() {
        var id = parseInt($('#ID_Father').val());
        if (id) {
          $.getJSON('/ajax/features', {q: id}).done(function(data) {
            family.features_father = data;
            family.setRestBoolean(family.features_father);
          });
        } else {
          this.features_father = [];
        }
      },
      getJSON: function(value) {
        return JSON.stringify(value);
      },

      addPreview: function() {
        if (this.preview_settings == null) {
          this.preview_settings = [];
        }

        if (this.select != '' && this.position != '') {

          this.preview_settings.push({
            'ID_Feature': this.select, 
            'label': $('#select-feature').find('option:selected').text(),
            'value_before': '',
            'value_after': '', 
            'position': this.position
          });

          this.select = '';
          this.position = '';
        }
      },
      delPreview: function(id) {
        this.preview_settings.splice(id,1);
      },
      setRestBoolean: function (obj){
        for (var i = 0; i < obj.length; i++) {
          obj[i].Rest = (obj[i].Rest == 1);
        }
      }
    },
  });

  family.setRestBoolean(family.family_features);
  family.setRestBoolean(family.features_father);
}

function initInventory() {
  var inventory = new Vue({
    el: '#inventory',
    data: {
      product_id: '',
      locations: [],
      products: php_products,
      features: [],
    },
    methods: {
      getLocationByProduct: function() {
        if (this.product_id != '') {
          $.getJSON('/ajax/location', {q: parseInt(this.products[this.product_id].ID_Product)}).done(function(data) {
            inventory.locations = data.locations;
          });
          this.getFamilyFeatures(this.products[this.product_id].ID_Family);
        }
      },
      getFamilyFeatures: function(id) {
        this.features = [];

        if (id) {
          $.getJSON('/ajax/features', {q: id}).done(function(data) {
            inventory.features = data;
          });
        }
      },
      isInteger: function($val){
        return $.isNumeric($val);
      },
      reset: function() {
        this.product_id = '';
        this.locations = [];
        this.features = [];
      }
    },
  });
}

function initSteps() {
  var vue_steps = new Vue({
    el: '#list_steps',
    mixins: [languageFunctions],
    data: {
      steps: php_steps,
      stock: php_stock,
      historic: php_historic,
      status: php_status,
      id_historic: php_id_historic,
    },
    methods: {
      completed: function(id_step, id_actionStep) {
        console.log(id_step + "##" + id_actionStep);
        //this.historic.actions[this.steps[id_step].ID_Step][this.steps[id_step].actions[id_actionStep].ID_ActionStep] = '1';
        this.finishStep(this.steps[id_step].ID_Step);

        //El 2 d'aquest if es per que les accions tenen que ser les primeres aixo es tindra que canviar si s'afegeixen mes accions(tornejar), segurament ficant el type tot quedi arreglat
        if (this.steps[id_step].actions[id_actionStep].ID_Action <= 2) {
          var id_product = this.steps[id_step].actions[id_actionStep].ID_Product;
          //Es tindra que modifcar depenet la ubicació que es trii
          this.historic.need[id_product] = { '1': { 'full': [ '0' ], 'rest': [] } };
          this.historic.actions[this.steps[id_step].ID_Step][this.steps[id_step].actions[id_actionStep].ID_ActionStep] = 1;
          //TODO si es buit el stock
          if (this.stock[id_product] && Object.keys(this.stock[id_product]).length) {
            //TODO canviar el 1 per la localització
            this.stock[id_product][1].full[0].qty = parseInt(this.stock[id_product][1].full[0].qty) + parseInt(this.steps[id_step].actions[id_actionStep].Qty);
          
          } else {
            this.stock[id_product] =  { '1': { 'full': [ { 'qty': this.steps[id_step].actions[id_actionStep].Qty, 'features': [] } ] } };
          }
          $.getJSON('/ajax/action', {
            features_action: this.steps[id_step].actions[id_actionStep].Features_Values,
            features: this.steps[id_step].Product.Attributs,
            qty: this.steps[id_step].actions[id_actionStep].Qty,
          });
        
        } else if(this.steps[id_step].actions[id_actionStep].ID_Action == 3) {
          this.historic.actions[this.steps[id_step].ID_Step][this.steps[id_step].actions[id_actionStep].ID_ActionStep] = 1;

          $.getJSON('/ajax/action', {
            features_action: JSON.parse(this.steps[id_step].actions[id_actionStep].Features_Values).ID_Feature,
            id_product: this.steps[id_step].actions[id_actionStep].ID_Product,
            qty: this.steps[id_step].actions[id_actionStep].Qty,
          });
          /*$.getJSON('/ajax/actionGenerate', {

          }).done(function(data) {

          });*/
        }

        //this.save();
      },
      completedOptimizeCut(id_step, id_line){
        Vue.set(this.historic.extra[id_step], id_line, parseInt(this.historic.extra[id_step][id_line]) + 1 );

        for (var id_action = 0 ; id_action < this.steps[id_step].Extra[id_line].lines.length; id_action++) {
          var id_product = this.steps[id_step].Extra[id_line].lines[id_action].product;

          this.historic.need[id_product] = { '1': { 'full': [ '0' ], 'rest': [] } };

          if (this.stock[id_product] && Object.keys(this.stock[id_product]).length) {
            //TODO canviar el 1 per la localització
            this.stock[id_product][1].full[0].qty = parseInt(this.stock[id_product][1].full[0].qty) + parseInt(this.steps[id_step].Extra[id_line].lines[id_action].qty);
          } else {
            Vue.set(this.stock, id_product, {} );  
            this.stock[id_product] =  { '1': { 'full': [ { 'qty': this.steps[id_step].Extra[id_line].lines[id_action].qty, 'features': [] } ] } };
          } 
          
          $.getJSON('/ajax/action', {
            features_action: this.steps[id_step].Extra[id_line].lines[id_action].features,
            features: this.steps[id_step].Product.Attributs,
            qty: this.steps[id_step].Extra[id_line].lines[id_action].qty,
          });
        }

        this.save();
      },
      getAllStock: function(id_product, id_location, id_qty, qty, max_qty) {
        var value_qty = qty;
        
        if(max_qty < qty){
          value_qty = max_qty;
        }

        Vue.set(this.historic.need[id_product][id_location].full, id_qty, value_qty);
      },
      save: function(){
        $.getJSON('/ajax/saveHistoric', {
          id: this.id_historic,
          historic: this.getJSON(this.historic),
        });
      },
      finishStep: function(id_step) {
        if (this.historic.actions[id_step]) {
          var keys = Object.keys(this.historic.actions[id_step]);

          for(var i = 0; i < keys.length; i++){
            if (this.historic.actions[id_step][keys[i]] == 0) {
              return false;
            }
          }
          
          return true;
        }
      },
      getJSON: function(obj) {
        return JSON.stringify(obj);
      },
    },
  });

  return vue_steps;
}