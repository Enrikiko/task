var languageFunctions = {
	data: {
      language: {},
    },
    methods: {
      addLabel: function(key, value) {
        Vue.set(this.language, key, JSON.parse(value));
      },
      getLabel: function(key, values = ""){
        var tmp = this.language[key];
        var str = "";

        if ( tmp ) {
        	if (typeof values == "string") {
		        values = JSON.parse(values);
		      }

	        var keys = Object.keys(tmp);
	        for (var i = 0; i < keys.length; i++) {
	        	if (values[keys[i]]) {
	        		str += tmp[keys[i]];
	          	str = str.replace('%'+ keys[i] , values[keys[i]]);
	        	}
	        }
	      }
        return str;
      },
    }
}