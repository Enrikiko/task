<?php

class dbService {
  private $host = "localhost";
  private $user = "root";
  private $password = "manage1234";
  private $dbName = "manageTask";
  public $record = 20;

  public $db_connection;
  public static $instance;

  private function __construct(){
    $this->db_connection = new PDO("mysql:host=". $this->host .";dbname=". $this->dbName .";charset=utf8mb4", $this->user, $this->password);
  }

  public static function getInstance() {
    if (!self::$instance instanceof self) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  public function select($table, $where = "", $columns = "", $order = "", $limit = "") {
    $stmt = $this->find($table, $where, $columns, $order, $limit);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function selectIdKey($table, $where = "", $columns = "", $order = "", $limit = ""){
    $stmt = $this->find($table, $where, $columns, $order, $limit);
    return $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
  }

  private function find($table, $where = "", $columns = "", $order = "", $limit = "") {
    $sql = "SELECT ". ($columns ? $columns : "*" ) ." FROM ". $table . ($where ? " WHERE " . $where : "") . ($order ? " ORDER BY " . $order : "") . ($limit ? " LIMIT " . $this->record . " OFFSET " . (($limit-1) * $this->record ) : "");
    return $this->execSql($sql);
  }

  public function insert($table, $ids, $columns_values) {
    $values = "";
    $columns = "";
    foreach ($columns_values as $column_name => $value) {
      if ($values) {
        $columns .= ",";
        $values .= ",";
      }
      $columns .= $column_name;
      $values .= "'" . $value . "'";
    }

    $sql = "INSERT INTO ". $table . "(" . $columns  .") VALUES(". $values .")";
    $stmt = $this->execSql($sql);

    return $this->getLastId();
  }

  public function update($table, $columns_values, $where) {
    $values = "";
    foreach ($columns_values as $column_name => $value) {
      if ($values) {
        $values .= ",";
      }
      $values .= $column_name. " = '" . $value . "'";
    }

    $sql = "UPDATE ". $table . " SET " . $values . ($where ? " WHERE " . $where : "");
    $stmt = $this->execSql($sql);

    return $this->getLastId();
  }

  public function delete($table, $where) {
    $sql = "DELETE FROM $table " . ($where ? " WHERE " . $where : ""); 
    return $this->execSql($sql);
  }

  public function execSql($sql) {
    return $this->db_connection->query($sql);
  }

  public function getLastId(){
    return $this->db_connection->lastInsertId();
  }

}
