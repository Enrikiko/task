<?php
include_once "./models/productModel.php";
include_once "./models/locationModel.php";
include_once "./models/productLocationModel.php";

include_once "./services/familyService.php";
include_once "./services/featureService.php";
include_once "./services/fileService.php";

class productService {
  public static function getAll($where = "", $limit = "", $order = "product.ID_Family", $show_id_as_key = false){
    $products = new productModel;
    $columns = "product.*, COUNT(*) as 'Quantity'";
    $group = " product.ID_Product";

    if (!authService::inSection("admin")) {
      $where = ($where ? $where . " AND " : "") . "location.ID_workshop = '" . $_SESSION["workshop"] . "'";
      $columns = "product.*, location.*, productLocation.Quantity as 'Quantity'";
      $group = " product.ID_Product, location.ID_Location, productLocation.Quantity";
    }

    $results = $products->search($where, $columns, $group, $order /*. "," . featureService::getOrderByRest()*/ ,$limit, $show_id_as_key);
    $products->setNameByAttribute($results);

    return $results;
  }

  public static function getAllProducts($where = "", $limit = "") {
    $order = featureService::getOrder();

    $products = new productModel;
    return $products->find($where, "", "ID_Family, Name DESC, " . ($order ? $order . ", " : "") . " CAST(Attributs->'$.\"1\"' AS UNSIGNED) DESC", $limit);
  }

  public static function getStockAndLocationsByHistoric($id_product, $id_workshop, $id_hisotric) {
    $model = new locationModel;
    
    $sql = "SELECT  l.ID_Location, l.Name_location, COUNT(pl.ID_Quantity) as 'qty', pl.Features
            FROM ProductLocation pl
            INNER JOIN Location l ON l.ID_Location = pl.ID_Location
            WHERE l.ID_workshop = '$id_workshop' AND pl.ID_Product = '$id_product' AND (pl.ID_Historic = '0' OR pl.ID_Historic = '$id_hisotric')
            GROUP BY l.ID_Location, pl.Features
            ORDER BY l.ID_Location, pl.Features";

    $stock = array();

    $results = array("full" => array(),  "rest" => array());
    $results = $model->execSql($sql);
    
    foreach ($results as $key => $location) {
      $type = "full";
      $location["Features"] = json_decode($location["Features"], true);
      
      if (count($location["Features"])) {
        $type = "rest";
      }

      if (!array_key_exists($location["ID_Location"],$stock)) {
        $stock[$location["ID_Location"]][$type] = array();
      }

      if ($type == "full") {
          $stock[$location["ID_Location"]][$type][] = array(
            "qty"       => $location["qty"],
            "features"  => $location["Features"],
          );
      
      } else {
          $key =  json_encode($location["Features"]);
          $stock[$location["ID_Location"]][$type][str_replace("\"", "", $key)] = array(
            "qty"       => $location["qty"],
            "features"  => $location["Features"],
          );
      }
    }

    return $stock;
  }

  public static function getStockAndLocations($id_product, $id_workshop) {
    $model = new locationModel;
    $sql = "SELECT l.ID_Location, l.Name_location, COUNT(pl.ID_Quantity) as 'qty', pl.Features
            FROM ProductLocation pl
            INNER JOIN Location l ON l.ID_Location = pl.ID_Location
            WHERE l.ID_workshop = '$id_workshop' AND pl.ID_Product = '$id_product' 
            GROUP BY pl.ID_Location, pl.Features
            ORDER BY l.ID_Location";

    $results = $model->execSql($sql);
    $stock = array("locations" => array());

    foreach ($results as $key => $qty) {
      if (!array_key_exists($qty["ID_Location"], $stock["locations"])) {
        $stock["locations"][$qty["ID_Location"]] = array(
          "id_Location"     => $qty["ID_Location"],
          "name"            => $qty["Name_location"],
          "qty"             => array(),
        );
      }

      $stock["locations"][$qty["ID_Location"]]["qty"][] = array(
        "qty"             => $qty["qty"],
        "features"        => json_decode($qty["Features"], true),
      );
    }

    return $stock;
  }
  /*public static function getStockAndLocations($id_product, $id_workshop, $id_hisotric = 0, $all_locations = false) {
    if ($all_locations) {
      $sql_product = "AND (pl.ID_Product IS NULL OR pl.ID_Product = '$id_product')";
      $sql_historic = "(pl.ID_Historic IS NULL OR pl.ID_Historic = '0' OR pl.ID_Historic = '$id_hisotric')";

    } else {
      $sql_product = "AND pl.ID_Product = '$id_product'";
      $sql_historic = "(pl.ID_Historic = '0' OR pl.ID_Historic = '$id_hisotric')";
    }

    $model = new locationModel;
    $sql = "SELECT l.ID_Location, l.Name_location, COUNT(pl.ID_Quantity) as 'qty', pl.Rest
            FROM Location l
            LEFT JOIN ProductLocation pl ON pl.ID_Location = l.ID_Location $sql_product
            WHERE l.ID_workshop = '$id_workshop' AND $sql_historic
            GROUP BY l.ID_Location, pl.Rest";

    $lines = $model->execSql($sql);
    $results["locations"] = array();

    foreach ($lines as $key => $line) {
      if (!array_key_exists($line["ID_Location"], $results["locations"])) {
        $results["locations"][$line["ID_Location"]] = array(
          "id_Location"     => $line["ID_Location"],
          "name"            => $line["Name_location"],
          "qty"             => 0,
          "rest_features"   => array(),
          "lack"            => 0,
        );
      }

      if ($line["Rest"]) {
        $results["locations"][$line["ID_Location"]]["rest_features"] = productService::getFeatureRest($id_product, $line["ID_Location"]);

      } else {
        $results["locations"][$line["ID_Location"]]["qty"] = $line["qty"];
      }
    }

    return $results;
  }*/

  public static function getFeatureRest($id_product, $id_location) {
    $model = new productModel;
    $sql = "SELECT ID_Quantity, ID_Feature, Feature_value
            FROM ProductFeature
            WHERE ID_Quantity IN (
                SELECT ID_Quantity
                FROM ProductLocation
                WHERE ID_Product = '$id_product' AND ID_Location = '$id_location' AND ID_Historic = '0')";

    $features = array();
    $results = $model->execSql($sql);

    foreach ($results as $key => $result) {
      $features[$result["ID_Quantity"]][$result["ID_Feature"]] = $result["Feature_value"];
    }

    return $features;
  }

  public static function getProductById($id_product, $edit_page = false) {
    $products = new productModel;
    return $products->findOne("ID_Product = $id_product", "", $edit_page);
  }

  public static function getAviableStock(&$task_steps){
    foreach ($task_steps as $key => $step) {
      $products = new productModel;

      if(is_numeric($key)){
        $result = $products->searchOne("product.ID_Product = " . $step["ID_Product_need"] . " AND productLocation.Quantity >= 0");
        $task_steps[$key]["Qty"] =  0;
        
        if (count($result)) {
          $task_steps[$key]["Qty"] =  $result["Quantity"];
        }
      
      }
    }
  }

  public static function insert($name, $description, $family, $attribut, $min_qty, $id_product_father, $tmp = 0) {
    $product = new productModel;
    $id_product =  $product->insert($product->columnsAndValues($name, $description, $family, $attribut, $min_qty, $id_product_father, $tmp));
    fileService::renameFile($id_product, "product");
    return $id_product;
  }

  public static function update($id_product,$name, $description, $family, $attribut, $min_qty) {
    $product = new productModel;
    fileService::renameFile($id_product, "product");
    return $product->update($id_product,$product->columnsAndValues($name, $description, $family, $attribut, $min_qty, 0));
  }

  public static function removeStock($id_product) {
    $stock = new productLocationModel;
    return $stock->delete($id_product);
  }

  public static function generateStock($id_product, $id_location, $qty, $features = "{}") {
    $model = new productLocationModel;
    $sql = "INSERT INTO ProductLocation (ID_Product, ID_Location, ID_Historic, Features)
            VALUES ";

    for ( $i = 0; $i < $qty; $i++){
      $sql = $sql . "($id_product, $id_location, 0, '$features')";

      if ($i+1<$qty) {
        $sql = $sql . ", ";
      }
    }

    $model->execSql($sql);
    //TODO: mirar com poder agafar els ids generats en comptes de fer una compta enrere
    $id = $model->getLastId();
    $ids = array();
    for ($i=0; $i < $qty; $i++) { 
      $ids[] = $id + $i;
    }
    
    return $ids;
  }

  public static function deleteStock($id_product, $id_location, $qty, $id_historic) {
    $model = new productLocationModel;

    $model->delete("ID_Product = '$id_product' AND ID_Location = '$id_location' AND (ID_Historic = '$id_historic' OR ID_Historic = '0')  LIMIT $qty", false);

    /*$quantities = productService::getIdQuanities($id_product,$id_location, $id_historic,$qty);
    for( $i = 0; $i < $qty; $i++) {
      productService::deleteStockByQuantity(array($quantities[$i]["ID_Quantity"]));
    }*/
  }

  public static function deleteStockByQuantity($ids_quantities) {
    $model = new productLocationModel;

    if (!is_array($ids_quantities)) {
      $ids_quantities = array($ids_quantities);
    }

    foreach ($ids_quantities as $key => $id_quantity) {
      $model->delete($id_quantity);
    }
  }

  public static function delete($id_product) {
    $product = new productModel;
    $stock = new productLocationModel;
    $stock->delete($id_product);

    return $product->delete($id_product);
  }

  public static function setImage($id, $path) {
    $product = new productModel;
    return $product->update($id,array("Image" => $path));
  }

  public static function getAlertStock() {
    $stocks = new productLocationModel;
    $id_user = 0;

    if (array_key_exists("ID_User", $_SESSION)) {    
      $sql = "SELECT pl.ID_Product
              FROM Product p 
              LEFT JOIN ProductLocation pl ON p.ID_Product = pl.ID_Product
              WHERE p.Min_enabled = 1 AND ID_Historic = '0' AND p.ID_User = '" . $_SESSION["ID_User"] . "'
              GROUP BY ID_Quantity, ID_Product, p.Min_quantity
              HAVING COUNT(*) <= p.Min_quantity";

      $result = $stocks->execSql($sql);

      $ids_alert = array();
      foreach ($result as $key => $product) {
        $ids_alert[$product["ID_Product"]] = "";
      }

      return $ids_alert;
    }
  }

  public static function getPages($where){
    $product = new productModel;
    return $product->getPages($where);
  }

  public static function getIdQuanities($id_product, $id_location, $id_historic, $limit = 0) {
    $model = new productLocationModel;
    $sql = "SELECT ID_Quantity
            FROM ProductLocation
            WHERE ID_Product = '$id_product' AND ID_Location = '$id_location' AND ID_Historic = '$id_historic' AND Rest <> 1".
            ($limit ? " LIMIT " . $limit : "");

    return $model->execSql($sql);
  }

  public static function updateRest($id_quantity, $id_location, $id_feature, $value) {
    $model = new productLocationModel;
    $model->update($id_quantity, array("ID_Location" => $id_location,"Rest" => "1", "ID_Historic" => "0"));
    productService::addRest($id_quantity, $id_feature, $value);
  }

  public static function insertRest($id_product, $id_location, $id_feature, $value) {
    $model = new productLocationModel;
    $id_quantity = $model->insert($model->columnsAndValues($id_product, $id_location, 1));
    productService::addRest($id_quantity, $id_feature, $value);
  }

  /*public static function addRest($id_quantity, $id_feature, $value) {
    $model = new productFeatureModel;
    if (count($model->find("ID_Quantity = '$id_quantity' AND ID_Feature = '$id_feature'"))) {
      $model->update(array($id_quantity, $id_feature), array("Feature_value" => $value));
    } else {
      $model->insert($model->columnsAndValues($id_quantity, $id_feature, $value));
    }
  }*/

  public static function reserveQuantity($ids_quantities, $id_historic) {
    $model = new productLocationModel;
    $sql = "UPDATE ProductLocation
            SET ID_Historic = $id_historic
            WHERE ID_Quantity IN (" . join(',', $ids_quantities) .")";
    $model->execSql($sql);
  }

  public static function deleteStockByFeatures($id_product, $id_location, $features, $id_historic = 0, $qty = 1) {
    $model = new productLocationModel;

    $model->delete("ID_Product = '$id_product' AND ID_Location = '$id_location' AND (ID_Historic = '$id_historic' OR ID_Historic = '0') AND " . productService::generateWhereFromFeatures($features, "Features"). " LIMIT $qty", false);
  }

  public static function generateWhereFromFeatures($features, $name) {
    $where = "";

    foreach ($features as $key => $feature) {
      if ($where) {
        $where .= " AND ";
      }

      $where .= " $name->'$.\"$key\"' = '$feature'";
    }

    return $where;
  }

  public static function createProductFromFeatures($id_product, $features){
    if ($id_product) {
      $product = productService::getProductById($id_product);

      if ($product["Attributs"]) {
        foreach ($product["Attributs"] as $key => $feature) {
          if (!array_key_exists($key, $features)) {
            $features[$key] = $feature;
          }
        }
      }
    }

    if (!$new_product = productService::getProductByFeatures($features)){
      $new_product["ID_Product"] = productService::insert($product["Name"], $product["Description"], $product["ID_Family"], $features, 0, $product["ID_Product"]);
    }

    return $new_product["ID_Product"];



    /*
    foreach ($features_action as $key => $feature) {
      $features[$key] = $feature;
    }

    $product = productService::getProductByFeatures($features);
    //TODO: Enviar location ara esta fixat
    return productService::generateStock($product[0]["ID_Product"], 1, $qty);*/
  }

  public static function getProductByFeatures($features, $id_family = 0) {
    $model = new productModel;

    return $model->findOne(productService::generateWhereFromFeatures($features, "Attributs") . " AND JSON_LENGTH(Attributs) = " . count($features) . ( $id_family ? " AND ID_Family = '$id_family'" : ""));
  }
}
