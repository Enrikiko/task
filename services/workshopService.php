<?php
include_once "./models/workshopModel.php";
include_once "./services/locationService.php";

class workshopService {

  public static function getAll($where = "", $limit = ""){
      $workshop = new workshopModel;
      return $workshop->find($where,"","",$limit);
  }

  public static function getAllIds($where = "") {
    $model = new workshopModel;
    $workshops = $model->find($where);
    $ids = array();

    foreach ($workshops as $key => $workshop) {
      $ids[] = $workshop["ID_workshop"];
    }

    return $ids;
  }

  public static function getAllLocations($id_workshop = "", $where_location = ""){
    $workshops = workshopService::getAll();
    
    foreach ($workshops as $key => $workshop) {
      if(!$id_workshop || $id_workshop == $workshop["ID_workshop"]) {
        $workshops[$key]["locations"] = locationService::getLocationByIdWorkshop($workshop["ID_workshop"], $where_location);
      } else {
        $workshops[$key]["locations"] = array();
      }
    }

    return $workshops;
  }

  public static function getWorkshop($id_workshop){
      $workshop = new workshopModel;
      return $workshop->findOne("ID_workshop = $id_workshop");
  }

  public static function insert($name, $address, $phone) {
    $location = new workshopModel;
    $id_workshop = $location->insert($location->columnsAndValues($name, $address, $phone));
    fileService::renameFile($id_workshop, "workshop");
    return $id_workshop;
  }

  public static function update($id_workshop, $name, $address, $phone) {
    $location = new workshopModel;
    fileService::renameFile($id_workshop, "workshop");
    return $location->update($id_workshop,$location->columnsAndValues($name, $address, $phone));
  }

  public static function delete($id_workshop) {
    if ($id_workshop != $_SESSION["workshop"]) {
      $workshop = new workshopModel;

      foreach (locationService::getLocationByIdWorkshop($id_workshop) as $key => $location) {
        if (!locationService::delete($location["ID_Location"])) {
          return 2;
        }
      }

      $workshop->delete($id_workshop);
      return 1;
    }

    return 0;
  }

  public static function setImage($id, $path) {
    $workshop = new workshopModel;
    return $workshop->update($id,array("Image" => $path));
  }


  public static function getPages($where){
    $model = new workshopModel;
    return $model->getPages($where);
  }

}
