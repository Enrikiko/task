<?php

class uiService {
  public static function select($name, $default_value, $array, $key_item, $opt_group = "") {
    $html = "<select class=\"form-control\" name=\"$name\" id=\"$name\">";
    $html .= "<option value=\"$default_value[0]\">$default_value[1]</option>";
    if (is_array($array) && count($array)) {
      foreach ($array as $key => $item) {
        if (count($opt_group)) {
          $html .= "<optgroup label=\"" . $item[$opt_group[0]] . "\">";
          foreach ($item[$opt_group[1]] as $key => $subitem) {
            $html .= "<option value=\"" . $subitem[$key_item[0]] . "\" >" . $subitem[$key_item[1]] ."</option>";
          }
          $html .= "</optgroup>";
        } else {
            $html .= "<option value=\"" . $item[$key_item[0]] . "\">" . $item[$key_item[1]] . "</option>";
        }
      }
    }
    $html .= "</select>";
    return $html;
  }
}
