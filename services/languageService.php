<?php
class languageService {
  public $translate;
  private static $instance;

  private function __construct(){
    include_once "./assets/locale/es.php";
    $this->translate = $language;
  }

  public static function getInstance() {
    if (!self::$instance instanceof self) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  public function getLabel($key){
    return ( array_key_exists($key, $this->translate) ? $this->translate[$key] : $key);
  }
  
  public function getLabelWithValues($key, $values) {
    if (!is_array($values)) {
      $values = array($values);
    }

    $str = $this->translate[$key];
    foreach ($values as $key => $value) {
      $str = str_replace("%".($key+1), $value, $str);
    }
    return $str;
  }
}
