<?php

class fileService {
	public static function renameFile($id, $class) {
		$target = "assets/images/$class/" . $_SESSION["ID_User"];
		if ($class == "user") {
	    	$class = "auth";
	    }
		$class .= "Service";

		foreach (glob($target . "/tmp_" . $_SESSION["employee"] . "_*.*") as $file) {
			$info = pathinfo($file);
      		$ext = $info['extension'];
      		$new_FileName = $target . "/" . "$id.$ext";

		    rename($file, $new_FileName);
			$class::setImage($id, "/" . $new_FileName);
		    break;
		}
	}
}