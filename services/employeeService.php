<?php
include_once "./models/employeeModel.php";
include_once "./models/taskHistoricModel.php";
include_once "./services/fileService.php";

class employeeService {
  public static function getEmployeesConnected($ID_workshop){
    $employees = new employeeModel;
    return $employees->find("ID_workshop = $ID_workshop AND ID_User = '" . $_SESSION["ID_User"] . "' AND enabled = '1'");
  }

  public static function getEmployeesDisconnected($ID_workshop){
    $employees = new employeeModel;
    return $employees->find("ID_workshop <> $ID_workshop AND ID_User = '" . $_SESSION["ID_User"] . "' AND enabled = '1'");
  }

  public static function getAll($where = "", $limit = ""){
    $employees = new employeeModel;
    return $employees->find(($where ? $where . " AND " : "") . "enabled = '1'", "", "", $limit);
  }

  public static function insert($name, $schedule, $role, $tasks) {
    $employee = new employeeModel;
    $id_employee =  $employee->insert($employee->columnsAndValues($name, $schedule, $role, $tasks));
    fileService::renameFile($id_employee, "employee");
    return $id_employee;
  }

  public static function update($id_employee, $name, $schedule, $role, $tasks) {
    $employee = new employeeModel;
    fileService::renameFile($id_employee, "employee");
    return $employee->update($id_employee, $employee->columnsAndValues($name, $schedule, $role, $tasks));
  }

  public static function delete($id_employee){
    if ($id_employee != $_SESSION["employee"]) {
      $employee = new employeeModel;
      $historic = new taskHistoricModel;
      $results = $historic->find("ID_Employee = '$id_employee'");
      if (count($results)) {
        $employee->update($id_employee, array("enabled" => 0));
      } else {
        $employee->delete($id_employee);
      }

      return true;
    }
    return false;
  }

  public static function getEmployee($id_employee) {
    $employees = new employeeModel;
    return $employees->findOne("ID_Employee = $id_employee");
  }

  public static function disconnectEmployee($id_employee){
    $employees = new employeeModel;
    return $employees->update($id_employee,array("ID_workshop" => "0"));
  }

  public static function connectEmployeeToWorkshop($id_employee, $id_workshop){
    $employees = new employeeModel;
    return $employees->update($id_employee,array("ID_workshop" => $id_workshop));
  }

  public static function getEmployeeTasks($id_employee){
    $ids = array();
    $employee = employeeService::getEmployee($id_employee, "Tasks");
    
    if ($employee["Tasks"]) {
      $employee["Tasks"] = json_decode($employee["Tasks"],true);
      $ids = $employee["Tasks"];
    }

    return $ids;
  }

  public static function setImage($id, $path) {
    $employee = new employeeModel;
    return $employee->update($id,array("Image" => $path));
  }

  public static function getPages($where){
    $model = new employeeModel;
    return $model->getPages(($where ? $where ." AND " : '') . "enabled = '1'");
  }
}
