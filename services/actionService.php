<?php
include_once "./models/actionModel.php";
include_once "./services/featureService.php";
include_once "./services/productService.php";

class actionService {
	
	public static function getAll($where = "", $limit = "", $order = "") {
    	$model = new actionModel;
    	$results = $model->find($where, "", $order, $limit, true);

    	foreach ($results as $id_action => $action) {
    		$action["Features"] = json_decode($action["Features"],true);

    		if (is_array($action["Features"]["ID_Feature"])) {
				foreach ($action["Features"]["ID_Feature"] as $key => $id_feature) {

					if (ctype_alpha($id_feature)) {
						$id_feature = $key;
					}
					$results[$id_action]["Obj_Features"][] = featureService::getFeature($id_feature);
				}
			}
    	}
    	
    	return $results;
	}

	public static function getAction($id_action){
    	$model = new actionModel;
    	return $model->findOne("ID_Action = $id_action");
	}

	/*public static function setTask($id_task, $steps) {
		
	}*/

	public static function action_Generate_Stock($features, $features_action, $qty) {
        foreach ($features_action as $key => $feature) {
          $features[$key] = $feature;
        }

		$product = productService::getProductByFeatures($features);
		//TODO: Enviar location ara esta fixat
		return productService::generateStock($product[0]["ID_Product"], 1, $qty);
	}
}