<?php
include_once "./services/routeService.php";

$not_found = "/404";
$url = explode("?",$_SERVER["REQUEST_URI"]);
$_SERVER["REQUEST_URI"] = $url[0];

$routes = array(
  "/"                         => new routeService("./controller/workshops.php","./templates/workshop.php","list"),
  "/employee/"                => new routeService("./controller/employees.php","./templates/employee.php","list"),
  "/login/"                   => new routeService("./controller/Auth.php","./templates/login.php","login","",""),
  "/logout/"                  => new routeService("./controller/Auth.php","","logout","",""),
  "/products/"                => new routeService("./controller/products.php","./templates/products-list.php","list"),
  "/task/"                    => new routeService("./controller/tasks.php","./templates/task-list.php","list"),
  //TODO: s'ha de canviar
  "/task/[0-9]+"              => new routeService("./controller/tasks.php","./templates/task-step.php","step"),
  "/order/"                   => new routeService("./controller/orders.php","./templates/order-list.php","list"),
  "/receipt/"                 => new routeService("./controller/receipts.php","./templates/receipt-list.php","list"),
  /* AJAX */
  "/ajax/employee/"           => new routeService("./controller/Ajax.php","","employee","",""),
  "/ajax/dropzone/"           => new routeService("./controller/Ajax.php","","dropzone","",""),
  "/ajax/features/"           => new routeService("./controller/Ajax.php","","FeaturesFamily","",""),
  "/ajax/location/"           => new routeService("./controller/Ajax.php","","locationByProduct","",""),
  "/ajax/product/"            => new routeService("./controller/Ajax.php","","product","",""),
  "/ajax/action/"             => new routeService("./controller/Ajax.php","","action","",""),
  "/ajax/actionGenerate/"     => new routeService("./controller/Ajax.php","","actionGenerate","",""),
  "/ajax/saveHistoric/"       => new routeService("./controller/Ajax.php","","saveHistoric","",""),
  /* ADMIN */
  "/admin/employee/"          => new routeService("./controller/employees.php","./templates/admin/employee-list.php","alist"),
  "/admin/employee/[0-9]+"    => new routeService("./controller/employees.php","./templates/admin/employee-detail.php","aedit"),
  "/admin/family/"            => new routeService("./controller/families.php","./templates/admin/family-list.php","alist"),
  "/admin/family/[0-9]+"      => new routeService("./controller/families.php","./templates/admin/family-detail.php","aedit"),
  "/admin/feature/"           => new routeService("./controller/features.php","./templates/admin/feature-list.php","alist"),
  "/admin/feature/[0-9]+"     => new routeService("./controller/features.php","./templates/admin/feature-detail.php","aedit"),
  "/admin/location/"          => new routeService("./controller/locations.php","./templates/admin/location-list.php","alist"),
  "/admin/location/[0-9]+"    => new routeService("./controller/locations.php","./templates/admin/location-detail.php","aedit"),
  "/admin/product/"           => new routeService("./controller/products.php","./templates/admin/products-list.php","alist"),
  "/admin/product/[0-9]+"     => new routeService("./controller/products.php","./templates/admin/products-detail.php","aedit"),
  "/admin/taskproject"        => new routeService("./controller/projects.php","./templates/admin/project-list.php","alist"),
  "/admin/taskproject/[0-9]+" => new routeService("./controller/projects.php","./templates/admin/project-detail.php","aedit"),
  "/admin/task/"              => new routeService("./controller/tasks.php","./templates/admin/task-list.php","alist"),
  "/admin/task/[0-9]+"        => new routeService("./controller/tasks.php","./templates/admin/task-detail.php","aedit"),
  "/admin/taskflow/[0-9]+"    => new routeService("./controller/projects.php","./templates/admin/task-flow.php","aflow"),
  "/admin/taskhistoric/"      => new routeService("./controller/tasks.php","./templates/admin/task-historic.php","ahistoric"),
  "/admin/workshop/"          => new routeService("./controller/workshops.php","./templates/admin/workshop-list.php","alist"),
  "/admin/workshop/[0-9]+"    => new routeService("./controller/workshops.php","./templates/admin/workshop-detail.php","aedit"),
  /*SETTINGS*/
  "/settings"                 => new routeService("./controller/settings.php","./templates/admin/settings.php","general"),
  /* ERRORS */
  "/404"                      => new routeService("","./templates/_404.php","","",""),
);

$route = $routes[$not_found];
$keys_routes = array_keys($routes);

foreach ($keys_routes as $index => $key) {
  if (preg_match("/^" . str_replace("/","\/",$key) ."$/", $_SERVER["REQUEST_URI"])) {
    $route = $routes[$key];
  }
}

$id_search = explode("/",$_SERVER["REQUEST_URI"]);
$id_search = $id_search[count($id_search)-1];
if (preg_match("/[0-9]+/",$id_search)) {
  $route->setId($id_search);
}

if (array_key_exists($_SERVER["REQUEST_URI"],$routes)) {
  $route = $routes[$_SERVER["REQUEST_URI"]];
}
if (array_key_exists($_SERVER["REQUEST_URI"] . "/",$routes)) {
  $route = $routes[$_SERVER["REQUEST_URI"] . "/"];
}


$route->exec();
