<?php

include_once "./services/languageService.php";
include_once "./services/authService.php";
include_once "./services/uiService.php";
include_once "./services/notificationService.php";
include_once "./services/validationService.php";
include_once "./services/searchService.php";
include_once "./services/employeeService.php";
include_once "./services/productService.php";

class RouteService {
  private $auth = "./controller/Auth.php";
  private $controller;
  private $function;
  private $template;
  private $header = "./templates/_header.php";
  private $footer = "./templates/_footer.php";
  private $language;
  public $data;
  public $request;
  public $notification;
  public $employee;
  public $alert;

  private $url_workshop = array("/", "/login", "/logout", "/admin/workshop/0", "/employee");
  private $url_employee = array("/admin/employee/0", "/task", "/ajax/employee");

  public function __construct($controller, $template, $function, $header = "none", $footer = "none"){
    if ($header != "none") {
      $this->header =  $header;
    }

    if ($footer != "none") {
      $this->footer =  $footer;
    }

    $this->controller = $controller;
    $this->function = $function;
    $this->template = $template;
  }

  public function setFinalValues() {
    $this->language = languageService::getInstance();
    $this->notification = new notificationService;
    $this->alert["Stock"] = productService::getAlertStock();
  }

  public function execController() {
    if ($this->controller != "") {
      include $this->controller;
      $this->data = $this->execFunction($controller);
    }
  }

  public function execFunction($controller) {
    if ($this->function != "") {
      $func = $this->function;
      return $controller->$func($this->request, $this->notification);
    }
  }

  public function execTemplate() {
    if ($this->template != "") {
      if (array_key_exists("employee", $_SESSION)) {
        $this->employee = employeeService::getEmployee($_SESSION["employee"]);
      }

      $this->execHeader();
      include $this->template;
      $this->execFooter();
    }
  }

  public function execHeader() {
    if ($this->header != "") {
      include $this->header;
    }
  }

  public function execFooter() {
    if ($this->footer != "") {
      include $this->footer;
    }
  }

  public function execAuth() {
    return authService::validation();
  }

  public function hideElement($element) {
    $notShow = array(
      "employee"  => "/employee",
      "workshop"  => "",
    );

    return $_SERVER["REQUEST_URI"] != $notShow[$element] && $_SERVER["REQUEST_URI"] != $notShow[$element]."/";
  }

  public function exec() {
    $this->setFinalValues();

    if ($_SERVER["REQUEST_URI"] == "/login" && $this->execAuth()) {
      authService::redirect("/");

    } else if($this->execAuth() || $_SERVER["REQUEST_URI"] == "/login"){
      if (!array_key_exists("workshop" ,$_SESSION) && !in_array($_SERVER["REQUEST_URI"], $this->url_workshop)) {
        authService::redirect("/");

      } else if (!in_array($_SERVER["REQUEST_URI"], $this->url_workshop)) {
        if (!array_key_exists("employee" ,$_SESSION) && !in_array($_SERVER["REQUEST_URI"], $this->url_employee)) {
          authService::redirect("/employee");
        }
      }

      if (authService::allowAccess($_SERVER["REQUEST_URI"])) {
        $this->execController();
        $this->execTemplate();
      } else {
        authService::redirect("/task");
      }

    } else {
      authService::redirect("/login");
    }
  }

  public function setId($id){
    $this->request["id"] = $id;
  }

  public function getValue($value, $ids){
    if (!is_array($ids)) {
      $ids = str_replace("]", "", $ids);
      $ids = explode("[", $ids);
    }

    foreach ($ids as $id) {
      if (is_array($value) && array_key_exists($id, $value) && ($value[$id] || $value[$id] == 0)) {
        $value = $value[$id];
      } else {
        return "";
      }
    }

    return $value;
  }
}
