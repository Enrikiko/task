<?php
include_once "./models/roleModel.php";

class roleService{

  public static function getAll(){
    $group = new roleModel;
    return $group->find();
  }

  public static function getAdmin() {
    $group = new roleModel;
    return $group->find("Role = 'Admin'");
  }

}
