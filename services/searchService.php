<?php

class searchService {
	public $type;
	public $name;
	public $object;
	public $label;
	public $width;

	public static function getSelect($name, $label, $object_name, $object_option, $width = "3") {
		$search = new searchService;

		$search->type = "select";
		$search->name = $name;
		$search->object = array("name" => $object_name, "option" => $object_option);
		$search->label = $label;
		$search->width = $width;

		return $search;
	}

	public static function getInputText($name, $label, $width = "3") {
		$search = new searchService;

		$search->type = "text";
		$search->name = $name;
		$search->label = $label;
		$search->width = $width;

		return $search;
	}
}