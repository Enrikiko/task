<?php
include_once "./models/userModel.php";
include_once "./services/employeeService.php";
include_once "./services/roleService.php";
include_once "./services/fileService.php";

class authService{
  public static function validation() {
    $login = array();
    if (array_key_exists("login", $_POST) && array_key_exists("password", $_POST)) {
      $login["user"] = $_POST["login"];
      $login["pass"] = $_POST["password"];
    
    } else if (array_key_exists("user", $_SESSION) && array_key_exists("pass", $_SESSION)) {
      $login["user"] = $_SESSION["user"];
      $login["pass"] = $_SESSION["pass"];
    
    } else if (array_key_exists("login", $_COOKIE) && array_key_exists("pass", $_COOKIE["login"]) && array_key_exists("user", $_COOKIE["login"])) {
      $login["user"] = $_COOKIE["login"]["user"];
      $login["pass"] = $_COOKIE["login"]["pass"];
    }

    if ($login) {
        if($user = authService::isUser($login["user"], $login["pass"])) {

          $_SESSION["user"] = $user["username"];
          $_SESSION["pass"] = $user["password"];
          $_SESSION["ID_User"] = $user["ID_User"];
          $_SESSION["logo"] = $user["Image"];

          setcookie( "login[user]", $user["username"], strtotime( '+30 days' ) );
          setcookie( "login[pass]", $user["password"], strtotime( '+30 days' ) );
          return true;
        }
    }

    return false;
  }

  public static function isUser($login, $password) {
    $users = new UserModel;
    $results = $users->findOne("(username = '$login' OR email = '$login') AND password = '$password'");

    return $results;
  }

  public static function setUser($email, $company) {
    $user = new UserModel;
    fileService::renameFile($_SESSION["ID_User"], "user");
    $user->update($_SESSION["ID_User"], $user->columnsAndValues($email, $company));
  }

  public static function logout(){
    session_destroy();
    setcookie('login[user]', '', 1);
    setcookie('login[pass]', '', 1);
    authService::redirect("/");
  }

  public static function redirect($url){
    header("Location: ". $url);
    exit();
  }

  public static function isAdmin() {
    if (array_key_exists("employee", $_SESSION)) {
      $roles = roleService::getAdmin();
      $employee = employeeService::getEmployee($_SESSION["employee"]);

      foreach ($roles as $key => $rol) {
        if($employee["ID_Role"] == $rol["ID_Role"]) {
          return true;
        }
      }
    } else {
      $employees = employeeService::getAll();
      if (count($employees) == 0) {
        return true;
      }
    }
    return false;
  }

  public static function inSection($section) {
    if (strpos($_GET["u"], $section) !== false) {
      return true;
    }
    return false;
  }

  public static function allowAccess($url) {
    if (strpos($url, "admin") !== false || strpos($url, "settings") !== false) {
      return authService::isAdmin();
    }
    
    return true;
  }

  public static function setImage($id, $path) {
    $user = new UserModel;
    return $user->update($id,array("Image" => $path));
  }
}
