<?php
include_once "./models/familyModel.php";
include_once "./models/featureModel.php";
include_once "./models/productModel.php";

class familyService {

  public static function getAll($where = "", $limit = "", $order = "type, Description", $show_id_as_key = false) {
    $family = new familyModel;
    return $family->find($where, "", $order, $limit, $show_id_as_key);
  }

  public static function getAllProduct($where = "") {
    return familyService::getAllType('P', $where);
  }

  public static function getAllTask($where = "") {
    return familyService::getAllType('T', $where);
  }

  public static function getAllType($type, $where = "") {
    return familyService::getAll("Type = '$type'" . ($where ? " AND " . $where : ""),"","", true);
  }

  public static function getFamily($id_family) {
    $family = new familyModel;
    return $family->findOne("ID_Family = '$id_family'");
  }

  public static function delete($id_family) {
    $family = new familyModel;
    $product = new productModel;
    $product->execSql("UPDATE $product->tableName SET ID_Family = 0 WHERE ID_Family = '$id_family' AND " . $product->onlyUserData());
    return $family->delete($id_family);
  }

  public static function insert($description, $type, $father, $state_matter) {
    $family = new familyModel;
    return $family->insert($family->columnsAndValues($description, $type, $father, $state_matter));
  }

  public static function update($id_family,$description,$type, $father, $state_matter) {
    $family = new familyModel;
    return $family->update($id_family,$family->columnsAndValues($description, $type, $father, $state_matter));
  }

  public static function getTypes() {
    $family = new familyModel;
    return $family->types;
  }

  public static function getStateMatter() {
    $family = new familyModel;
    $result = array();

    foreach ($family->state_matter as $key => $state) {
      $result[$state] = featureService::getFeatureState($state);
    }

    return $result;
  }

  public static function getPages($where) {
    $model = new familyModel;
    return $model->getPages($where);
  }

  public static function setPreview($id, $preview) {
    $family = new familyModel;
    return $family->update($id,array("Preview_Product" => $preview));
  }

  public static function getAllPreview() {
    return familyService::getAll("", "", "", true);
  }

  public static function deletePreviewByIDFeature($id_feature) {
    $model = new familyModel;
    
    $sql = "SELECT ID_Family, Preview_Product
            FROM Family
            WHERE Preview_Product LIKE '%\"ID_Feature\": \"$id_feature\"%'";
    $families = $model->execSql($sql);

    foreach ($families as $id_family => $family) {
      
      $features = json_decode($family["Preview_Product"], true);
      foreach ($features as $id => $feature) {
        if ($feature["ID_Feature"] == $id_feature) {
          unset($features[$id]);
        }
      }

      familyService::setPreview($family["ID_Family"], json_encode($features));
    }
  }
}
