<?php
include_once "./models/featureModel.php";
include_once "./services/familyService.php";
include_once "./models/featureFamilyModel.php";

class featureService {
	public static function getAll($where = "", $limit = "", $order = "", $show_id_as_key = false) {
		$model = new featureModel;
		return $model->find($where, "", $order, $limit, $show_id_as_key);
	}

  public static function getOrder(){
    $model = new featureModel;
    $results = $model->find("ID_User = '" . $_SESSION["ID_User"] . "'");
    $order = "";

    foreach ($results as $key => $feature) {
      if ($order != "") {
        $order .= ", ";
      }

      $order .= "Attributs->'$.\"" . $feature["ID_Feature"] . "\"'";
    }

    return $order;
  }

	public static function getFeature($id_feature) {
		$model = new featureModel;
		return $model->findOne("ID_Feature = '$id_feature'");
	}

	public static function getUnities() {
    $model = new featureModel;
    return $model->unities;
  }

  public static function insert($name, $unity) {
    $model = new featureModel;
    return $model->insert($model->columnsAndValues($name, $unity));
  }

	public static function update($id_feature, $name, $unity) {
    $model = new featureModel;
    return $model->update($id_feature, $model->columnsAndValues($name, $unity));
  }

	public static function delete($id_feature) {
    familyService::deletePreviewByIDFeature($id_feature);

    $model = new featureModel;
    $model->delete($id_feature);
  }

  public static function getFeaturesByFamily($id_family, $where = "") {
    $model = new featureModel;
    $sql = "SELECT feature.ID_Feature, feature.Name, featurefamily.Rest , family.ID_Family_Father, '0' as 'remove', feature.State_matter
            FROM Family family
            LEFT JOIN FeatureFamily featurefamily ON family.ID_Family = featurefamily.ID_Family
            LEFT JOIN Feature feature ON featurefamily.ID_Feature = feature.ID_Feature
            WHERE family.ID_Family = '$id_family' AND family.ID_User = '" . $_SESSION["ID_User"] . "'" . ($where ? " AND " . $where : "");

    $results = $model->execSql($sql);
    
    if (count($results) == 1 && !$results[0]["ID_Feature"]) {
      return array();
    }

    return $results;
  }

  public static function getFeaturesByFather($id_family, $where = "") {
    $features = array();

    while ($id_family != 0) {
      $feature = featureService::getFeaturesByFamily($id_family, $where);
      $id_family = 0;

      if (count($feature)) {
        if ($feature[0]["ID_Feature"]) {
          $features = array_merge($feature, $features);
        }
        $id_family = $feature[0]["ID_Family_Father"];  
      }
    }

    return $features;
  }

  public static function getOrderByRest() {
    $model = new FeatureFamilyModel;
    $order = "";

    $results = $model->find("Rest = 1");

    foreach ($results as $key => $feature) {
      if ($order) {
        $order .= ",";
      }

      $order .= "cast(Attributs->'$.\"" . $feature["ID_Feature"] . "\"' as unsigned)";
    }

    return $order;
  }

  public static function setFeatures($features, $id_family) {
    $model = new featureFamilyModel;
    $features = json_decode($features, true);

    foreach ($features as $key => $feature) {
      $feature["Rest"] = ($feature["Rest"] ? 1 : 0);
      if ($feature["remove"] && !array_key_exists("new", $feature)) {
        $model->delete(array($feature["ID_Feature"],$id_family));
        familyService::deletePreviewByIDFeature($feature["ID_Feature"]);
      } else if (array_key_exists("new", $feature) && !$feature["remove"]) {
        $model->insert($model->columnsAndValues($id_family, $feature["ID_Feature"], $feature["Rest"]));
      } else if (!$feature["remove"]) {
        $model->update(array($feature["ID_Feature"],$id_family),array("Rest" => $feature["Rest"]));
      }
    }
  }

	public static function getPages($where) {
    $model = new featureModel;
    return $model->getPages($where);
  }

  public static function getFeatureVariance($id_family) {
    $model = new featureModel;

    foreach(featureService::getFeaturesByFather($id_family) as $key => $feature){
      if ($feature["State_matter"]) {
        return $feature["ID_Feature"];
      }
    }

    return 0;
  }

  public static function getFeatureState($state) {
    $model = new featureModel;
    return $model->find("State_matter = '$state'");
  }
}