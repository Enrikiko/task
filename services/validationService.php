<?php

class validationService {
	public static function isCorrect($value, $pattern = "", $length = 0) {
		$pass = true;

		if ($pattern != "") {
			$pass = preg_match($pattern, $value, $match);
		}

		if ($length != 0) {
			$pass = strlen($value) <= $length;
		}

		return !($pass && ($value || $value == '0'));
	}
}