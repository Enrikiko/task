<?php
include_once "./models/projectModel.php";
include_once "./models/projectTaskModel.php";

include_once "./services/taskService.php";

class projectService {
	public static function getAll($where = "", $limit = "") {
		$model = new projectModel;
		return $model->find($where, "", "", $limit);
	}

	public static function getProject($id_project) {
		$model = new projectModel;
    return $model->findOne("ID_Project = '$id_project'");
	}

	public function getAllTask($id_project) {
		$model = new projectModel;
		$sql = "SELECT t.ID_Task, t.Name, t.Description, '0' as 'in_project' 
						FROM Task t";

		if ($id_project) {
			$sql .= " WHERE t.ID_Task NOT IN (
									SELECT t.ID_Task
									FROM Task t
									LEFT JOIN ProjectTask pt ON pt.ID_Task = t.ID_Task
									WHERE pt.ID_Project = '$id_project' AND t.ID_User = '" . $_SESSION["ID_User"] . "') AND t.ID_User = '" . $_SESSION["ID_User"] . "'
								UNION
								SELECT t.ID_Task, t.Name, t.Description, p.ID_Project as 'in_project'
								FROM Task t
								LEFT JOIN ProjectTask pt ON pt.ID_Task = t.ID_Task
								LEFT JOIN `Project` p ON p.ID_Project = pt.ID_Project
								WHERE p.ID_Project = '$id_project' AND t.ID_User = '" . $_SESSION["ID_User"] . "'

								ORDER BY ID_Task";
		}

		return $model->execSql($sql);
	}

	public static function insert($name, $in_project) {
		$model = new projectModel;
		$id_project = $model->insert($model->columnsAndValues($name));
		
		projectService::insertTasks($id_project, $in_project);
		
		return $id_project;
	}

	public static function insertTasks($id_project, $in_project) {
		$model = new projectModel;
		$sql = "INSERT INTO ProjectTask (ID_Project, ID_Task)
            VALUES ";
    $pass = false;

		foreach ($in_project as $id_task => $value) {
			if ($value) {
				if ($pass) {
					$sql .= ",";
				}

				$sql .= "($id_project, $id_task)";
				$pass = true;
			}
		}

		if ($pass) {
			$model->execSql($sql);
		}
	}

	public static function update($id_project, $name, $in_project) {
		$model = new projectModel;
		$model->update($id_project,$model->columnsAndValues($name));
		
		projectService::deleteTasks($id_project);
		projectService::insertTasks($id_project, $in_project);
	}

	public static function delete($id_project) {
		$model = new projectModel;
		$model->delete($id_project);
		projectService::deleteTasks($id_project);
	}

	public static function deleteTasks($id_project) {
		$model = new projectModel;
		$sql = "DELETE FROM ProjectTask WHERE ID_Project = '$id_project'";
		$model->execSql($sql);
	}

	public static function setFlow($id_project, $data_json) {
	    $flow = array();
	    $json = json_decode($data_json, true);

	    foreach ($json["links"] as $key => $link) {
	      $flow[$link["source"]]["ID_Task"][] = array("id" => $link["target"], "type" => $link["type"]);
	    }

	    foreach ($json["data"] as $key => $data) {
	      $flow[$data["id"]]["start_date"] = $data["start_date"];
	    }

	    $task = new projectTaskModel;
	    foreach ($flow as $id_task => $next_steps) {
	        $task->update(array($id_project, $id_task), array("Next_Tasks" => json_encode($next_steps)));
	    }
	}

	public static function getFlow($id_project){
	    $task = new taskModel;
	    $sql = "SELECT t.ID_Task, t.Name, t.Description, pt.Next_Tasks
	            FROM Task t
	            INNER JOIN ProjectTask pt ON t.ID_Task =  pt.ID_Task
	            WHERE pt.ID_Project = '$id_project'";
	    $results = $task->execSql($sql);

	    $links = array();
	    $data = array();
	    $id = 0;

	    foreach ($results as $key => $task) {
	      $start_date = date("d-m-Y");
	      if ($task["Next_Tasks"]) {
	        $task["Next_Tasks"] = json_decode($task["Next_Tasks"],true);
	        $start_date = $task["Next_Tasks"]["start_date"];

	        if (array_key_exists("ID_Task", $task["Next_Tasks"])) {
	          foreach ($task["Next_Tasks"]["ID_Task"] as $key => $id_task) {
	            $links[] = array(
	              "id"      => $id,
	              "source"  => $task["ID_Task"],
	              "target"  => $id_task["id"],
	              "type"    => $id_task["type"],
	            );

	            $id++;
	          }
	        }
	      }

	      $data[] = array(
	        "id"          => $task["ID_Task"],
	        "text"        => $task["Name"] . "(" . $task["Description"] . ")",
	        "start_date"  => $start_date,
	        "duration"    => 1,
	      );
	    }

	    return array(
	      "data"  => $data,
	      "links" => $links,
	    );
	}

	public static function startProject($id_project) {
		$model = new projectTaskModel;
		$sql = "SELECT pt.ID_Task 
				FROM ProjectTask pt
				INNER JOIN Task t ON t.ID_Task = pt.ID_Task 
				WHERE pt.ID_Project = '$id_project' AND t.ID_User ='" . $_SESSION["ID_User"] . "'";

		$tasks = $model->execSql($sql);
		$model = taskService::newModel();

		foreach ($tasks as $key => $task) {
			taskService::activeTask($task["ID_Task"], $model, $id_project);
		}
	}

	public static function getPages($where = "") {

	}
}