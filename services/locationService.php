<?php
include_once "./models/locationModel.php";
include_once "./services/workshopService.php";

class locationService {
  public static function getLocationByIdWorkshop($id_workshop, $where = "") {
    $location = new locationModel;
    return $location->find("ID_workshop = $id_workshop" . ($where ? " AND " . $where : ""));
  }

  public static function getLocation($id_location) {
    $location = new locationModel;
    $workshops_ids = workshopService::getAllIds();
    return $location->findOne("ID_Location = $id_location AND ID_workshop IN (" . join(',', $workshops_ids) . ")");
  }

  public static function insert($name, $description, $workshop) {
    $location = new locationModel;
    return $location->insert($location->columnsAndValues($name, $description, $workshop));
  }

  public static function update($id_family, $name, $description, $workshop) {
    $location = new locationModel;
    return $location->update($id_family,$location->columnsAndValues($name, $description, $workshop));
  }

  public static function delete($id_location) {
    $stocks = locationService::getStock($id_location);

    foreach ($stocks as $key => $stock) {
      if ($stock["Quantity"]) {
        return false;
      }
    }

    $location = new locationModel;
    $location->delete($id_location);

    return true;
  }

  public static function getStock($id_location) {
    $products = new locationModel;
    $sql = "SELECT stock.ID_Product, stock.Quantity" .
      " FROM Location l LEFT JOIN ProductLocation stock ON l.ID_Location = stock.ID_Location" .
      " WHERE stock.ID_Location = $id_location";

    $results = $products->execSql($sql);
    return $results;
  }

  public static function getPages($where){
    $model = new locationModel;
    return $model->getPages($where);
  }
}
