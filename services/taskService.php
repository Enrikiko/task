<?php
include_once "./models/taskModel.php";
include_once "./models/taskStepsModel.php";
include_once "./models/projectTaskModel.php";
include_once "./models/taskHistoricModel.php";
include_once "./models/actionModel.php";
include_once "./models/taskStepActionModel.php";

include_once "./services/productService.php";
include_once "./services/employeeService.php";
include_once "./services/familyService.php";

class taskService {
  public static function getStockTask($id_employee = 0){
    $sql_task ="";

    if ($id_employee){
      if ($tasks_ids = employeeService::getEmployeeTasks($id_employee)) {
        $sql_task = " AND (th.ID_Employee = $id_employee  OR (th.ID_Employee = '0' AND t.ID_Task IN (" . join(',', $tasks_ids) . "))) ";

      } else {
        return false;
      }
    }

    $sql = "SELECT DISTINCT t.ID_Task, th.Priority, t.Partial, th.Status, t.Name, th.ID_Historic
            FROM Task t
            INNER JOIN TaskHistoric th ON t.ID_Task = th.ID_Task";

    $where = " WHERE t.ID_User = '" . $_SESSION["ID_User"] . "' $sql_task AND t.Enabled = 1 AND th.Status <> 'F' ";

    $historic = new taskHistoricModel;
    return $historic->execSql($sql . $where);
  }

  public static function getAll($where = "", $limit = "", $order = "" ,$show_id_as_key = false) {
    $task = new taskModel;
    return $task->find($where,"",$order,$limit, $show_id_as_key);
  }

  public static function getGroup_Task(){
    $tasks = taskService::getAll();
    $families = familyService::getAllTask();

    foreach ($tasks as $key => $task) {
      $families[$task["ID_Family"]]["tasks"][] = $task;
    }

    if (array_key_exists("0", $families)) {
      $families[0]["Description"]  = "empty";
    }

    return $families;
  }

  public static function getSteps($id_task) {
    $model = new taskStepsModel;
    $results = $model->find("ID_Task = $id_task");
    
    $model = new taskStepActionModel;
    $steps = array();

    foreach ($results as $id_step => $step) {
      $actions = $model->search("ID_Step = '" . $step["ID_Step"] . "'", "taskstepaction.ID_ActionStep, taskstepaction.ID_Action as 'id_action', taskstepaction.Qty as 'qty', taskstepaction.Features_Values as 'values', taskstepaction.ID_Product as 'id_product', '0' as 'del', action.Type as 'type', taskstepaction.Description as 'description'");
      foreach ($actions as $key => $action) {
        $actions[$key]["values"] = json_decode($actions[$key]["values"],true);
      }

      $results[$id_step]["actions"] = $actions;
      $steps[] = array(
        "ID_Step"       => $step["ID_Step"],
        "product"       => $step["ID_Product"],
        "qty"           => $step["Qty"],
        "type"          => ($step["Type"] ? $step["Type"] : 0 ),
        "actions"       => $actions,
        "del"           => "0",
      );
    }

    return $steps;
  }
  
  public static function getHistoricByEmployee($id_employee) {
    $historic = new taskHistoricModel;
    return $historic->find("ID_Employee = '$id_employee'");
  }

  public static function setRest($id_task, $rest_need, $rest_result) {
    $model = new taskModel;
    $model->update($id_task, array("Rest_need" => $rest_need, "Rest_result" => $rest_result));
  }

  public static function getStepsByHistoric($id_historic) {
    $historic = new taskHistoricModel;
    $steps = new taskStepsModel;

    $result_historic = $historic->findOne("ID_Historic =  $id_historic");
    $results_steps = $steps->find("ID_Task = " . $result_historic["ID_Task"]);
    $results_steps["progress"] = $result_historic["Steps"];

    return $results_steps;
  }

  public static function getTaskByHistoric($id_historic) {
    $historic = new taskHistoricModel;
    $task = new taskModel;

    $result_historic = $historic->findOne("ID_Historic =  $id_historic");
    return $task->findOne("ID_Task = " . $result_historic["ID_Task"]);
  }

  public static function getTaskById($id_task) {
      $task = new taskModel;
      return $task->findOne("ID_Task = $id_task");
  }

  public static function generateHistoricTask($id_task, $id_project, $priority, $version) {
    if ($id_project) {
      $model = new projectTaskModel;
      $projectTask = $model->findOne("ID_Project = '$id_project' AND ID_Task = '$id_task'");

      $ids = json_decode($projectTask["Next_Tasks"],true);
      $historic = new taskHistoricModel;

      if (array_key_exists("ID_Task", $ids) && is_array($ids["ID_Task"])) {
        foreach ($ids["ID_Task"] as $id) {
          $sql = "SELECT th.ID_Historic 
                  FROM Task t 
                  INNER JOIN TaskHistoric th ON t.ID_Task = th.ID_Task 
                  WHERE t.ID_User = '" . $_SESSION["ID_User"] . "' AND th.ID_Project = '$id_project' AND t.ID_Task = '" . $id["id"] . "' 
                        AND th.Model = '$version' ORDER BY th.Model DESC LIMIT 1";

          if (!count($historic->execSql($sql))) {
            $historic->insert($historic->columnsAndValues($id_project, $id["id"], '0', $version, ($priority > 0 ? $priority-1 : 0 )));
          }
        }
      }
    }
  }

  public static function finishTask($id_historic, $steps, $action_task = "D") {
    $model = new taskHistoricModel;
    $steps = json_decode($steps, true);
//TODO: s'ha de mirar que passa
    if ($action_task == "D") {

      foreach ($steps["need"] as $id_product => $product) {
        foreach ($product as $id_location => $location) {

          if($location["full"][0]){
            productService::deleteStock($id_product, $id_location, $location["full"][0], $id_historic);
          }

          foreach ($location["rest"] as $features => $qty) {
            if ($qty) {
              $features = str_replace(":", "\":\"", $features);
              $features = str_replace("{", "{\"", $features);
              $features = str_replace("}", "\"}", $features);
              $features = str_replace(",", "\",\"", $features);
              productService::deleteStockByFeatures($id_product, $id_location, json_decode($features,true), $id_historic, $qty);
            }
          }
        }
      }

    } else if ($action_task == "A") {
      foreach ($steps as $key => $step) {
        //TODO canviar el 1 per la localització
        productService::generateStock($step["Product"]["ID_Product"], 1, $step["Qty"]);

        foreach ($step["actions"] as $key => $action) {
          //TODO canviar el 1 per la localització
          productService::deleteStock($action["ID_Product"], 1, $action["Qty"], $id_historic);
        }
      }

    } else if ($action_task == "T") {
      print "<pre>";print_r($steps);die;

    }
    die;
    $model->update($id_historic, array("Status" => "F"));
  }

  /*public static function finishTask($id_historic, $stocks) {
    $historic = new taskHistoricModel;
    $methods = array("product_need" => "deleteStock", "product_result" => "generateStock");

    foreach ($methods as $product_action => $method) {
      foreach ($stocks[$product_action] as $key => $product) {
        $quantities = productService::getIdQuanities($product["ID_Product"], $product["ID_Location"], $id_historic);
        $i = 0;

        if (array_key_exists("rest", $product) && $product["rest"] && count($product["rest"])) {
          foreach ($product["rest"] as $key => $qty) {
            
            if($product["result_rest"]) {
              $keys_rest_selected = array_keys($product["rest_selected"]);
              $id_key_rest_selected = $keys_rest_selected[0];
            }

            foreach ($qty["features"] as $id_feature => $feature_value) {

              if ($feature_value) {
                if ($product["result_rest"]) {
                  productService::updateRest($product["rest_selected"][$id_key_rest_selected], $qty["ID_Location"], $id_feature, $feature_value);

                } elseif ($product["qty"]) {
                  productService::updateRest($quantities[$i]["ID_Quantity"], $qty["ID_Location"], $id_feature, $feature_value);

                } else {
                  productService::insertRest($product["ID_Product"], $qty["ID_Location"], $id_feature, $feature_value);
                }
              }
            }

            if ($product["result_rest"]) {
              $product["result_rest"]--;
              unset($product["rest_selected"][$id_key_rest_selected]);

            } elseif ($product["qty"]) {
              $product["qty"]--;
              $i++;

            }
          }
        }

        if ( $product_action == "product_need") {
          if ($product["qty"]) {
            productService::$method($product["ID_Product"], $product["ID_Location"], $product["qty"], $id_historic);
          }
          if($product["result_rest"]) {
            productService::deleteStockByQuantity(array_keys($product["rest_selected"]));
          }

        } else {
          productService::$method($product["ID_Product"], $product["ID_Location"], $product["qty"]);
        }
      }
    }
    $historic->update($id_historic, array("Status" => "F"));
  }*/

  public static function setHistoricStatus($id_historic) {
    $historic = new taskHistoricModel;
    $sql = "UPDATE TaskHistoric SET ID_Employee = '" . $_SESSION["employee"] . "', Status = 'S', date_start = '" . date("Y-m-d H:i:s") . "' WHERE ID_Historic = '$id_historic' AND (ID_Employee = '0' || ID_Employee = '" . $_SESSION["employee"] . "')";
    $historic->execSql($sql);
  }

  public static function setHistoricSteps($id_historic, $steps) {
    $historic = new taskHistoricModel;
    $historic->update($id_historic, array("Steps" => $steps));
  }

  public static function getHistoric($id_historic) {
    $historic = new taskHistoricModel;
    $result = $historic->findOne("ID_Historic = '$id_historic'");
    $result["Steps"] = json_decode($result["Steps"], true);
    return $result;
  }


  public static function insert($name,$description, $observation, $hours, $partial, $family, $steps, $product = 0, $qty = 0) {
    $task = new taskModel;
    $id_task = $task->insert($task->columnsAndValues($name,$description, $observation, $hours, $partial, $family, $product, $qty));

    taskService::updateSteps($id_task, $steps);
    return $id_task;
  }

  public static function update($id_task, $name, $description, $observation, $hours, $partial, $family, $steps, $product = 0, $qty = 0) {
    $task = new taskModel;
    $task->update($id_task, $task->columnsAndValues($name,$description, $observation, $hours, $partial, $family, $product, $qty));
    
    taskService::updateSteps($id_task, $steps);
  }

  public static function updateSteps($id_task, $steps){
    $model = new taskStepsModel;
    $model_action = new taskStepActionModel;
    $create_products = array();
    foreach ($steps as $key => $step) {
      if (array_key_exists("del", $step) && $step["del"]) {
        $model->delete($step["ID_Step"]);

      } else {
        if (array_key_exists("ID_Step", $step)) {
          //$model->update($step["ID_Step"], array("ID_Product" => $step["product"], "Qty" => $step["qty"], "Type" => $step["type"]));
        
        } else {
          //$step["ID_Step"] = $model->insert(array("ID_Task" => $id_task, "ID_Product" => ($step["product"] ? $step["product"] : '0'), "Qty" => $step["qty"], "Type" => $step["type"]));

        }

        foreach ($step["actions"] as $key => $action) {
          if (array_key_exists("del", $action) && $action["del"]) {
            $model_action->delete($action["ID_ActionStep"]);

          } else {
            //$create_products[] = $action["id_product"];
            if (strval($step["type"]) != "agrupation") {
              $id_product = $step["product"];

              if (array_key_exists("type", $action) && $action["type"] == "T") {
                $id_product = $action["id_product"];
              }

              $action["id_product"] = productService::createProductFromFeatures($id_product, $action["values"]);
            }
            
            if (array_key_exists("ID_ActionStep", $action)) {
              $model_action->update($action["ID_ActionStep"], array("ID_Action" => $action["id_action"], "Description" => "", "ID_Product" => $action["id_product"], "Qty" => $action["qty"], "Features_Values" => json_encode($action["values"]), "Description" => $action["description"]));
            
            } else {
              $model_action->insert(array("ID_Step" => $step["ID_Step"], "ID_Action" => $action["id_action"], "Description" => "", "ID_Product" => $action["id_product"], "Qty" => $action["qty"], "Features_Values" => json_encode($action["values"]), "Description" => $action["description"]));
            }
          }
        }
      }
    }

    //TODO: En principi ara es tindria que borrar el attribut tmp dels productes, ja que no es creen amb ajax, fer els canvis necesaris
    /*if (count($create_products)) {
      $model->execSql("UPDATE Product SET TMP = 0 WHERE ID_Product IN (" . join(',', $create_products) . ")");
    }*/
  }

  public static function delete($id_task) {
    $task_step = new taskStepsModel;
    $task = new taskModel;
    $historic = new taskHistoricModel;

    $task_step->delete("ID_Task = '$id_task'",false);
    $historic->delete("ID_Task = '$id_task'",false);
    return $task->delete($id_task);
  }

  public static function activeTask($id_task, $model = 0, $id_project = 0) {
    $historic = new taskHistoricModel;
    $historic->insert($historic->columnsAndValues($id_project, $id_task, '0', $model));
  }
  
  public static function getBeforeTask($id_task){
    $task = new projectTaskModel;
    return $task->find("Next_Tasks LIKE '%\"id\": \"$id_task\"%' OR Next_Tasks LIKE '%\"id\": $id_task%'");
  }

  public static function increasePriority($id_project, $id_task, $priority){
    $historic = new taskHistoricModel;
    $sql = "UPDATE TaskHistoric SET Priority = $priority WHERE ID_Project = '$id_project' AND ID_Task = '$id_task' AND Priority < $priority AND Status <> 'F'";
    $historic->execSql($sql);
  }

  public static function newModel() {
    $task = new taskModel;
    $sql = "SELECT th.Model 
            FROM Task t 
            INNER JOIN TaskHistoric th ON t.ID_Task = th.ID_Task 
            WHERE t.ID_User = '" . $_SESSION["ID_User"] . "' ORDER BY th.Model DESC LIMIT 1";
    $model = $task->execSql($sql);
    return (count($model) ? $model[0]["Model"] + 1 : 1);
  }

  public static function getAllHistoric($where = "", $limit = "") {
    $model = new taskHistoricModel;
    $sql = "SELECT th.ID_Historic, th.date_start, th.Hours, th.Status, th.Priority, t.Name as 'taskName', e.Name
            FROM TaskHistoric th
            INNER JOIN Task t ON t.ID_Task =  th.ID_Task
            LEFT JOIN Employee e ON th.ID_Employee =  e.ID_Employee
            WHERE t.ID_User = '" . $_SESSION["ID_User"] . "'"
            . ($where ? " WHERE " . $where : "");
            //. ($limit ? " LIMIT " . $this->record . " OFFSET " . (($limit-1) * $this->record ) : "");
    return $model->execSql($sql);
  }

  public static function getPages($where){
    $model = new taskModel;
    return $model->getPages($where);
  }

  public static function getStepsToWork($id_hisotiric, $id_task, &$stock) {
    $model = new taskStepsModel;
    $sql = "SELECT ts.*
            FROM TaskSteps ts
            INNER JOIN TaskStepAction tsa ON tsa.ID_Step = ts.ID_Step
            WHERE ts.ID_Task = '$id_task'
            ORDER BY ts.Type DESC, CAST(Features_Values->'$.\"3\"'AS UNSIGNED), CAST(Features_Values->'$.\"4\"'AS UNSIGNED), CAST(Features_Values->'$.\"1\"'AS UNSIGNED)";

    $steps = $model->execSql($sql, true);
    //$steps = $model->find("ID_Task = '$id_task'");

    $model = new taskStepActionModel;
    
    foreach ($steps as $id_step => $step) {
      $steps[$id_step]["Product"] = productService::getProductById($step["ID_Product"]);
      $steps[$id_step]["ID_Step"] = $id_step;
      $steps[$id_step]["Extra"] = json_decode($steps[$id_step]["Extra"],true);
      //$id_feature = featureService::getFeatureVariance($steps[$key]["Product"]["ID_Family"]);

      //$stock[$step["ID_Product"]] = productService::getStockAndLocationsByHistoric($step["ID_Product"], $_SESSION["workshop"], $id_feature, $steps[$key]["Product"]["Attributs"][$id_feature], $id_hisotiric);

      if (count($steps[$id_step]["Product"])) {
        $stock[$steps[$id_step]["Product"]["ID_Product"]] = productService::getStockAndLocationsByHistoric($steps[$id_step]["Product"]["ID_Product"], $_SESSION["workshop"], $id_hisotiric);
      }

      $steps[$id_step]["actions"] = $model->find("ID_Step = '" . $id_step . "'", "", "CAST(Features_Values->'$.\"1\"'AS UNSIGNED)");

      if ($steps[$id_step]["Extra"]) {
        foreach ($steps[$id_step]["actions"] as $key => $actions) {
          $product = productService::getProductById($actions["ID_Product"]);
          $steps[$id_step]["actions"][$key]["image"] = $product["Image"];
          $steps[$id_step]["actions"][$key]["Name"] = $product["Name"];
        }
      }
    }
    
    return $steps;
  }

  public static function getStock(&$stock, $task, $id_hisotiric) {

    $steps = taskService::getSteps($task["ID_Task"]);

//TODO mirar com fer la action
    if ($task["Action"] != "T") {
      $stock_product = productService::getStockAndLocations($task["ID_Product"], $_SESSION["workshop"], $id_hisotiric, true);
      $stock[($task["Action"] == "A" ? "product_result" : "product_need")][] = array(
        "locations"             => $stock_product["locations"],
        "product"               => productService::getProductById($task["ID_Product"]),
        "qty"                   => $task["Quantity"],
        "result"                => 0,
        "result_rest"           => 0,
      );

      if ($task["Action"] == "A") {
        $stock["product_result"][0]["result_locations"] = array();
      } else {
        $stock["product_need"][0]["feature_rest"] = featureService::getFeaturesByFather($stock["product_need"][0]["product"]["ID_Family"], "featurefamily.Rest = 1");
        $stock["product_need"][0]["rest_selected"] = array();
        $stock["product_need"][0]["rest"] = array();

        foreach ($stock["product_need"][0]["locations"] as $key => $location) {
          foreach ($location["rest_features"] as $id_qty => $rest) {
            $stock["product_need"][0]["rest_selected"][""+$id_qty] = 0;
          }
        }
      }
    }

    foreach ($steps as $key => $step) {
      if (is_numeric($key)) {
        if ($step["ID_Product_need"]) {
          $stock_product = productService::getStockAndLocations($step["ID_Product_need"], $_SESSION["workshop"], $id_hisotiric, true);
          $stock["product_need"][] = array(
            "locations"         => $stock_product["locations"],
            "product"           => productService::getProductById($step["ID_Product_need"]),
            "qty"               => $step["Quantity_need"],
            "result"            => 0,
            "result_rest"       => 0,
          );
        }

        if ($step["ID_Product_result"]) {
          $stock_product = productService::getStockAndLocations($step["ID_Product_result"], $_SESSION["workshop"], $id_hisotiric, true);
          $stock["product_result"][] = array(
            "locations"         => $stock_product["locations"],
            "product"           => productService::getProductById($step["ID_Product_result"]),
            "qty"               => $step["Quantity_result"],
            "result"            => 0,
            "result_locations"  => array(),
          );
        }
      }
    }
  }

  public static function generateHistoricJson(){
    $stocks = json_decode($_POST["stocks"], true);
    $json_historic = "";

    foreach ($stocks["product_need"] as $key => $product_need) {
      foreach ($product_need["locations"] as $key => $location) {
        if ($location["lack"] > 0) {
          $json_historic["product_need"][] = array(
              "ID_Product"        => $product_need["product"]["ID_Product"],
              "ID_Family"         => $product_need["product"]["ID_Family"],
              "ID_Location"       => $location["id_Location"],
              "qty"               => $location["lack"],
              "result_rest"       => (array_key_exists("result_rest", $product_need) ? $product_need["result_rest"] : '' ),
              "rest"              => (array_key_exists("rest", $product_need) ? $product_need["rest"] : '' ),
              "rest_selected"     => (array_key_exists("rest_selected", $product_need) ? $product_need["rest_selected"] : ''),
          );
        }
      }
    }

    foreach ($stocks["product_result"] as $key => $product_result) {
      foreach ($product_result["result_locations"] as $key => $location) {
        if ($location["qty"] > 0) {
          $json_historic["product_result"][] = array(
              "ID_Product"        => $product_result["product"]["ID_Product"],
              "ID_Family"         => $product_need["product"]["ID_Family"],
              "ID_Location"       => $location["ID_Location"],
              "qty"               => $location["qty"],
          );
        }
      }
    }

    return $json_historic;
  }

  public static function setStockByHistoric(&$stock, $historic){
    if (is_array($historic["Steps"])) {
      if (array_key_exists("product_need", $historic["Steps"])) {
        foreach ($historic["Steps"]["product_need"] as $key => $product) {
          $qty = 0;

          foreach ($stock["product_need"] as $id_product => $product_need) {
            if ($product["ID_Product"] == $product_need["product"]["ID_Product"]) {

              foreach ($product_need["locations"] as $id_location => $location) {
                if ($product["ID_Location"] == $location["id_Location"]) {
                  $qty += intval($product["qty"]);
                  $stock["product_need"][$id_product]["locations"][$id_location]["lack"] = $product["qty"];
                  $stock["product_need"][$id_product]["result"] += intval($product["qty"]);
                  $stock["product_need"][$id_product]["rest"] = $product["rest"];
                  $stock["product_need"][$id_product]["result_rest"] = $product["result_rest"];
                  $stock["product_need"][$id_product]["rest_selected"] = $product["rest_selected"];
                  
                  break;
                }
              }

              break;
            }
          }
        }
      }

      if (array_key_exists("product_result", $historic["Steps"])) {
        foreach ($historic["Steps"]["product_result"] as $key => $product) {

          foreach ($stock["product_result"] as $id_product => $product_result) {
            if ($product["ID_Product"] == $product_result["product"]["ID_Product"]) {

              foreach ($product_result["locations"] as $id_location => $location) {
                if ($product["ID_Location"] == $location["id_Location"]) {
                  $qty += intval($product["qty"]);
                  $stock["product_result"][$id_product]["result_locations"][] = array(
                      "ID_Location"   => $location["id_Location"],
                      "Name"          => $location["name"],
                      "qty"           => $product["qty"],
                  );
                  $stock["product_result"][$id_product]["result"] += intval($product["qty"]);
                  break;
                }
              }

              break;
            }
          }
        }
      }
    }
  }

  public static function reserveStock($stock, $id_historic) {
    foreach ($stock["product_need"] as $key => $product) {
      if(array_key_exists("rest_selected", $product) && count($product["rest_selected"])) {
        productService::reserveQuantity(array_keys($product["rest_selected"]), $id_historic);
      }

      foreach ($product["locations"] as $key => $location) {
        if ($location["lack"]) {
          taskService::reserveProductByLocation( $product["product"]["ID_Product"], $location["id_Location"], $location["lack"], $id_historic);
        }
      }
    }
  }

  public static function reserveProductByLocation( $id_product, $id_location, $qty, $id_historic) {
    $quantities = productService::getIdQuanities($id_product, $id_location, 0, $qty);
    productService::reserveQuantity(array_column($quantities, 'ID_Quantity'), $id_historic);
  }

  public static function initSteps(&$historic, $stock, $steps){
    $historic["Steps"] = array(
      "actions"    => array(),
      "need"       => array(),
      "extra"      => array(),
    );
    
    foreach ($stock as $id_product => $product) {
      $historic["Steps"]["need"][$id_product] = array();

      foreach ($product as $id_location => $location) {
        $historic["Steps"]["need"][$id_product][$id_location] = array(
          "full"  => array(),
          "rest"  => array()
        );

        if (array_key_exists("full", $location)) {
          foreach ($location["full"] as $id_qty => $qty) {
            $historic["Steps"]["need"][$id_product][$id_location]["full"][$id_qty] = "0";
          }
        }

        if (array_key_exists("rest", $location)) {
          foreach ($location["rest"] as $id_qty => $qty) {
            $key =  json_encode($qty["features"]);
            $historic["Steps"]["need"][$id_product][$id_location]["rest"][str_replace("\"", "", $key)] = "0";
          }
        }
      }
    }

    foreach ($steps as $key => $step) {
      if ($step["Type"] == "optimizeCut") {
        for ($i = 0; $i < count($step["Extra"]); $i++){
          $historic["Steps"]["extra"][$step["ID_Step"]][] = "0";
        }

      } else if ($step["Type"] == "0") {
        $historic["Steps"]["actions"][$step["ID_Step"]] = array();
        
        foreach ($step["actions"] as $key => $action) {
          $historic["Steps"]["actions"][$step["ID_Step"]][$action["ID_ActionStep"]] = "0";
        }
      }
    }
  }

  public static function saveSteps($id_historic, $steps) {
    $model = new taskHistoricModel;
    $model->update($id_historic, array("Steps" => $steps, "Status" => "S"));
  }
}
