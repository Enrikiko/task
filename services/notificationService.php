<?php
 
 class notificationService {
 	public $title;
 	public $message;
 	public $type;
 	public $icon;
 	public $enable = false;
 	private $language;

 	public function setError($message = "", $title = "") {
 		$this->setTitle($title);
 		$this->setMessage($message);

 		$this->type =  "danger";
 		$this->icon = "icon-cross-circle";
 	}

 	public function setWarning($message = "", $title = "") {
 		$this->setTitle($title);
 		$this->setMessage($message);

 		$this->type =  "warning";
 		$this->icon = "icon-warning";
 	}

 	public function setSuccess($message = "", $title = "update") {
 		$this->setTitle($title);
 		$this->setMessage($message);

 		$this->type =  "success";
 		$this->icon = "icon-checkmark-circle";
 	}

 	public function setTitle($title) {
 		$this->setLanguage();
 		if ($title) {
			$this->title = $this->language->getLabel($title);
	 		$this->enable = true;
	 	}
 	}

 	public function setMessage($message) {
 		$this->setLanguage();
 		if ($message || is_array($message)) {
	 		if (is_array($message)) {
	 			foreach ($message as $key => $mes) {
	 				$this->message[] = $this->language->getLabel($mes);
	 			}
	 		} else {
	 			$this->message[] = $this->language->getLabel($message);
	 		}
	 		$this->enable = true;
	 	}
 	}

 	public function show(){
 		return $this->enable && ($this->title != "" || $this->message != "");
 	}

 	private function setLanguage() {
 		if ($this->language == null) {
 			$this->language = languageService::getInstance();
 		}
 	}

 	public static function delete(&$notification, $url = "") {
 	  $service = $url . "Service";
    $notify_true = "delete_" . $url;
    $notify_false = "d_same_" . $url;

    if (array_key_exists("success", $_GET) && $_GET["success"]) {
      $notification->setSuccess("",$notify_true . $_GET["success"]);
    
    } else {
      if (array_key_exists("del", $_POST) && $_POST["del"]) {
        if ($service::delete($_POST["del"])) {
          if ($url) {
            authService::redirect("/admin/" . $url . "?success=".$url);

          } else {
            $notification->setSuccess("",$notify_true);
          }
          
        } else {
          $notification->setError("",$notify_false);
        }
      }
    }
  }
 }