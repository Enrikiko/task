<?php
include_once "./services/productService.php";
include_once "./services/featureService.php";
include_once "./services/workshopService.php";

class products {
  public static $page = "product";

  public function list($request, $notification) {
    if (array_key_exists("cmd-add-stock", $_POST)) {
      productService::generateStock($_POST["product"], $_POST["location"], $_POST["qty"]);
    }

    return array(
      "products"    => productService::getAllProducts(),
      "workshop"    => workshopService::getAllLocations(),
    );
  }

  public function alist($request, $notification) {
    $where = "";
    $limit = 1;
    
    notificationService::delete($notification, $this::$page);

    if (array_key_exists("del", $_POST) && $_POST["del"]) {
      productService::delete($_POST["del"]);
      $notification->setSuccess("","delete_product");
    }

    if (array_key_exists("limit", $_POST)) {
      $limit = $_POST["limit"];
    }

    if (array_key_exists("ID_Family", $_POST) && $_POST["ID_Family"]) {
      $where .= ($where ? " AND " : "") . "ID_Family =  '" . $_POST["ID_Family"] . "'";
    }

    if (array_key_exists("Name", $_POST) && $_POST["Name"]) {
      $where .= ($where ? " AND " : "") . "Name LIKE '%" . $_POST["Name"] . "%'";
    }

    if (array_key_exists("Description", $_POST) && $_POST["Description"]) {
      $where .= ($where ? " AND " : "") . "Description LIKE '%" . $_POST["Description"] . "%'";
    }

    if (array_key_exists("Feature", $_POST)) {
      $where_feature = "";
      
      foreach ($_POST["Feature"] as $key => $feature) {
        if ($feature) {
          $where_feature .= ($where_feature ? " AND " : "") . "Attributs->'$.\"$key\"' = '" .$feature ."'";
        }
      }
      
      $where .= ($where_feature ? ($where ? " AND " : "") . $where_feature : "");
    }

    $products = productService::getAllProducts($where ,$limit);

    $items_search = array(
      searchService::getSelect("ID_Family", "family", "family", "Description"),
      searchService::getInputText("Name", "name"),
      searchService::getInputText("Description", "description" ,6)
    );

    $features = featureService::getAll();
    foreach ($features as $key => $feature) {
      $items_search[] =  searchService::getInputText("Feature[" . $feature["ID_Feature"] ."]", $feature["Name"],2);
    }

    return array(
      "family"          => familyService::getAllProduct(),
      "products"        => $products,
      "page"            => $this::$page,
      "request"         => $_POST,
      "items-search"    => $items_search,
      "pagination"      => productService::getPages($where),
    );
  }

  public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);

    $product = array();

    if (array_key_exists("success", $_GET)) {
        $notification->setSuccess("","create_product");
    }

    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();
      /*if(validationService::isCorrect($_POST["Name"],"",50)) { $notification->setError($language->getLabelWithValues("error_name",50)); }
      if(validationService::isCorrect($_POST["Description"],"",250)) { $notification->setError($language->getLabelWithValues("error_description",250)); }*/

      if (!$notification->enable) {
        $id_product = 0;
        $min_quantity = 0;

        if (array_key_exists("Min_quantity", $_POST)) {
          $min_quantity = $_POST["Min_quantity"];
        }

        if (array_key_exists("Min_enabled", $_POST)) {
          $_POST["Min_enabled"] = 1;
        } else {
          $_POST["Min_enabled"] = 0;
        }

        if (!array_key_exists("features", $_POST)) {
          $_POST["features"] = "";
        }

        if (array_key_exists("cmd-save", $_POST)) {
          $id_product = productService::insert($_POST["Name"],$_POST["Description"], $_POST["ID_Family"], json_encode($_POST["features"]), $min_quantity);
          authService::redirect("/admin/product/$id_product?success");

        } else if (array_key_exists("cmd-overwrite", $_POST)) {
          productService::update($_POST["cmd-overwrite"], $_POST["Name"],$_POST["Description"], $_POST["ID_Family"], json_encode($_POST["features"]), $min_quantity);
          $id_product = $_POST["cmd-overwrite"];
        }

        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }

    if($request["id"]) {
      $product = productService::getProductById($request["id"], true);

      if (!count($product)) {
        authService::redirect("/admin/product/0");
      }

    }

    return array(
      "product"     => array_merge($product, $_POST),
      "family"      => familyService::getAllProduct(),
      "workshop"    => workshopService::getAllLocations(),
      "page"        => $this::$page,
    );
  }

}

$controller = new products;
