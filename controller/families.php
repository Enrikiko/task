<?php
include_once "./services/familyService.php";
include_once "./services/featureService.php";

class families {
  public static $page = "family";

  public function alist($request, $notification){
    $where = "";
    $limit = 1;

    notificationService::delete($notification, $this::$page);

    if (array_key_exists("limit", $_POST)) {
      $limit = $_POST["limit"];
    }

    if (array_key_exists("ID_type_family", $_POST) && $_POST["ID_type_family"]) {
      $where .= ($where ? " AND " : "") . "Type =  '" . $_POST["ID_type_family"] . "'";
    }

    if (array_key_exists("Description", $_POST) && $_POST["Description"]) {
      $where .= ($where ? " AND " : "") . "Description LIKE '%" . $_POST["Description"] . "%'";
    }

    $types = familyService::getTypes();

    $type_family = array();
    $language = languageService::getInstance();
    foreach ($types as $key => $name) {
      $type_family[] = array(
        "ID_type_family"  => $key,
        "Description"  => $language->getLabel($name),
      );
    }

    return array(
      "type"        => $types,
      "families"    => familyService::getAll($where, $limit),
      "type_family" => $type_family,
      "page"        => $this::$page,
      "request"     => $_POST,
      "items-search"    => array(
          searchService::getSelect("ID_type_family", "family", "type_family", "Description"),
          searchService::getInputText("Description", "description" ,6)
      ),
      "pagination"  => familyService::getPages($where),
    );
  }

  public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);

    $family = array();
    $family_features = array();
    $features_family = array();
    $features = array();

    if (array_key_exists("success", $_GET)) {
        $notification->setSuccess("","create_family");
    }

    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();
      if(validationService::isCorrect($_POST["Description"],"",50)) { $notification->setError($language->getLabelWithValues("error_description",50)); }
      if(validationService::isCorrect($_POST["Type"])) { $notification->setError("error_type"); }

      if (!$notification->enable) {
        $id = 0;
        if (array_key_exists("cmd-save",$_POST)) {
          if (!array_key_exists("ID_Father", $_POST)){
            $_POST["ID_Father"] = 0;
          }

          $id = familyService::insert($_POST["Description"], $_POST["Type"], $_POST["ID_Father"], $_POST["State_matter"]);
          featureService::setFeatures($_POST["features"], $id);
          authService::redirect("/admin/family/$id?success");

        } else if (array_key_exists("cmd-overwrite",$_POST)) {
          familyService::update($_POST["cmd-overwrite"], $_POST["Description"], $_POST["Type"], $_POST["ID_Father"], $_POST["State_matter"]);
          $id = $_POST["cmd-overwrite"];
          featureService::setFeatures($_POST["features"], $id);
        }

        if ($id && array_key_exists("preview", $_POST)) {
          familyService::setPreview($id, $_POST["preview"]);
        }

        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }

    if ($request["id"]) {
      $family = familyService::getFamily($request["id"]);
      $families = familyService::getAllProduct("ID_Family <> '" . $family["ID_Family"] . "'");
      $family_features =  featureService::getFeaturesByFamily($request["id"]);
      $features_family = featureService::getFeaturesByFather($family["ID_Family_Father"]);
      $features = featureService::getAll($request["id"]);

      if (!count($family)) {
        authService::redirect("/admin/family/0");
      }
    } else {
      $families = familyService::getAllProduct();
      $features = featureService::getAll();
    }

    return array(
      "type"              => familyService::getTypes(),
      "features"          => $features,
      "family_features"   => $family_features,
      "features_family"   => $features_family,
      "family"            => array_merge($family, $_POST),
      "families"          => $families,
      "state_matter"      => familyService::getStateMatter(),
      "page"              => $this::$page,
    );
  }
}

$controller = new families;

