<?php
include_once "./services/projectService.php";

class projects {
	public static $page = "taskproject";

	public function alist($request, $notification) {
    $where = "";
    $limit = 1;

    if (array_key_exists("startProject", $_POST)) {
      projectService::startProject($_POST["startProject"]);

    }

		return array(
			"projects"				=> projectService::getAll($where ,$limit),
		  "page"            => $this::$page,
		  "request"         => $_POST,
		  "pagination"      => projectService::getPages($where),
		);
	}

	public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);
    
    if (array_key_exists("success", $_GET)) {
        $notification->setSuccess("","create_project");
    }
    
    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();
      if(validationService::isCorrect($_POST["Name"],"",50)) { $notification->setError($language->getLabelWithValues("error_name",50)); }

      if (!$notification->enable) {
      	if (array_key_exists("cmd-save", $_POST)) {
          $id_project = projectService::insert($_POST["Name"],$_POST["in_project"]);
          authService::redirect("/admin/taskproject/$id_project?success");

        } else if (array_key_exists("cmd-overwrite", $_POST)) {
          projectService::update($_POST["cmd-overwrite"], $_POST["Name"],$_POST["in_project"]);
        }

        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }



		return array(
			"project"					=> projectService::getProject($request["id"]),
			"tasks"						=> projectService::getAllTask($request["id"]),
      "page"          	=> $this::$page,
		);
	}

  public function aflow($request, $notification) {
    if (array_key_exists("cmd-save",$_POST)) {
      projectService::setFlow($request["id"], $_POST["cmd-save"]);
    }

    $flow = projectService::getFlow($request["id"]);

    /*if (!count($flow["data"])) {
      authService::redirect("/admin/taskflow?flow");
    }*/

    return array(
      "flow"   => $flow,
    );
  }
}

$controller = new projects;