<?php
include_once "./services/employeeService.php";
include_once "./services/taskService.php";
include_once "./services/roleService.php";
include_once "./services/familyService.php";

class employees {
  public static $page = "employee"; 

  public function list($request, $notification) {
    unset($_SESSION["employee"]);
    if (array_key_exists("workshop", $_POST)) {
      $_SESSION["workshop"] = $_POST["workshop"];
    }

    if (!array_key_exists("workshop", $_SESSION)) {
      authService::redirect("/");
    }

    return array(
      "employees_disc"    => employeeService::getEmployeesDisconnected($_SESSION["workshop"]),
      "employees_connec"  => employeeService::getEmployeesConnected($_SESSION["workshop"]),
    );
  }

  public function alist($request, $notification) {
    $where = "";
    $limit = 1;

    notificationService::delete($notification, $this::$page);

    if (array_key_exists("limit", $_POST)) {
      $limit = $_GET["limit"];
    }

    if (array_key_exists("ID_Role", $_POST) && $_POST["ID_Role"]) {
      $where .= ($where ? " AND " : "") . "ID_Role =  '" . $_POST["ID_Role"] . "'";
    }

    if (array_key_exists("Name", $_POST) && $_POST["Name"]) {
      $where .= ($where ? " AND " : "") . "Name LIKE '%" . $_POST["Name"] . "%'";
    }

    $employees =  employeeService::getAll($where, $limit);
    foreach ($employees as $key => $employee) {
      $horario = $employee["Horario"];

      if ($employee["Horario"] != "null") {
        $horario = json_decode($employee["Horario"], true);
        $employees[$key]["Horario"] = $horario["since"]["m"] . " - " . $horario["until"]["m"];

        if (is_array($horario["since"]) && array_key_exists("a", $horario["since"])) {
          $employees[$key]["Horario"] .= ", " . $horario["since"]["a"] . " - " . $horario["until"]["a"];
        }

      } else {
        $employees[$key]["Horario"] = "-";
      }
    }

    return array(
      "rol"             => roleService::getAll(),
      "employees"       => $employees,
      "page"            => $this::$page,
      "request"         => $_POST,
      "items-search"    => array(
          searchService::getSelect("ID_Role", "rol", "rol", "Description"),
          searchService::getInputText("Name", "name" ,6)
      ),
      "pagination"  => employeeService::getPages($where),
    );
  }

  public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);

    $employee = array();
    if (array_key_exists("success", $_GET)) {
        $notification->setSuccess("","create_employee");
    }

    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();
      if(validationService::isCorrect($_POST["Name"],"",50)) { $notification->setError($language->getLabelWithValues("error_name",50)); }
      if(validationService::isCorrect($_POST["ID_Role"])) { $notification->setError("error_role"); }

      if (!$notification->enable) {
        if (array_key_exists("cmd-save",$_POST)) {
          $id = employeeService::insert($_POST["Name"], array("since" => $_POST["since"], "until" => $_POST["until"]), $_POST["ID_Role"], $_POST["tasks"]);
          authService::redirect("/admin/employee/$id?success");

        } else if (array_key_exists("cmd-overwrite",$_POST)) {
          employeeService::update($_POST["cmd-overwrite"], $_POST["Name"], array("since" => $_POST["since"], "until" => $_POST["until"]), $_POST["ID_Role"], $_POST["tasks"]);
        }
        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }

    if ($request["id"]) {
      $employee = employeeService::getEmployee($request["id"]);

      if (!count($employee)) {
        authService::redirect("/admin/employee/0");
      }
      
      $employee["Horario"] = json_decode($employee["Horario"],true);
    }

    return array(
      "role"            => roleService::getAll(),
      "tasks"           => taskService::getGroup_Task(),
      "employee"        => array_merge($employee, $_POST),
      "family"          => familyService::getAllTask(),
      "page"            => $this::$page,
    );
  }
}

$controller = new employees;
