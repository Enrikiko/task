<?php
include_once "./services/workshopService.php";
include_once "./services/locationService.php";

class locations {
  public static $page = "location";

  public function alist($request, $notification){
    $where = "";
    $id_workshop = "";

    notificationService::delete($notification, $this::$page);

    if (array_key_exists("limit", $_POST)) {
      $limit = $_POST["limit"];
    }

    if (array_key_exists("ID_workshop", $_POST) && $_POST["ID_workshop"]) {
      $id_workshop = $_POST["ID_workshop"];
    }

    if (array_key_exists("Name", $_POST) && $_POST["Name"]) {
      $where .= ($where ? " AND " : "") . "Name_location LIKE '%" . $_POST["Name"] . "%'";
    }

    if (array_key_exists("Description", $_POST) && $_POST["Description"]) {
      $where .= ($where ? " AND " : "") . "Description_location LIKE '%" . $_POST["Description"] . "%'";
    }

    $workshops = workshopService::getAllLocations($id_workshop, $where);
    $count_locations = 0;
    foreach ($workshops as $key => $workshop) {
      $count_locations += count($workshop["locations"]);
    }

    return array(
      "workshop"      => $workshops,
      "n_locations"   => $count_locations,
      "page"          => $this::$page,
      "request"       => $_POST,
      "items-search"    => array(
          searchService::getSelect("ID_workshop", "workshop", "workshop", "name"),
          searchService::getInputText("Name", "Name"),
          searchService::getInputText("Description", "description"),
      ),
      "pagination"  => locationService::getPages($where),
    );
  }

  public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);
    
    $location = array();

    if (array_key_exists("success", $_GET)) {
        $notification->setSuccess("","create_family");
    }

    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();
      if(validationService::isCorrect($_POST["Name_location"],"",50)) { $notification->setError($language->getLabelWithValues("error_name",50)); }
      if(validationService::isCorrect($_POST["Description_location"],"",100)) { $notification->setError($language->getLabelWithValues("error_description",100)); }
      if(validationService::isCorrect($_POST["ID_workshop"])) { $notification->setError("error_workshop"); }

      if (!$notification->enable) {
        if (array_key_exists("cmd-save",$_POST)) {
          $id = locationService::insert($_POST["Name_location"],$_POST["Description_location"], $_POST["ID_workshop"]);
          authService::redirect("/admin/location/$id?success");

        } else if (array_key_exists("cmd-overwrite",$_POST)) {
          locationService::update($_POST["cmd-overwrite"],$_POST["Name_location"], $_POST["Description_location"], $_POST["ID_workshop"]);
        }

        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }

    if ($request["id"]) {
      $location = locationService::getLocation($request["id"]);

      if (!count($location)) {
        authService::redirect("/admin/location/0");
      }
    }

    return array(
      "workshop"    => workshopService::getAll(),
      "location"    => array_merge($location, $_POST),
      "page"        => $this::$page,
    );
  }
}

$controller = new locations;
