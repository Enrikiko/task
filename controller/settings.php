<?php

class settings {

  public function general($request, $notification) {
    if (!authService::isAdmin()) {
      authService::redirect("/task");
    }

    if (array_key_exists("cmd-save",$_POST)) {
      $language = languageService::getInstance();
      /*if(validationService::isCorrect($_POST["Name"],"",50)) { $notification->setError($language->getLabelWithValues("error_name",50)); }
      if(validationService::isCorrect($_POST["Description"],"",250)) { $notification->setError($language->getLabelWithValues("error_description",250)); }*/

      if (!$notification->enable) {
        authService::setUser($_POST["email"], $_POST["company_name"]);
      }
    }

    return array(
      "user"     => array_merge(authService::isUser($_SESSION["user"], $_SESSION["pass"]), $_POST),
    );
  }

}

$controller = new settings;
