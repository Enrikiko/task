<?php
include_once "./services/taskService.php";
include_once "./services/productService.php";
include_once "./services/locationService.php";
include_once "./services/familyService.php";
include_once "./services/actionService.php";

class tasks {
  public static $page = "task";

  public function list($request, $notification){
    if (array_key_exists("employee", $_POST)) {
      $_SESSION["employee"] = $_POST["employee"];
    }

    if (!array_key_exists("employee", $_SESSION)) {
      authService::redirect("/employee");
    }

    if (array_key_exists("success", $_GET)) {
      $notification->setSuccess("","end_task");
    }

    if (array_key_exists("error", $_GET)) {
      $notification->setError("","error_access_task");
    }

    return array(
      "tasks"   => taskService::getStockTask($_SESSION["employee"]),
    );

  }

  public function step($request, $notification){
    $task = taskService::getTaskByHistoric($request["id"]);

    if ((array_key_exists("action", $_POST) &&  ($_POST["action"] == "S" || $_POST["action"] == "F")) || (array_key_exists("cmd-task-save", $_POST) && $_POST["cmd-task-save"])) {
      taskService::saveSteps($request["id"], $_POST["historic"]);

      if (array_key_exists("action", $_POST) && $_POST["action"] == "F") {
        taskService::finishTask($request["id"], $_POST["steps"] , $task["Action"]);
        authService::redirect("/task");
      }
    }

    $historic = taskService::getHistoric($request["id"]);

    if (($historic["ID_Employee"] == "0" || $historic["ID_Employee"] == $_SESSION["employee"]) && $historic["Status"] != "F") {
      $stock = array();
      $steps = taskService::getStepsToWork($historic["ID_Historic"], $task["ID_Task"], $stock);

      if (!$historic["Steps"]) {
        taskService::initSteps($historic, $stock, $steps);
      }
      
      return array(
        "task"              => $task,
        "historic"          => $historic,
        "steps"             => $steps,
        "stock"             => $stock,
        "locations"         => "",
      );
      
    } else {
      authService::redirect("/task?error");
    }
  }

  public function alist($request, $notification) {
    $where = "";
    $limit = 1;

    notificationService::delete($notification, $this::$page);

    if (array_key_exists("active", $_GET)) {
      taskService::activeTask($_GET["active"]);
      $notification->setSuccess("","enable_task");

    } else if (array_key_exists("flow", $_GET)) {
      $notification->setError("","no-flow");
    }

    if (array_key_exists("limit", $_POST)) {
      $limit = $_POST["limit"];
    }

    if (array_key_exists("Name", $_POST) && $_POST["Name"]) {
      $where .= ($where ? " AND " : "") . "Name LIKE '%" . $_POST["Name"] . "%'";
    }

    if (array_key_exists("Description", $_POST) && $_POST["Description"]) {
      $where .= ($where ? " AND " : "") . "Description LIKE '%" . $_POST["Description"] . "%'";
    }

    return array(
      "tasks"       => taskService::getAll($where, $limit, "Name, Description"),
      "page"        => $this::$page,
      "request"     => $_POST,
      "items-search"    => array(
          searchService::getInputText("Name", "name"),
          searchService::getInputText("Description", "description" ,6)
      ),
      "pagination"  => taskService::getPages($where),
    );
  }

  public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);
    
    $task = array();
    $product = array();
    $steps = array();

    if (array_key_exists("success", $_GET)) {
      $notification->setSuccess("","create_task");
    }

    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();

      if(validationService::isCorrect($_POST["Name"],"",100)) { $notification->setError($language->getLabelWithValues("error_name",50)); }
      if(validationService::isCorrect($_POST["Description"],"",250)) { $notification->setError($language->getLabelWithValues("error_description",250)); }
      //if(validationService::isCorrect($_POST["Action"],"",11)) { $notification->setError($language->getLabel("error_action")); }

      if (!$notification->enable) {
        $id = 0;

        if (array_key_exists("cmd-save",$_POST)) {
          $id = taskService::insert($_POST["Name"], $_POST["Description"], $_POST["Observations"], $_POST["Hours"], $_POST["Partial"], $_POST["ID_Family"],json_decode($_POST["steps"], true));

          if (array_key_exists("Rest_need", $_POST) || array_key_exists("Rest_result", $_POST)) {
            taskService::setRest($id, $_POST["Rest_need"], $_POST["Rest_result"]);
          }

          authService::redirect("/admin/task/$id?success");

        } else if (array_key_exists("cmd-overwrite",$_POST)) {
          $id = $_POST["cmd-overwrite"];
          taskService::update($id, $_POST["Name"],$_POST["Description"], $_POST["Observations"], $_POST["Hours"], $_POST["Partial"], $_POST["ID_Family"],json_decode($_POST["steps"], true));
          
          if (array_key_exists("Rest_need", $_POST) || array_key_exists("Rest_result", $_POST)) {
            taskService::setRest($id, $_POST["Rest_need"], $_POST["Rest_result"]);
          }
        }

        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }

    $steps[] = array(
      "product"   => "",
      "qty"       => "0",
      "actions"   => array(),
      "type"      => "0",
    );

    if($request["id"]) {
      $task = taskService::getTaskById($request["id"]);

      if (!count($task)) {
        authService::redirect("/admin/task/0");
      }

      $steps = taskService::getSteps($request["id"]);
    }

    return array(
      "task"              => array_merge($task, $_POST),
      "family"            => familyService::getAllTask(),
      "steps"             => $steps,
      "products"          => productService::getAll("","","product.ID_Family",true),
      "page"              => $this::$page,

      "actions_steps"     => actionService::getAll(),
    );
  }

  public function ahistoric() {
    $where = "";
    $limit = 1;
    
    return array(
      "historic"    => taskService::getAllHistoric($where, $limit),
      "request"     => $_POST,
      "items-search"    => array(
          searchService::getInputText("Name", "name"),
          searchService::getInputText("Description", "description" ,6)
      ),
      "pagination"  => taskService::getPages($where),
      "page"        => $this::$page . "historic",
      "no_create"   => "",
    );
  }

}

$controller = new tasks;
