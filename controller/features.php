<?php

include_once "./services/featureService.php";


class features {
  public static $page = "feature";

	public function alist($request, $notification){
		$where = "";
		$limit = 1;

    notificationService::delete($notification, $this::$page);

		return array(
			"features"    => featureService::getAll($where, $limit),
      
      "page"        => $this::$page,
      "request"     => $_POST,
      "items-search"    => array(
          searchService::getInputText("Description", "description" ,6)
      ),
      "pagination"  => familyService::getPages($where),
		);
	}


	public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);
    $feature = array();

    if (array_key_exists("success", $_GET)) {
        $notification->setSuccess("","create_feature");
    }

    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();
      if(validationService::isCorrect($_POST["Name"],"",50)) { $notification->setError($language->getLabelWithValues("error_name",50)); }
      if(validationService::isCorrect($_POST["Unity"])) { $notification->setError("error_unity"); }

      if (!$notification->enable) {
        if (array_key_exists("cmd-save",$_POST)) {
          $id = featureService::insert($_POST["Name"], $_POST["Unity"]);
          authService::redirect("/admin/feature/$id?success");

        } else if (array_key_exists("cmd-overwrite",$_POST)) {
          featureService::update($_POST["cmd-overwrite"], $_POST["Name"], $_POST["Unity"]);
        }

        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }


    if ($request["id"]) {
      $feature = featureService::getFeature($request["id"]);
    }

		return array(
      "feature"       => array_merge($feature, $_POST),
      "unity"         => featureService::getUnities(),
      "page"          => $this::$page,
		);
	}
}

$controller = new features;
