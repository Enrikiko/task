<?php
include_once "./services/employeeService.php";
include_once "./services/productService.php";
include_once "./services/familyService.php";
include_once "./services/featureService.php";
include_once "./services/actionService.php";
include_once "./services/taskService.php";

class Ajax {
  public function employee() {
    if (array_key_exists("q", $_GET) && $_GET["q"] > 0) {
      if (array_key_exists("action", $_GET) && $_GET["action"] == "d") {
        return employeeService::disconnectEmployee($_GET["q"]);
      } else if (array_key_exists("action", $_GET) && $_GET["action"] == "c" && array_key_exists("workshop", $_SESSION)) {
        return employeeService::connectEmployeeToWorkshop($_GET["q"], $_SESSION["workshop"]);
      }
    }
    return false;
  }

  public function dropzone() {
    if (array_key_exists("q", $_GET) && array_key_exists("f", $_GET) && $_GET["f"] && ($_GET["q"] || $_GET["q"] == "0")) {
      $info = pathinfo($_FILES['file']['name']);
      $ext = $info['extension'];
      $target = "assets/images/" . $_GET["f"] . "/" . $_SESSION["ID_User"];
      
      if ($_GET["f"] == "user") {
        $_GET["f"] = "auth";
      }

      if (!is_dir($target)) {
        mkdir($target);
      }

      array_map('unlink', glob($target . "/tmp_" . $_SESSION["employee"] . "_*.*"));
      $nameFile =  uniqid("tmp_" . $_SESSION["employee"] . "_") . "." . $ext;
      
      if (move_uploaded_file($_FILES["file"]["tmp_name"], $target . "/" . $nameFile)) {
        echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
      } else {
        echo "Sorry, there was an error uploading your file.";
      }
    }
  }

  public function FeaturesFamily(){
    if (array_key_exists("q", $_GET) && $_GET["q"]>0) {
      $features = featureService::getFeaturesByFather($_GET["q"], false);
      foreach ($features as $key => $feature) {
        $features[$key]["Rest"]  = ($feature["Rest"] ? true : false);
      }
      print json_encode($features);
    }
  }

  public function locationByProduct() {
    if (array_key_exists("q", $_GET) && $_GET["q"] > 0) {
      print json_encode(productService::getStockAndLocations($_GET["q"], $_SESSION["workshop"]));
    }
  }

  public function product(){
    if (array_key_exists("product", $_GET) && $_GET["product"] && array_key_exists("values", $_GET) && count($_GET["values"])) {
      $product = productService::getProductById($_GET["product"]);
      
      foreach ($_GET["values"] as $key => $feature) {
        $product["Attributs"][$key] = $feature;
      }

      $product_search = productService::getProductByFeatures($product["Attributs"], $product["ID_Family"]);
      if (!count($product_search)) {
        $id = productService::insert("", "", $product["ID_Family"], json_encode($product["Attributs"]), 0,1);
        $product_search = productService::getProductById($id);
      } else {
        //TODO: mirar que s'ha de fer per triar un sol producte, potser val la pena retornar una llista i que el user decideixi
        $product_search = $product_search[0];
      }

      print json_encode($product_search);
    }
  }

  public function action(){
    if (array_key_exists("id_product", $_GET)) {
      $product = productService::getProductById($_GET["id_product"]);
      $_GET["features"] = $product["Attributs"];
    }

    $ids = actionService::action_Generate_Stock($_GET["features"], $_GET["features_action"], $_GET["qty"] );
    print json_encode($ids);
  }

  public function actionGenerate() {

  }

  public function saveHistoric() {
    if (array_key_exists("id", $_GET) && $_GET["id"] && array_key_exists("historic", $_GET)) {
      taskService::saveSteps($_GET["id"], $_GET["historic"]);
    }
  }
}

$controller = new Ajax;
