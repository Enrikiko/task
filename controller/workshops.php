<?php
include_once "./services/workshopService.php";

class workshops {
  public static $page = "workshop";

  public function list($request, $notification) {
      unset($_SESSION["workshop"]);
      unset($_SESSION["employee"]);

      $workshops = workshopService::getAll($_SESSION["ID_User"]);
      
      if (count($workshops) == 1) {
        $_SESSION["workshop"] = $workshops[0]["ID_workshop"];
        authService::redirect("/admin/employee");

      }

      return $data = array(
        "workshops"    => $workshops,
      );
  }

  public function alist($request, $notification){
    $where = "";
    $limit = 1;

    notificationService::delete($notification, $this::$page);

    if (array_key_exists("limit", $_POST)) {
      $limit = $_POST["limit"];
    }

    if (array_key_exists("name", $_POST) && $_POST["name"]) {
      $where .= ($where ? " AND " : "") . "name LIKE '%" . $_POST["name"] . "%'";
    }

    if (array_key_exists("address", $_POST) && $_POST["address"]) {
      $where .= ($where ? " AND " : "") . "address LIKE '%" . $_POST["address"] . "%'";
    }

    if (array_key_exists("phone", $_POST) && $_POST["phone"]) {
      $where .= ($where ? " AND " : "") . "phone LIKE '%" . $_POST["phone"] . "%'";
    }

    return array(
      "workshop"    => workshopService::getAll($where, $limit),
      "page"        => $this::$page,
      "request"     => $_POST,
      "items-search"    => array(
          searchService::getInputText("name", "name" ),
          searchService::getInputText("address", "address"),
          searchService::getInputText("phone", "phone")
      ),
      "pagination"  => workshopService::getPages($where),
    );
  }

  public function aedit($request, $notification) {
    notificationService::delete($notification, $this::$page);
    
    $workshop = array();

    if (array_key_exists("success", $_GET)) {
        $notification->setSuccess("","create_workshop");
    }

    if (array_key_exists("cmd-save",$_POST) || array_key_exists("cmd-overwrite",$_POST)) {
      $language = languageService::getInstance();
      if(validationService::isCorrect($_POST["name"],"",100)) { $notification->setError($language->getLabelWithValues("error_name",100)); }
      if(validationService::isCorrect($_POST["address"],"",100)) { $notification->setError($language->getLabelWithValues("error_description",100)); }
      if(validationService::isCorrect($_POST["phone"],"",11)) { $notification->setError($language->getLabelWithValues("error_phone",11)); }

      if (!$notification->enable) {
        if (array_key_exists("cmd-save",$_POST)) {
          $id = workshopService::insert($_POST["name"],$_POST["address"], $_POST["phone"]);
          authService::redirect("/admin/workshop/$id?success");

        } else if (array_key_exists("cmd-overwrite",$_POST)) {
          workshopService::update($_POST["cmd-overwrite"],$_POST["name"], $_POST["address"], $_POST["phone"]);
        }

        $notification->setSuccess();

      } else {
        $notification->setTitle("miss_input");
      }
    }

    if ($request["id"]) {
      $workshop = workshopService::getWorkshop($request["id"]);
      if (!count($workshop)) {
        authService::redirect("/admin/workshop/0");
      }
    }

    return array(
      "workshop"    => array_merge($workshop, $_POST),
      "page"        => $this::$page,
    );
  }

}

$controller = new workshops;
