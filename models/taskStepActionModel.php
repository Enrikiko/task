<?php
include_once "./models/fwModel.php";

class taskStepActionModel extends fwModel{
  protected $ids = array("ID_ActionStep");
  protected $tableName = "TaskStepAction";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($name) {
    return array(
      "Description"       => $name,
    );
  }

  public function search($where = "", $columns = "", $group = "", $order = "" ,$limit = "", $show_id_as_key= false) {
    $sql = "SELECT ". ($columns ? $columns : "*" ) ." FROM ". $this->tableName .
      " taskstepaction LEFT JOIN Action action ON action.ID_Action = taskstepaction.ID_Action" .
      " WHERE " . $this->onlyUserData() . $where .
       ($group ? " GROUP BY " . $group : "") . ($order ? " ORDER BY " . $order : "") .
       ($limit ? " LIMIT " . $this->getRecord() . " OFFSET " . (($limit-1) * $this->getRecord() ) : "");

    return $this->execSql($sql, $show_id_as_key);
  }
}
