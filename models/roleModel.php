<?php
include_once "./models/fwModel.php";

class roleModel extends fwModel{
  protected $ids = array("ID_Role");
  protected $tableName = "Role";

  protected function onlyUserData() {
    return "";
  }
}
