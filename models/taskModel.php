<?php
include_once "./models/fwModel.php";

class TaskModel extends fwModel{
  protected $ids = array("ID_Task");
  protected $tableName = "Task";

  protected function onlyUserData() {
    return " ID_User = '" . $_SESSION["ID_User"] . "'";
  }

  public static function columnsAndValues($name, $description, $observation, $hours, $partial, $family, $product, $qty) {
    return array(
      "ID_User"                   => $_SESSION["ID_User"],
      "Name"                      => $name,
      "Description"               => $description,
      "Observations"              => $observation,
      "Hours"                     => ($hours ? $hours : '0'),
      "ID_Product"                => $product,
      "ID_Family"                 => ($family ? $family : '0'),
      "Quantity"                  => $qty,
      "Partial"                   => $partial,
    );
  }
}
