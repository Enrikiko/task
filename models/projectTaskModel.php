<?php
include_once "./models/fwModel.php";

class projectTaskModel extends fwModel{
  protected $ids = array("ID_Project", "ID_Task");
  protected $tableName = "ProjectTask";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($id_task, $id_project) {
    return array(
      "ID_Task"      => $id_task,
      "ID_Project"   => $id_project,
    );
  }

}
