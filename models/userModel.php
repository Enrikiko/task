<?php
include_once "./models/fwModel.php";

class userModel extends fwModel{
  protected $ids = array("ID_User");
  protected $tableName = "User";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($email, $company) {
    return array(
      "email"       		=> $email,
      "company_name"     	=> $company,
    );
  }
}
