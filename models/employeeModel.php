<?php
include_once "./models/fwModel.php";

class employeeModel extends fwModel{
  protected $ids = array("ID_Employee");
  protected $tableName = "Employee";

  protected function onlyUserData() {
    return " ID_User = '" . $_SESSION["ID_User"] . "'";
  }

  public static function columnsAndValues($name, $schedule, $role, $tasks) {
    return array(
      "Name"      => $name,
      "Horario"   => json_encode($schedule),
      "ID_Role"   => $role,
      "ID_User"   => $_SESSION["ID_User"],
      "Tasks"     => $tasks,
    );
  }

}
