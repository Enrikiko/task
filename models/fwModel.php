<?php
include_once "./services/dbService.php";

abstract class fwModel {
  private $db_connection;

  public function __construct() {
    $this->db_connection = dbService::getInstance();
  }

  public function find($where = "", $columns = "", $order = "", $limit="", $show_id_as_key = false) {
    $where = $this->onlyUserData() . ($this->onlyUserData() && $where ? " AND " : "") . $where;
    
    if ($show_id_as_key) {
      return $this->db_connection->selectIdKey($this->tableName, $where, $columns, $order, $limit);

    } else {
      return $this->db_connection->select($this->tableName, $where, $columns, $order, $limit);
    }
  }

  public function findOne($where = "", $columns = "") {
    $results = $this->find($where,$columns);
    return (count($results) ? $results[0] : array());
  }

  public function insert($columns_values) {
    return $this->db_connection->insert($this->tableName, $this->ids, $columns_values);
  }

  public function update($id, $columns_values) {
    $where = $this->getWhereIds($id);
    return $this->db_connection->update($this->tableName, $columns_values, $where);
  }

  public function delete($id, $is_id = true) {
    if ($is_id) {
      $where = $this->getWhereIds($id);

    } else {
      $where = $id;
    }

    $row = $this->findOne($where);
    if (array_key_exists("Image", $row) && $row["Image"]) {
      unlink("./" . $row["Image"]);
    }

    return $this->db_connection->delete($this->tableName, $where);
  }

  public function execSql($sql, $show_id_as_key = false) {
    $stmt = $this->db_connection->execSql($sql);
    
    if ($show_id_as_key) {
      return $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
    } else {
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
  }

  private function getWhereIds($id){
    if (!is_array($id)) {
      $id = array($id);
    }
    $where = "";
    for ($i = 0; $i < count($this->ids); $i++) {
      if (array_key_exists($i, $id)) {
        if ($where) {
          $where .= " AND ";
        }
        $where .= $this->ids[$i] . " = '" . $id[$i] ."'";
      }
    }

    return $where;
  }

  public function getRecord() {
    return $this->db_connection->record;
  }

  public function getPages($where = "") {
    $result = $this->findOne($where, "COUNT(*) as 'row'");
    return ceil($result["row"] / $this->getRecord());
  }

  public function getLastId() {
    return $this->db_connection->getLastId();
  }

  abstract protected function onlyUserData();

}
