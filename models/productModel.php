<?php
include_once "./models/fwModel.php";

class productModel extends fwModel{
  protected $ids = array("ID_Product");
  public $tableName = "Product";

  public function onlyUserData() {
    return " ID_User = '" . $_SESSION["ID_User"] . "'";
  }

  public function search($where = "", $columns = "", $group = "", $order = "" ,$limit = "", $show_id_as_key) {
    $sql = "SELECT ". ($columns ? $columns : "*" ) ." FROM ". $this->tableName .
      " product LEFT JOIN ProductLocation productLocation ON productLocation.ID_Product = product.ID_Product" .
      " LEFT JOIN Location location ON location.ID_Location = productLocation.ID_Location" .
      " WHERE " . $this->onlyUserData() . ($where ? " AND " . $where  : "") .
       ($group ? " GROUP BY " . $group : "") . ($order ? " ORDER BY Name, " . $order : "ORDER BY Name") .
       ($limit ? " LIMIT " . $this->getRecord() . " OFFSET " . (($limit-1) * $this->getRecord() ) : "");

    return $this->execSql($sql, $show_id_as_key);
  }

  public function searchOne($where = "", $columns = "") {
    $results = $this->search($where,$columns);
    return (is_array($results) && count($results) > 0 ? $results[0] : $results);
  }

  public function find($where = "", $columns = "", $order = "", $limit="", $edit_page = false) {
    $results = parent::find($where, $columns, $order, $limit);
    $this->setNameByAttribute($results, $edit_page);

    return $results;
  }

  public function findOne($where = "", $columns = "", $edit_page = false) {
    $results = $this->find($where, $columns, "", "", $edit_page);
    return (count($results) ? $results[0] : array());
  }

  public static function columnsAndValues($name, $description, $family, $attribut, $min_qty, $id_product_father, $tmp) {
    return array(
      "Name"              => $name,
      "Description"       => $description,
      "ID_Family"         => $family,
      "ID_User"           => $_SESSION["ID_User"],
      "Attributs"         => (is_array($attribut) ? json_encode($attribut): $attribut),
      "Min_enabled"       => ($min_qty ? 1 : 0),
      "Min_quantity"      => $min_qty,
      "TMP"               => $tmp,
      "ID_Product_Father" => $id_product_father,
      "date_creation"     => date("Y-m-d H:i:s"),
    );
  }

  public static function setNameByAttribute(&$products, $edit_page = false) {
    if (!is_array($products)) {
      $products = array($products);
    }

    $families = familyService::getAllPreview();

    if (!$edit_page) {
      foreach ($products as $id_product => $product) {
        if (array_key_exists("Attributs", $product) && array_key_exists("ID_Family", $product) && $product["ID_Family"] && array_key_exists("Name", $product)) {
          if (!is_array($product["Attributs"])){
            $products[$id_product]["Attributs"] = json_decode($product["Attributs"], true);
          }

          if (is_array($families[$product["ID_Family"]]) && count($families[$product["ID_Family"]]) && is_array($products[$id_product]["Attributs"])) { 

            $products[$id_product]["Name"] .= " ";
            $preview = json_decode($families[$product["ID_Family"]]["Preview_Product"],true);
            
            foreach ($preview as $key => $feature) {
              if (array_key_exists($feature["ID_Feature"], $products[$id_product]["Attributs"]) && $products[$id_product]["Attributs"][$feature["ID_Feature"]]) {
                $products[$id_product]["Name"] .= $feature["value_before"] . $products[$id_product]["Attributs"][$feature["ID_Feature"]] . $feature["value_after"];
              }
            }

          }
        }
      }
    }
  }
}
