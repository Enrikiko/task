<?php
include_once "./models/fwModel.php";

class featureModel extends fwModel{
  protected $ids = array("ID_Feature");
  protected $tableName = "Feature";
  public $unities = array("mm","l","kg","degree", "quality");

  protected function onlyUserData() {
    return " (ID_User = '" . $_SESSION["ID_User"] . "' OR ID_User = '0')";
  }

  public static function columnsAndValues($name, $unity) {
    return array(
      "ID_User"         => $_SESSION["ID_User"],
      "Name"            => $name,
      "Unity"           => $unity,
    );
  }
}
