<?php
include_once "./models/fwModel.php";

class projectModel extends fwModel{
  protected $ids = array("ID_Project");
  protected $tableName = "Project";

  protected function onlyUserData() {
    return " ID_User = '" . $_SESSION["ID_User"] . "'";
  }

  public static function columnsAndValues($name) {
    return array(
      "Name"      => $name,
      "ID_User"   => $_SESSION["ID_User"],
    );
  }

}
