<?php
include_once "./models/fwModel.php";

class taskStepsModel extends fwModel{
  protected $ids = array("ID_Step");
  protected $tableName = "TaskSteps";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($id_task, $id_product_need, $id_product_result, $description, $qty_need, $qty_result) {
    return array(
      "ID_Task"           => $id_task,
      "ID_Product_need"   => $id_product_need,
      "ID_Product_result" => $id_product_result,
      "Description"       => $description,
      "Quantity_need"     => $qty_need,
      "Quantity_result"   => $qty_result,
    );
  }
}
