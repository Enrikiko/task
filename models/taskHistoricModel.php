<?php
include_once "./models/fwModel.php";

class taskHistoricModel extends fwModel{
  protected $ids = array("ID_Historic");
  protected $tableName = "TaskHistoric";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($id_project, $id_task, $id_employee, $model, $priority = 0) {
    return array(
      "ID_Project"       => $id_project,
      "ID_Task"          => $id_task,
      "ID_Employee"      => $id_employee,
      "Model"            => $model,
      "Priority"         => $priority,
    );
  }
}
