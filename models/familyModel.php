<?php
include_once "./models/fwModel.php";

class familyModel extends fwModel{
  protected $ids = array("ID_Family");
  protected $tableName = "Family";
  public $types = array("P" => "product","T" => "task");
  public $state_matter = array("S","L","G");

  protected function onlyUserData() {
    return " ID_User = '" . $_SESSION["ID_User"] . "'";
  }

  public static function columnsAndValues($description, $type, $father, $state_matter) {
    return array(
      "Description"       => $description,
      "Type"              => $type ,
      "ID_User"           => $_SESSION["ID_User"],
      "ID_Family_Father"  => $father,
      "State_matter"      => $state_matter,
    );
  }
}
