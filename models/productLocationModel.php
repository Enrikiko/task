<?php
include_once "./models/fwModel.php";

class productLocationModel extends fwModel{
  protected $ids = array("ID_Quantity");
  protected $tableName = "ProductLocation";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($id_product, $id_location, $rest = 0) {
    return array(
      "ID_Product"        => $id_product,
      "ID_Location"       => $id_location,
      "Rest"              => $rest,
    );
  }
}
