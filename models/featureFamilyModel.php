<?php
include_once "./models/fwModel.php";

class featureFamilyModel extends fwModel{
  protected $ids = array("ID_Feature", "ID_Family");
  protected $tableName = "FeatureFamily";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($id_family, $id_feature, $rest) {
    return array(
      "ID_Family"            => $id_family,
      "ID_Feature"           => $id_feature,
      "Rest"                 => ($rest ? 1 : 0),
    );
  }
}
