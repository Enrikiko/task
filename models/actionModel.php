<?php
include_once "./models/fwModel.php";

class ActionModel extends fwModel{
  protected $ids = array("ID_Action");
  protected $tableName = "Action";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($name) {
    return array(
      "Description"       => $name,
    );
  }
}
