<?php
include_once "./models/fwModel.php";

class locationModel extends fwModel{
  protected $ids = array("ID_Location");
  protected $tableName = "Location";

  protected function onlyUserData() {
    return "";
  }

  public static function columnsAndValues($name, $description, $workshop) {
    return array(
      "Description_Location"      => $description,
      "Name_Location"             => $name,
      "ID_workshop"               => $workshop,
    );
  }

}
