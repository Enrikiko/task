<?php
include_once "./models/fwModel.php";

class workshopModel extends fwModel{
  protected $ids = array("ID_workshop");
  protected $tableName = "Workshop";

  protected function onlyUserData() {
    return " ID_User = '" . $_SESSION["ID_User"] . "'";
  }

  public static function columnsAndValues($name, $address, $phone) {
    return array(
      "name"            => $name,
      "address"         => $address,
      "phone"           => $phone,
      "ID_User"         => $_SESSION["ID_User"],
    );
  }
}
